import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import { AddressPage } from '../../pages/address/address';
import { ServicethankyouPage } from '../../pages/servicethankyou/servicethankyou';

import { AddaddressPage } from '../../pages/addaddress/addaddress';
import { SharedProvider } from '../../providers/shared/shared';
import { Storage } from '@ionic/storage';
import { AlertController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';

/**
 * Generated class for the ServicebookingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-servicebooking',
  templateUrl: 'servicebooking.html',
})
export class ServicebookingPage {
  data:any;
  loaded:any="0";
  userid:any;
addaddresspage:any=AddaddressPage;
addresspage:any=AddressPage;
thankyou:any=ServicethankyouPage;
address:any;
serviceid:any;
addressid:any;
servicename:any;
specialmessage:any;
mobile:any;

  constructor(public shared: SharedProvider,public navCtrl: NavController, public navParams: NavParams,public http: Http,private storage: Storage,private alertCtrl: AlertController,public loadingCtrl: LoadingController) {
    this.serviceid=this.navParams.get('serviceid');
    this.servicename=this.navParams.get('servicename');

    this.storage.get('mobile').then((val) => {
      this.mobile=val;
       
            });
  }
  ionViewDidLoad() {
    


  }
  ionViewDidEnter(){
   // this.ionViewDidLoad(); 

this.storage.get('userid').then((val) => {
      this.userid= val;
      if(this.userid=='nouserid'){
        
      }else{
            this.getaddress();
      
      }
          });
           console.log('ionViewDidLoad StorecheckoutPage');
    var cartarray=this.shared.storecartdetails.all;
console.log(this.shared.storecartdetails.all);

  }
  gotoaddresspage(){
    this.navCtrl.push(this.addaddresspage,{pop :1});
  }
 
  getaddress() {
    var link = 'https://api.mmsilks.onstep.in/farmlifeappbackend/address.php'; 
    
     var myData = JSON.stringify({userid:  this.userid});
     
    
      
     
     
    this.http.post(link, myData)
    //this.http.get(link)
    
     .subscribe(data => {

     this.data = data["_body"];
     console.log(this.data);


     this.data = JSON.parse(this.data);
if(this.data.status=='sucess'){

//this.dates=this.data.data[0];
console.log(this.data);
 this.address= this.data.address[0];
 this.addressid= this.address.id;
 
}
if(this.data.status=='noaddress'){
//  this.navCtrl.push(this.addresspage,{pop :1});


  let alert = this.alertCtrl.create({
    title: 'ADD ADDRESS',
    message: 'Add your address',
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
          this.navCtrl.pop();
        }
      },
      {
        text: 'Add',
        handler: () => {
          this.navCtrl.push(this.addaddresspage,{pop :1});

          console.log('Buy clicked');
        }
      }
    ]
  });
  alert.present();

}
 this.loaded="1";

     }, error => {
    // alert(id);
     });
     }
     
 


     booknow() {

      let loading = this.loadingCtrl.create({
        content: 'Please wait...'
      });
      loading.present();

          setTimeout(() => {
          loading.dismiss();
        }, 20000);
      var link = 'http://18.220.1.217/services/bookservice.php'; 
        this.specialmessage="";
      
      
       var myData = JSON.stringify({mobile:this.mobile,userid:  this.userid,serviceid: this.serviceid,servicename:this.servicename,addressid:this.addressid,message:this.specialmessage});
       
      
        
       
       
      this.http.post(link, myData)
      //this.http.get(link)
      
       .subscribe(data => {
  
       this.data = data["_body"];
  // alert(this.data);
  
       this.data = JSON.parse(this.data);
  if(this.data.status=='sucess'){
  
//alert(this.data.id);

 //alert('sucess');
 loading.dismiss();
 this.navCtrl.popToRoot() ;
 this.navCtrl.push(this.thankyou, {  orderid: this.data.id });
 
  }
  
  
       }, error => {
      // alert(id);
       });
       }
       
   
    
  

}
