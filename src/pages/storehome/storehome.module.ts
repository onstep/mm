import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StorehomePage } from './storehome';

@NgModule({
  declarations: [
    StorehomePage,
  ],
  imports: [
    IonicPageModule.forChild(StorehomePage),
  ],
})
export class StorehomePageModule {}
