import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ResidencelistPage } from '../../pages/residencelist/residencelist';

/**
 * Generated class for the ResidencetypePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-residencetype',
  templateUrl: 'residencetype.html',
})
export class ResidencetypePage {

  list:any=ResidencelistPage;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ResidencetypePage');
  }
  golist(type){
    this.navCtrl.push(this.list,{residencetype: type});
  }

}
