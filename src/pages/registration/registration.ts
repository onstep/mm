import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController,Events } from 'ionic-angular';
import {Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
//import { OTPAutoVerification } from 'cordova-plugin-otp-auto-verification';
import { Storage } from '@ionic/storage';
declare var OTPAutoVerification: any;
//import { AndroidPermissions } from '@ionic-native/android-permissions';
import { Platform } from 'ionic-angular';
import { OneSignal } from '@ionic-native/onesignal';
import { ToastController } from 'ionic-angular';
import { FirstorderPage} from '../../pages/firstorder/firstorder';
/**
 * Generated class for the RegistrationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-registration',
  templateUrl: 'registration.html',
})
export class RegistrationPage {
new:any={};
error:any;
error2:any;
panerror:any;
aadharerror:any;
mobileerror:any;
formdata:any={};
userid:any;
disablebutton:any;
loaded:any;
data:any;
sponserdata:any;
orderpage:any=FirstorderPage;
constructor(public toastCtrl: ToastController,public navCtrl: NavController, public navParams: NavParams,public http: Http,public loadingCtrl: LoadingController,private storage: Storage, public events: Events,public plt: Platform,public oneSignal: OneSignal) {
  this.storage.get('registerformdata').then((val) => {
    if(val){
this.formdata=val;
    }

     });
}

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegistrationPage');

var today = new Date();
today = new Date('1 Jan 1995');
this.formdata.dob = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
  }


  toast(text) {
    const toast = this.toastCtrl.create({
      message: text,
      duration: 3000,
      position:'bottom'
    });
    toast.present();
  }

  update() {
    console.log('clicked');

    //return;
    
   

    if(!this.formdata.name || this.formdata.name == ' '){
     // PNotify.notice({ text: 'Notice 1.', stack: {'dir1':'down', 'firstpos1':25}   });
     this.error='Invalid name !';
     this.disablebutton=false;
     this.toast(this.error);

     return;

    }

    if(!this.formdata.surname || this.formdata.surname == ' '){
    
    }
    if(!this.formdata.mobile || this.formdata.mobile == ' ' || this.formdata.mobile.toString().length!=10 ){
     // PNotify.notice({ text: 'Notice 1.', stack: {'dir1':'down', 'firstpos1':25}   });
     this.error='Invalid Mobile Number !';
     this.disablebutton=false;
     this.toast(this.error);

     return;

    }
    if(!this.formdata.refid || this.formdata.refid == ' ' || this.formdata.refid.toString().length!=10 ){
     // PNotify.notice({ text: 'Notice 1.', stack: {'dir1':'down', 'firstpos1':25}   });
     this.error='Invalid Sponser Id !';
     this.disablebutton=false;
     this.toast(this.error);

     return;

    }
    if(!this.formdata.dob || this.formdata.dob == ' '){
     // PNotify.notice({ text: 'Notice 1.', stack: {'dir1':'down', 'firstpos1':25}   });
     this.error='Invalid D.O.B !';
     this.disablebutton=false;
     this.toast(this.error);

     return;

    }
    if(!this.formdata.email || this.formdata.email == ' '){
     //this.error='Invalid Email !';
     //this.disablebutton=false;

     //return;

    }
    if(!this.formdata.pan || this.formdata.pan == ' ' || this.formdata.pan.toString().length!=10){
    // this.error='Invalid PAN Number !';
     //this.disablebutton=false;

    // return;

    }
    if(!this.formdata.aadhar || this.formdata.aadhar == ' '  || this.formdata.aadhar.toString().length!=12){
     this.error='Invalid Aadhar Number!';
     this.disablebutton=false;
     this.toast(this.error);

     return;

    }
    if(!this.formdata.line1 || this.formdata.line1==''){
     this.error='Invalid Address!';
     this.disablebutton=false;
     this.toast(this.error);

     return;

    }
    if( !this.formdata.city || this.formdata.city == ''){
      this.error='Invalid city!';
      this.disablebutton=false;
      this.toast(this.error);

      return;
 
     }
     if( !this.formdata.state || this.formdata.state == ''){
      this.error='Invalid state!';
      this.disablebutton=false;
      this.toast(this.error);

      return;
 
     }

     if( !this.formdata.pincode || this.formdata.pincode == ''){

      this.error='Invalid pincode!';
      this.disablebutton=false;
      this.toast(this.error);

      return;
 
     }
    if(!this.formdata.bankname || this.formdata.bankname == ' '){
     this.error='Invalid Bank name!';
     this.disablebutton=false;
     this.toast(this.error);

     return;

    }
    if(!this.formdata.branch || this.formdata.branch == ' '){
     this.error='Invalid Branch!';
     this.disablebutton=false;
     this.toast(this.error);

     return;

    }
    if(!this.formdata.accountnumber || this.formdata.accountnumber == ' '){
     this.error='Invalid Account Number!';
     this.disablebutton=false;
     this.toast(this.error);

     return;

    }
    if(!this.formdata.ifsc || this.formdata.ifsc == ' '){
     this.error='Invalid IFSC !';
     this.disablebutton=false;
     this.toast(this.error);

     return;

    }
    if(!this.formdata.refname || this.formdata.refname == ' '){
     this.error='Invalid sponser id !';
     this.disablebutton=false;
     this.toast(this.error);

     return;

    }
   
    if(this.formdata.verifyaccountnumber!=this.formdata.accountnumber){
     this.error="Account number mismatch !";
     this.disablebutton=false;
     this.toast(this.error);

     return;

    }

   
this.error=null;

let loading = this.loadingCtrl.create({
  content: ' Please wait...'
});

loading.present();

       // this.router.navigate(['/workers/addworker/'+this.engineerid]);

       this.loaded='1';
       this.disablebutton=true;
    this.formdata.companyid=this.userid;
  
      var link = 'https://api.mmsilks.onstep.in/farmlife/auth/onstepvalidate.php';
  
       var myData = JSON.stringify(
        {
          first_name: this.formdata.name,
          last_name: this.formdata.surname,
          customer_phone: this.formdata.mobile,
          address: this.formdata.line1,
          address2: this.formdata.line2,
          address3: this.formdata.line3,
          city: this.formdata.city ,
          state: this.formdata.state,
          country: 'india',
          pincode: this.formdata.pincode,
          longitude: '-',
          latitude: '-',
          sponsor_name:  this.formdata.refname,
          sponsor_id: this.formdata.refid,
          sponsor_mobile : this.formdata.refmobile,
          aadhar_no: this.formdata.aadhar,
          bank_name: this.formdata.bank,
          account_no: this.formdata.accountnumber,
          branch: this.formdata.branch,
          ifsc_code: this.formdata.ifsc,
          unit_key: 11204,
          rice_option: 'ravi',
          customer_type: 'customer',
          country_code: 91,
          customer_active_status: '1',
          combo_active_status: '1'
        }
       );
       console.log(myData);
  
  
  

  
      this.http.post(link,myData)
      //this.http.get(link)
  
       .subscribe(data => {
        console.log(data);
  
       this.data = data["_body"];
    //alert(this.data);
    console.log(this.data);
  
       this.data = JSON.parse(this.data);
    console.log(this.data);
    if(this.data.status=='success'){
      //this.toast('sucess');
      this.storage.set('register',JSON.parse(myData));
     // this.data = JSON.parse(this.data);

      this.storage.set('registerformdata', this.formdata);
      loading.dismiss();

         this.navCtrl.push(this.orderpage);
    //  this.formdata={};
     // this.engineerid=this.data.id;
      this.loaded='0';

      //this.ngOnInit();
      //this.openSuccessCancelSwal();
  
    }
    if(this.data.status=='Failure'){
      loading.dismiss();

      this.toast(this.data.statusmessage);
    }
  
       }, error => {
        loading.dismiss();
        this.toast('Check your internet connection');

    this.data.status='failed';
       this.loaded='0';
  
       });


      
   


  
       } 

    


       checksponser(){
       if(!this.formdata.mobile){
return;
       }
       if(!this.formdata.refid){
return;
       }
      if(this.formdata.refid.toString().length>9 && this.formdata.mobile.toString().length>9){
        var link = 'https://api.mmsilks.onstep.in/farmlife/registration/checksponsor.php';
     
        var myData = JSON.stringify({mobile:  this.formdata.mobile,refid:this.formdata.refid});

           console.log(myData);
      
      
      
      
      
          this.http.post(link,myData)
          //this.http.get(link)
      
           .subscribe(data => {
            console.log(data);
      
           this.sponserdata = data["_body"];
        //alert(this.data);
        console.log(this.sponserdata);
      
           this.sponserdata = JSON.parse(this.sponserdata);
        console.log(this.sponserdata);
        if(this.sponserdata.status=='success'){
         // alert("sucecss");
         if(this.sponserdata.records=='norecords'){

          this.formdata.refname=null;

   
        }else{
         

   this.formdata.refname=this.sponserdata.records[0].name;
   
   
        }
         // this.engineerid=this.data.id;
          this.loaded='1';
          //this.ngOnInit();
          //this.openSuccessCancelSwal();
      
        }
      
           }, error => {
        this.data.status='failed';
           this.loaded='0';
      
           });
           } 
          }


}
