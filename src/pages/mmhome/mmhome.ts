import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MmcategoryPage} from '../../pages/mmcategory/mmcategory';
import { App, ViewController } from 'ionic-angular'; 

/**
 * Generated class for the MmhomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-mmhome',
  templateUrl: 'mmhome.html',
})
export class MmhomePage {
  category:any;
  Mmcategory=MmcategoryPage;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MmhomePage');

   this.category=[
      { "price":"500", "nameone":"saree",  "nametwo":"saree", "image":"https://mmsilks.com/wp-content/uploads/2019/10/Vinay-Fashion-Sheesha-Harmony-19688-diva-exports-300x300.jpeg","imagetwo":"https://mmsilks.com/wp-content/uploads/2019/09/Suvesh-Lakhnavi-Nx-Chikan-Work-Kurti-Wholesale-Catalog-10-Pcs-4-300x300.jpg","imagethree":"https://mmsilks.com/wp-content/uploads/2019/11/181122233014_0011-300x300.jpg"},
      { "price":"600", "nameone":"Kurtis",  "nametwo":"saree", "image":"https://mmsilks.com/wp-content/uploads/2019/10/Vinay-Fashion-Sheesha-Harmony-19688-diva-exports-300x300.jpeg","imagetwo":"https://mmsilks.com/wp-content/uploads/2019/09/Suvesh-Lakhnavi-Nx-Chikan-Work-Kurti-Wholesale-Catalog-10-Pcs-4-300x300.jpg" ,"imagethree":"https://mmsilks.com/wp-content/uploads/2019/11/181122233014_0011-300x300.jpg"},
      { "price":"700", "nameone":"Salwar Suits",  "nametwo":"saree", "image":"https://mmsilks.com/wp-content/uploads/2019/10/Vinay-Fashion-Sheesha-Harmony-19688-diva-exports-300x300.jpeg","imagetwo":"https://mmsilks.com/wp-content/uploads/2019/09/Suvesh-Lakhnavi-Nx-Chikan-Work-Kurti-Wholesale-Catalog-10-Pcs-4-300x300.jpg" ,"imagethree":"https://mmsilks.com/wp-content/uploads/2019/11/181122233014_0011-300x300.jpg"},
      { "price":"800", "nameone":"Lehenga",  "nametwo":"saree", "image":"https://mmsilks.com/wp-content/uploads/2019/10/Vinay-Fashion-Sheesha-Harmony-19688-diva-exports-300x300.jpeg","imagetwo":"https://mmsilks.com/wp-content/uploads/2019/09/Suvesh-Lakhnavi-Nx-Chikan-Work-Kurti-Wholesale-Catalog-10-Pcs-4-300x300.jpg" ,"imagethree":"https://mmsilks.com/wp-content/uploads/2019/11/181122233014_0011-300x300.jpg"},
      { "price":"900", "nameone":"Party Wears",  "nametwo":"saree", "image":"https://mmsilks.com/wp-content/uploads/2019/10/Vinay-Fashion-Sheesha-Harmony-19688-diva-exports-300x300.jpeg","imagetwo":"https://mmsilks.com/wp-content/uploads/2019/09/Suvesh-Lakhnavi-Nx-Chikan-Work-Kurti-Wholesale-Catalog-10-Pcs-4-300x300.jpg" ,"imagethree":"https://mmsilks.com/wp-content/uploads/2019/11/181122233014_0011-300x300.jpg"},
      { "price":"800", "nameone":"Handloom Wears",  "nametwo":"saree", "image":"https://mmsilks.com/wp-content/uploads/2019/10/Vinay-Fashion-Sheesha-Harmony-19688-diva-exports-300x300.jpeg","imagetwo":"https://mmsilks.com/wp-content/uploads/2019/09/Suvesh-Lakhnavi-Nx-Chikan-Work-Kurti-Wholesale-Catalog-10-Pcs-4-300x300.jpg" ,"imagethree":"https://mmsilks.com/wp-content/uploads/2019/11/181122233014_0011-300x300.jpg"},
    ]
  }
  subcategory(){
    this.navCtrl.push(this.Mmcategory);
  }

}
