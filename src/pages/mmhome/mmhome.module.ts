import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MmhomePage } from './mmhome';

@NgModule({
  declarations: [
    MmhomePage,
  ],
  imports: [
    IonicPageModule.forChild(MmhomePage),
  ],
})
export class MmhomePageModule {}
