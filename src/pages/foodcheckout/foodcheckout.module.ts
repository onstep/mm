import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FoodcheckoutPage } from './foodcheckout';

@NgModule({
  declarations: [
    FoodcheckoutPage,
  ],
  imports: [
    IonicPageModule.forChild(FoodcheckoutPage),
  ],
})
export class FoodcheckoutPageModule {}
