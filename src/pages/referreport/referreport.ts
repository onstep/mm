import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import { AlertController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { CallNumber } from '@ionic-native/call-number';
import { Item, ItemSliding } from 'ionic-angular';

/**
 * Generated class for the WalletPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

/**
 * Generated class for the ReferreportPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-referreport',
  templateUrl: 'referreport.html',
})
export class ReferreportPage {
  amount:any="";
  userid:any;
  razorpaysecure:any;
  data:any;
  loaded:any='0';
  loading:any;

  activeItemSliding: ItemSliding = null;

    constructor(private callNumber: CallNumber,public navCtrl: NavController, public navParams: NavParams,public http: Http,private storage: Storage,private alertCtrl: AlertController,public loadingCtrl: LoadingController) {
  
    }
   
    ionViewWillEnter() {
      
      this.storage.get('userdata').then((val) => {
        
        this.userid= val.userid;
        async function delay(ms: number) {
          await new Promise(resolve => setTimeout(()=>resolve(), 1000)).then(()=>console.log("fired"));
      }
    
      delay(500).then(any=>{
    //alert('hh');
    if(this.userid!='nouserid'){
                    this.getdata(this.userid);
  
    }
  
    });
      
  
    });
    }
  
      
    call(mobile){
      this.callNumber.callNumber(mobile, true)
  .then(res => console.log('Launched dialer!', res))
  .catch(err => console.log('Error launching dialer', err));
    }


    getdata(id) {
      var link = 'https://api.mmsilks.onstep.in/farmlifeappbackend/user/viewrefered.php'; 
      
       var myData = JSON.stringify({userid:  id});
       
      
        
       
       
      this.http.post(link, myData)
      //this.http.get(link)
      
       .subscribe(data => {
  
       this.data = data["_body"];
       console.log(this.data);
  
  
       this.data = JSON.parse(this.data);
  console.log(this.data);

  if(this.data.status=='success'){
    
  this.loaded="1";

  }
   
  
       }, error => {

      // alert(id);
       });
       }


       openOption(itemSlide: ItemSliding, item: Item) {
        console.log('opening item slide..');
        
        if(this.activeItemSliding!==null) //use this if only one active sliding item allowed
         this.closeOption();
     
        this.activeItemSliding = itemSlide;
     
        let swipeAmount = 124; //set your required swipe amount
     
        itemSlide.startSliding(swipeAmount);
        itemSlide.moveSliding(swipeAmount);
     
        itemSlide.setElementClass('active-options-right', true);
        itemSlide.setElementClass('active-swipe-right', true);
     
        item.setElementStyle('transition', null);
        item.setElementStyle('transform', 'translate3d(-'+swipeAmount+'px, 0px, 0px)');
       }
     
       closeOption() {
        console.log('closing item slide..');
     
        if(this.activeItemSliding) {
         this.activeItemSliding.close();
         this.activeItemSliding = null;
        }
       }


  
  }
  
  