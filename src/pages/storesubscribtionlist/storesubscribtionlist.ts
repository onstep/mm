import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import { StoresubscribtionlistbydayPage } from '../../pages/storesubscribtionlistbyday/storesubscribtionlistbyday';

/**
 * Generated class for the StoresubscribtionlistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-storesubscribtionlist',
  templateUrl: 'storesubscribtionlist.html',
})
export class StoresubscribtionlistPage {
  loaded:any="0";
  data:any;
  userid:any;
  byday:any=StoresubscribtionlistbydayPage;
  constructor(public navCtrl: NavController, public navParams: NavParams,public http: Http,private storage: Storage) {
  }

  ionViewDidLoad() {
    this.storage.get('userid').then((val) => {
      this.userid= val;
      async function delay(ms: number) {
        await new Promise(resolve => setTimeout(()=>resolve(), 1000)).then(()=>console.log("fired"));
    }
  
    delay(500).then(any=>{
  //alert('hh');
    var id=this.navParams.get('subcategoryid');
              this.getdata(this.userid);

  });
    

  });
  }
gobyday(day){
  this.navCtrl.push(this.byday,{day: day});
}
  getdata(id) {
    var link = 'https://api.mmsilks.onstep.in/farmlifeappbackend/subscriptionlist.php'; 
    
     var myData = JSON.stringify({userid:  id});
     
    
      
     
     
    this.http.post(link, myData)
    //this.http.get(link)
    
     .subscribe(data => {

     this.data = data["_body"];
     console.log(this.data);


     this.data = JSON.parse(this.data);

console.log(this.data);
 
this.loaded="1";

     }, error => {
    // alert(id);
     });
     }
}
