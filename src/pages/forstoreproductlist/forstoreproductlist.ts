import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ProductlistPage } from '../../pages/productlist/productlist';
import { Component, ViewChild, OnInit, Renderer, Input } from '@angular/core';
import { Content } from 'ionic-angular';
import { StoresearchPage } from '../../pages/storesearch/storesearch';

/**
 * Generated class for the ForstoreproductlistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-forstoreproductlist',
  templateUrl: 'forstoreproductlist.html',
})
export class ForstoreproductlistPage {
  @ViewChild(Content) content: Content;
  page1=ProductlistPage;
  name:any;
  storesearchpage=StoresearchPage;

  cat3id:any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
     this.cat3id=this.navParams.get('subcategoryid');
     this.name=this.navParams.get('subcategoryname');
//alert(this.name);
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad ForstoreproductlistPage');
  }


  gotosearch(){

    this.navCtrl.push(this.storesearchpage, { });

  }
  
}
