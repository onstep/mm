import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { Http } from '@angular/http';
import { StorethankyouPage } from '../../pages/storethankyou/storethankyou';
declare var RazorpayCheckout: any;
import { SharedProvider } from '../../providers/shared/shared';
import { LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the StorepaymentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-storepayment',
  templateUrl: 'storepayment.html',
})
export class StorepaymentPage {
  data:any;
  userdata:any;
 paymenttype:any='online';
 userid:any=this.navParams.get('userid');
 cart:any=this.navParams.get('cart');
 addressid:any=this.navParams.get('addressid');
 deliverytype:any=this.navParams.get('deliverytype');
 slotid:any=this.navParams.get('slotid');
 amount:any=this.navParams.get('amount');
 totalmrp:any=this.navParams.get('totalmrp');
 onlinepay:any=this.navParams.get('onlinepay');
 razorpaysecure:any=this.navParams.get('razorpaysecure');
 expressdeliverychargediscount:any=this.navParams.get('expressdeliverychargediscount');
 expressdeliverycharge:any=this.navParams.get('expressdeliverycharge');
 wallet:any=this.navParams.get('wallet');
 mobile:any=this.navParams.get('mobile');
 finalexpressdeliverycharge:any;
 thankyou:any=StorethankyouPage;
amounttopay:any;
amountfromwallet:any;
 constructor(private storage: Storage , public shared: SharedProvider,public navCtrl: NavController, public navParams: NavParams,private alertCtrl: AlertController,public http: Http,public loadingCtrl: LoadingController) {
//alert(this.slotid);
this.storage.get('userdata').then((val) => {
  // alert(val.status);
  if(val){
    this.userdata=val;
  }
 });
this.amount=Number(this.amount);
this.wallet=Number(this.wallet);

if(this.amount<this.wallet){
  this.amounttopay=this.wallet-this.amount;
  this.amounttopay=0;
  this.amountfromwallet=this.amount;


}

if(this.amount>=this.wallet){
this.amounttopay=this.amount-this.wallet;
this.amountfromwallet=this.wallet;

}





this.finalexpressdeliverycharge=this.expressdeliverycharge-this.expressdeliverychargediscount;
this.slotid='0';
if(this.slotid=='0'){
this.amount=(+this.amount)+(+this.finalexpressdeliverycharge);
//alert(this.amount);
}
var myy = JSON.stringify({userid:this.userid,cart: this.cart,addressid: this.addressid,deliverytype: this.deliverytype, slotid: this.slotid,amount:this.amount,totalmrp:this.totalmrp,paymenttype:this.paymenttype,expressdeliverychargediscount:this.expressdeliverychargediscount,expressdeliverycharge:this.expressdeliverycharge});
console.log(myy);
  }

  ionViewDidLoad() {
    this.storage.get('userdata').then((val) => {
      // alert(val.status);
      if(val){
        this.userdata=val;
      }
     });
  }

 completeorder(razorid){
       console.log('ionViewDidLoad StorepaymentPage');
       let loading = this.loadingCtrl.create({
        content: 'Please wait...'
      });
      loading.present();

          setTimeout(() => {
          loading.dismiss();
        }, 20000);
    var link = 'https://api.mmsilks.onstep.in/farmlifeappbackend/order.php'; 
    
   //  alert(razorid);
  

     var myData = JSON.stringify({userdata:this.userdata,mobile: this.mobile,userid:this.userid,cart: this.cart,addressid: this.addressid,deliverytype: this.deliverytype, slotid: this.slotid,amount:this.amount,totalmrp:this.totalmrp,paymenttype:this.paymenttype,expressdeliverychargediscount:this.expressdeliverychargediscount,expressdeliverycharge:this.expressdeliverycharge,razorpayid: razorid,firstorder:'no'});

      
     
     
    this.http.post(link, myData)
    //this.http.get(link)
    
     .subscribe(data => {

     this.data = data["_body"];
 console.log(this.data);

     this.data = JSON.parse(this.data);
if(this.data.status=='sucess'){
//alert("sucess");
loading.dismiss();


this.storage.get('storecart').then((val) => {
//console.log(val);
 this.shared.removestorecart();
 this.navCtrl.popToRoot() ;
  this.navCtrl.push(this.thankyou,{orderid: this.data.orderid});

    

   
 });
}

     }, error => {


     });
     
     

 }


  codconfirm() {
    let alert = this.alertCtrl.create({
      title: 'Confirm Order',
      message: 'Do you want to place this order on COD?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Buy',
          handler: () => {
            console.log('Buy clicked');
            this.completeorder('0');
          }
        }
      ]
    });
    alert.present();
  }

  placeorder(){
    if(this.paymenttype=='cod'){
      this.codconfirm();
    }
    if(this.paymenttype=='online'){
this.pay();
    }
  }

  pay() {
    var options = {
      description: 'Amount payable.',
      image: 'http://18.220.1.217/images/logolow.png',
      currency: 'INR',
      key: this.razorpaysecure,
      amount: this.amount*100,
      name: 'Onstep',
      prefill: {
      
     
      },
      external: {
        wallets: ['paytm','mobiwik']
      },
      theme: {
        color: '#ff7b1c'
      },
      modal: {
        ondismiss: function() {
          alert('dismissed')
        }
      }
    };

    var successCallback = (payment_id) => {
      let vm=this;
    // alert('payment_id: ' + payment_id);
     vm.completeorder(payment_id);
   //  alert('sucess');

     

    };

    var cancelCallback = function(error) {
      alert(error.description + ' (Error ' + error.code + ')');
     // alert("failed");
    };

    RazorpayCheckout.open(options, successCallback, cancelCallback);
  }

  
}
