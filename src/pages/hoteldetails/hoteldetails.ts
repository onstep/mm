import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http'; //https://stackoverflow.com/questions/43609853/angular-4-and-ionic-3-no-provider-for-http
import { SharedProvider } from '../../providers/shared/shared';
import { ImageLoader } from 'ionic-image-loader';
import { Component ,ViewChild} from '@angular/core';
import { Content } from 'ionic-angular';
import { StorecartPage } from '../../pages/storecart/storecart';
import { StorecategoryPage } from '../../pages/storecategory/storecategory';
  import { App, ViewController } from 'ionic-angular';
  import { HotelmenulistPage } from '../../pages/hotelmenulist/hotelmenulist';
  import { HotelmenubycategoryPage } from '../../pages/hotelmenubycategory/hotelmenubycategory';

/**
 * Generated class for the HoteldetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-hoteldetails',
  templateUrl: 'hoteldetails.html',
})
export class HoteldetailsPage {


@ViewChild(Content) content: Content;

data: any;
catg: any;
go: any=0;
loaded:any="0";
hotelid:any;
hotelmenu:any=HotelmenulistPage;
hotelmenubycategory:any=HotelmenubycategoryPage;
  constructor(public navParams: NavParams,public navCtrl: NavController ,public http: Http,public shared: SharedProvider, public imageLoader: ImageLoader,public app :App) {

    this.hotelid=this.navParams.get('hotelid');
  }


  ionViewDidEnter() {
   // console.log('ionViewDidLoad MainPage');
    this.getposts();
  }
  
  getposts() {
    var link = 'http://18.220.1.217/food/home.php'; 
    
     var myData = JSON.stringify({location:  'all'});
     
    
      
     
     
    this.http.post(link, myData)
    //this.http.get(link)
    
     .subscribe(data => {

     this.data = data["_body"];

     this.data = JSON.parse(this.data);
     //       console.log(this.catg);
     console.log(this.data);

           

  //  console.log(this.catg.subcategory);


this.loaded="1"
     }, error => {
     
     });
     }

}

