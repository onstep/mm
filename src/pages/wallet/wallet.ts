import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import { AlertController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';

/**
 * Generated class for the WalletPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-wallet',
  templateUrl: 'wallet.html',
})
export class WalletPage {
amount:any="";
userid:any;
razorpaysecure:any;
data:any;
transactiondata:any;
payoutdata:any;
loaded:any='0';
transloaded:any='0';
payoutloaded:any='0';
type:any='payin';
loading:any;
newuserid:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public http: Http,private storage: Storage,private alertCtrl: AlertController,public loadingCtrl: LoadingController) {

  }

   ionViewDidLoad() {
    this.storage.get('userid').then((val) => {
      this.userid= val;

      this.storage.get('userdata').then((userdata) => {
        this.newuserid=userdata.userid;
        this.gettransactions(this.newuserid);
        this.getpayout(this.newuserid);
        
      });

      async function delay(ms: number) {
        await new Promise(resolve => setTimeout(()=>resolve(), 1000)).then(()=>console.log("fired"));
    }
  
    delay(500).then(any=>{
  //alert('hh');
  if(this.userid!='nouserid'){
                  this.getdata(this.userid);

  }

  });
    

  });
  }

  add(amount){
    this.amount=amount;

  }
  getdata(id) {
    var link = 'https://api.mmsilks.onstep.in/farmlifeappbackend/walletview.php'; 
    
     var myData = JSON.stringify({userid:  id});
     
    
      
     
     
    this.http.post(link, myData)
    //this.http.get(link)
    
     .subscribe(data => {

     this.data = data["_body"];
     console.log(this.data);


     this.data = JSON.parse(this.data);
this.razorpaysecure=this.data.razorid;
console.log(this.data);
 
this.loaded="1";

     }, error => {
    // alert(id);
     });
     }
  gettransactions(id) {
    var link = 'https://api.mmsilks.onstep.in/farmlifeappbackend/user/transactions.php'; 
    
     var myData = JSON.stringify({id:  id});
     
    
      
     
     
    this.http.post(link, myData)
    //this.http.get(link)
    
     .subscribe(data => {

     this.transactiondata = data["_body"];
     console.log(this.transactiondata);


     this.transactiondata = JSON.parse(this.transactiondata);
console.log(this.transactiondata);
 
this.transloaded="1";

     }, error => {
    // alert(id);
     });
     }
  getpayout(id) {
    var link = 'https://api.mmsilks.onstep.in/farmlifeappbackend/user/payout.php'; 
    
     var myData = JSON.stringify({id:  id});
     
    
      
     
     
    this.http.post(link, myData)
    //this.http.get(link)
    
     .subscribe(data => {

     this.payoutdata = data["_body"];
     console.log(this.payoutdata);


     this.payoutdata = JSON.parse(this.payoutdata);
console.log(this.payoutdata);
 
this.payoutloaded="1";

     }, error => {
    // alert(id);
     });
     }
  addcredit(id,paymentid) {
    var link = 'https://api.mmsilks.onstep.in/farmlifeappbackend/walletadd.php'; 
    
     var myData = JSON.stringify({userid:  id,paymentid: paymentid});
     
    
      
     
     
    this.http.post(link, myData)
    //this.http.get(link)
    
     .subscribe(data => {

     //this.data = data["_body"];
     //console.log(this.data);


     //this.data = JSON.parse(this.data);

     this.getdata(this.userid);
     async function delay(ms: number) {
      await new Promise(resolve => setTimeout(()=>resolve(), 1000)).then(()=>console.log("fired"));
  }

  delay(500).then(any=>{
//alert('hh');
if(this.userid!='nouserid'){
                this.getdata(this.userid);
                this.amount="";
                this.loading.dismiss();

                let pp = this.alertCtrl.create({
                  title: 'Woah !',
                  subTitle: 'Sucessfully wallet Recharged ',
                  buttons: ['Dismiss']
                });
                pp.present(); 
               // alert('sucess');

}

});
this.loaded="1";

     }, error => {
    // alert(id);
     });
     }

  pay() {
    var options = {
      description: 'Amount payable.',
      image: 'http://18.220.1.217/images/logolow.png',
      currency: 'INR',
      key: this.razorpaysecure,
      amount: this.amount*100,
      name: 'Onstep',
      prefill: {
      
     
      },
      external: {
        wallets: ['paytm','mobiwik']
      },
      theme: {
        color: '#ff7b1c'
      },
      modal: {
        ondismiss: function() {
          alert('dismissed')
        }
      }
    };

    var successCallback = (payment_id) => {
      let vm=this;
   //alert('payment_id: ' + payment_id);
   vm.addcredit(vm.userid,payment_id);
   vm.loaded="0";
   vm.amount="";
    vm.loading = this.loadingCtrl.create({
    content: 'Please wait...'
  });

  vm.loading.present();

    // vm.completeorder(payment_id);
   //  alert('sucess');

     

    };

    var cancelCallback = function(error) {
      alert(error.description + ' (Error ' + error.code + ')');
     // alert("failed");
    };

    RazorpayCheckout.open(options, successCallback, cancelCallback);
  }
}

