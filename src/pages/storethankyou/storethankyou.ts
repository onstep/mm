import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import { Platform } from 'ionic-angular';
import { StoreordersPage } from '../../pages/storeorders/storeorders';
import { StoreorderdeatailPage } from '../../pages/storeorderdeatail/storeorderdeatail';


/**
 * Generated class for the StorethankyouPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-storethankyou',
  templateUrl: 'storethankyou.html',
})
export class StorethankyouPage {
  data:any;
  response:any;
  loaded:any="0";
  orderid:any;
  order:any;
  myorderspage:any = StoreorderdeatailPage; 
  public unregisterBackButtonAction: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,public http: Http,public platform: Platform) {
 
  }
  ionViewDidLoad() {
        this.orderid= this.navParams.get('orderid');
    //alert(this.orderid);
this.orderdeatails();

  //  this.initializeBackButtonCustomHandler();
    
}

ionViewWillLeave() {
    // Unregister the custom back button action for this page
  //  this.unregisterBackButtonAction && this.unregisterBackButtonAction();
}
myorders(){
    this.navCtrl.popToRoot() 
   this.navCtrl.push(this.myorderspage, {orderid: this.orderid});

}
gohome(){
    this.navCtrl.popToRoot() 
}

initializeBackButtonCustomHandler(): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(function(event){
        console.log('Prevent Back Button Page Change');
        alert('hghg');
        this.navCtrl.popToRoot() 
    }, 101); // Priority 101 will override back button handling (we set in app.component.ts) as it is bigger then priority 100 configured in app.component.ts file */
}      


 
 orderdeatails() {
  //alert('ss')
    var link = 'https://api.mmsilks.onstep.in/farmlifeappbackend/orderlast.php'; 
    
     var myData = JSON.stringify({orderid:  this.orderid});
     
    
      
     
     
    this.http.post(link, myData)
    //this.http.get(link)
    
     .subscribe(data => {
      console.log(data);

     this.data = data["_body"];
//alert(this.data);
this.data = JSON.parse(this.data);

this.order=this.data.product[0];
 this.loaded="1";

     }, error => {
  console.log(error);
     });
     }
}
