import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import { Component, ViewChild, OnInit, Renderer, Input } from '@angular/core';
import { App,ModalController, ViewController } from 'ionic-angular'; 
import { SharedProvider } from '../../providers/shared/shared';
import { Storage } from '@ionic/storage';
import { StoresubscriptionPage } from '../../pages/storesubscription/storesubscription';
import { ToastController } from 'ionic-angular';
import { Content, Select } from 'ionic-angular';

/**
 * Generated class for the ProductdetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-productdetails',
  templateUrl: 'productdetails.html',
})
export class ProductdetailsPage {
  @ViewChild('mySelect') selectRef: Select;

  productid: any;
   productname: any;
   productunit: any;
 productprice: number;
  productmrp: number;
  stock: number;
  product:any;
  itemlimit: number;
  variants: number;
  variantsproduct: any;
  combo: any;
  combodata: any;
  datum:any;
  datavariants: any;
  optionsloaded:any='0';
 img: any;
  count: any;
  productoffer: any;
loaded:any="0";
data:any;
image:string;
subspage:any=StoresubscriptionPage;
  constructor(public navCtrl: NavController, public navParams: NavParams,public http: Http,public app :App,public shared: SharedProvider,private storage: Storage,public modalCtrl: ModalController,private toastCtrl: ToastController) {
    this.image = 'assets/imgs/logo.png';
 
  }

 

  ionViewDidEnter() {
   
 this.productid=this.navParams.get('data');
this.getproductdetails(this.productid);
  }

  openSelect()
  {
    event.stopPropagation(); //THIS DOES THE MAGIC
    this.selectRef.open();

  }

  closeSelect()
  {
      this.selectRef.close();
  }


  changevariant(id){
   // this.getproductdetails(id);
        this.selectRef.close();

  }

  subscription() {
    let subsmodal = this.modalCtrl.create(this.subspage, { productid: this.productid },{showBackdrop: true});
    subsmodal.present();
  }
  getproductdetails(id) {
    var link = 'http://13.232.62.63/bigcart/mmsilksbackend/home/singleproduct.php'; 
    
     var myData = JSON.stringify({id:id});
     
    
      
     
     
    this.http.post(link, myData)
    //this.http.get(link)
    
     .subscribe(data => {

     this.data = data["_body"];
      

     this.product = JSON.parse(this.data);
      this.datum=this.product.data;
console.log(this.datum);
//  this.productid=this.datum.id;
//  this.productprice= this.datum.price;
//  this.productmrp=this.datum.regular_price;
//  this.stock=this.datum.stock_quantity;
//  this.itemlimit=this.datum.itemlimit;
//  this.variants=this.datum.variants;
//  this.productunit=this.data.finalunit;
//  this.combo=this.data.combo;
//  this.combodata=this.data.product.items;
this.loaded="1";
this.productcount();
// this.getvariants(this.productid);


     }, error => {
    // alert(id);
     });
     }

     getvariants(id) {
       return;
      var link = 'https://api.mmsilks.onstep.in/farmlifeappbackend/variants.php'; 
      
       var myData = JSON.stringify({variantid:  this.variants});
       
      
        
       
       
      this.http.post(link, myData)
      //this.http.get(link)
      
       .subscribe(data => {
  
       this.datavariants = data["_body"];
  //   alert(this.datavariants);
  
     this.variantsproduct = JSON.parse(this.datavariants);
  
   
 this.optionsloaded="1";

       }, error => {
      // alert(id);
       });
       }




     productcount(){

      this.storage.get('storecart').then((val) => {
    //console.log(val);
    var  index = val.findIndex((obj => obj.productid == this.productid));
    
     if(index == -1){
      this.count=0;
    
    }
    else{
    this.count=val[index].count;
    
    }
    });
     }
     
     
     add(event) {
      event.stopPropagation(); //THIS DOES THE MAGIC
     // alert(this.itemlimit);
    
      if(this.count>this.itemlimit-1){
    
    
    let toast = this.toastCtrl.create({
      message: 'You can buy only ' +this.itemlimit+ ' items.',
      duration: 2000,
      position: 'bottom'
    });
    
    
    
    toast.present();
      }else{
    
    
        if(this.count>this.stock-1){
    
          let toast = this.toastCtrl.create({
            message: 'You can buy only ' +this.stock+ ' items.',
            duration: 2000,
            position: 'bottom'
          });
          
          
          
          toast.present();
        
        }else{
          
        this.count++;
    this.addtostorecart(this.productid,this.productprice,this.productmrp);
    
            }
    
      }
    
         
    //setTimeout(this.productcount, 3000)
    
    
    }
     nothing(event) {
      event.stopPropagation(); //THIS DOES THE MAGIC
    
    }
     sub(event) {
    
      event.stopPropagation(); //THIS DOES THE MAGIC
         //  this.count=this.count-1;
    this.count--;
    
         this.reduceinstorecart(this.productid);
        // this.count=this.shared.allproductcount[this.productid];
           
    
         }
    
         addtostorecart(productid,price,mrp){
    
          this.storage.get('storecart').then((val) => {
      
          var  index = val.findIndex((obj => obj.productid == productid));
          //alert(index);
          if(index== -1){
         let item ={productid: productid, price: price,mrp:mrp,count:1};
        
      
        val.push(item),
      this.storage.set('storecart',val).then((val) => {
        var count;
        this.storage.get('storecart').then((vall) => {
        //  console.log(vall);
          var  index = vall.findIndex((obj => obj.productid == productid));
        
           if(index == -1){
           count=0;
          
          }
          else{
         count=vall[index].count;
    
          }
        
       this.count= count;
    
          });
      this.shared.storecart();
      
      });
        
      this.shared.storecart();
      
        var  index = val.findIndex((obj => obj.productid == productid));
        //val[index].count++;
        //this.storage.set('storecart',val);
      // alert(val[index].count);
      
      
      
          }else{
            val[index].count++;
            this.storage.set('storecart',val).then((val) => {
              var count;
              this.storage.get('storecart').then((vall) => {
             //   console.log(vall);
                var  index = vall.findIndex((obj => obj.productid == productid));
              
                 if(index == -1){
                 count=0;
                
                }
                else{
               count=vall[index].count;
          
                }
              
             this.count= count;
          
                });
              this.shared.storecart();
              
              });
          }
          });
         this.shared.storecart();
         
      
      
      
      
        }
    
         reduceinstorecart(productid){
          var count; 
             this.storage.get('storecart').then((val) => {
         
             var  index = val.findIndex((obj => obj.productid == productid));
             //alert(index);
            
               val[index].count--;
               if(val[index].count==0){
                 val.splice(index,1);
               }
               this.storage.set('storecart',val).then((val) => {
         
         
                this.shared.storecart();
    
         
         
                 this.storage.get('storecart').then((val) => {
                //   console.log(val);
                   var  index = val.findIndex((obj => obj.productid == productid));
                 
                    if(index == -1){
                    count=0;
                    this.shared.storecart();
    
                   }
                   else{
                  count=val[index].count;
         
                   }
                 
                this.count= count;
                this.shared.storecart();
    
                   });
         
         
         
         
         
         
                 
         
         
         
         
                 });
             });
             this.shared.storecart();
       
           }
    
    
    
    }
    