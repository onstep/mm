import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MmcategoryPage } from './mmcategory';

@NgModule({
  declarations: [
    MmcategoryPage,
  ],
  imports: [
    IonicPageModule.forChild(MmcategoryPage),
  ],
})
export class MmcategoryPageModule {}
