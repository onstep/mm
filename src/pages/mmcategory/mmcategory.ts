import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ProductlistPage} from '../../pages/productlist/productlist';
// import { MmproductlistPage} from '../../pages/mmproductlist/mmproductlist';

import { Http } from '@angular/http'; //https://stackoverflow.com/questions/43609853/angular-4-and-ionic-3-no-provider-for-http


/**
 * Generated class for the MmcategoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-mmcategory',
  templateUrl: 'mmcategory.html',
})

export class MmcategoryPage {
  category:any;
  loaded:any;
  data:any;
  catg:any;
  categories:any;
  subcategories:any;
  productlisting:any;
  productlist=ProductlistPage;


  // Mmproductlist=MmproductlistPage;

  constructor(public navCtrl: NavController, public navParams: NavParams,public http: Http) {
    // console.log("hii")
  }
 

  ionViewDidLoad() {
    // console.log('ionViewDidLoad MmcategoryPage');
    this.categories=this.navParams.get('subcategory');
    this.subcategories=this.categories.subcategory;
    console.log(this.subcategories);
    

    // this.category=[
    //   { "price":"500", "nameone":"saree",  "nametwo":"saree", "image":"https://mmsilks.com/wp-content/uploads/2019/10/Vinay-Fashion-Sheesha-Harmony-19688-diva-exports-300x300.jpeg","imagetwo":"https://mmsilks.com/wp-content/uploads/2019/09/Suvesh-Lakhnavi-Nx-Chikan-Work-Kurti-Wholesale-Catalog-10-Pcs-4-300x300.jpg","imagethree":"https://mmsilks.com/wp-content/uploads/2019/11/181122233014_0011-300x300.jpg"},
    //   { "price":"600", "nameone":"Kurtis",  "nametwo":"saree", "image":"https://mmsilks.com/wp-content/uploads/2019/10/Vinay-Fashion-Sheesha-Harmony-19688-diva-exports-300x300.jpeg","imagetwo":"https://mmsilks.com/wp-content/uploads/2019/09/Suvesh-Lakhnavi-Nx-Chikan-Work-Kurti-Wholesale-Catalog-10-Pcs-4-300x300.jpg" ,"imagethree":"https://mmsilks.com/wp-content/uploads/2019/11/181122233014_0011-300x300.jpg"},
    //   { "price":"700", "nameone":"Salwar Suits",  "nametwo":"saree", "image":"https://mmsilks.com/wp-content/uploads/2019/10/Vinay-Fashion-Sheesha-Harmony-19688-diva-exports-300x300.jpeg","imagetwo":"https://mmsilks.com/wp-content/uploads/2019/09/Suvesh-Lakhnavi-Nx-Chikan-Work-Kurti-Wholesale-Catalog-10-Pcs-4-300x300.jpg" ,"imagethree":"https://mmsilks.com/wp-content/uploads/2019/11/181122233014_0011-300x300.jpg"},
    //   { "price":"800", "nameone":"Lehenga",  "nametwo":"saree", "image":"https://mmsilks.com/wp-content/uploads/2019/10/Vinay-Fashion-Sheesha-Harmony-19688-diva-exports-300x300.jpeg","imagetwo":"https://mmsilks.com/wp-content/uploads/2019/09/Suvesh-Lakhnavi-Nx-Chikan-Work-Kurti-Wholesale-Catalog-10-Pcs-4-300x300.jpg" ,"imagethree":"https://mmsilks.com/wp-content/uploads/2019/11/181122233014_0011-300x300.jpg"},
    //   { "price":"900", "nameone":"Party Wears",  "nametwo":"saree", "image":"https://mmsilks.com/wp-content/uploads/2019/10/Vinay-Fashion-Sheesha-Harmony-19688-diva-exports-300x300.jpeg","imagetwo":"https://mmsilks.com/wp-content/uploads/2019/09/Suvesh-Lakhnavi-Nx-Chikan-Work-Kurti-Wholesale-Catalog-10-Pcs-4-300x300.jpg" ,"imagethree":"https://mmsilks.com/wp-content/uploads/2019/11/181122233014_0011-300x300.jpg"},
    //   { "price":"800", "nameone":"Handloom Wears",  "nametwo":"saree", "image":"https://mmsilks.com/wp-content/uploads/2019/10/Vinay-Fashion-Sheesha-Harmony-19688-diva-exports-300x300.jpeg","imagetwo":"https://mmsilks.com/wp-content/uploads/2019/09/Suvesh-Lakhnavi-Nx-Chikan-Work-Kurti-Wholesale-Catalog-10-Pcs-4-300x300.jpg" ,"imagethree":"https://mmsilks.com/wp-content/uploads/2019/11/181122233014_0011-300x300.jpg"},
    // ]
  }
   
  subcategory(id){

    this.navCtrl.push(this.productlist,{data:id});
    
  }

}
