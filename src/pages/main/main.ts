import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http'; //https://stackoverflow.com/questions/43609853/angular-4-and-ionic-3-no-provider-for-http
import { SharedProvider } from '../../providers/shared/shared';
import { ImageLoader } from 'ionic-image-loader';
import { Geolocation } from '@ionic-native/geolocation';

/**
 * Generated class for the MainPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-main',
  templateUrl: 'main.html',
})
export class MainPage {
title :any="ravu";
categoryname :any="ravu";
data: any;
catg: any;
go: any=0;
loaded:any="0";
  constructor(public navCtrl: NavController ,public http: Http,public shared: SharedProvider, public imageLoader: ImageLoader,private geolocation: Geolocation) {

  }

  ionViewDidLoad() {
    this.getlocation();

    console.log('ionViewDidLoad MainPage');
  }
  

  getlocation(){

    var options = { enableHighAccuracy: true};

    this.geolocation.getCurrentPosition(options).then((resp) => {
      // resp.coords.latitude
      // resp.coords.longitude

     // alert('latitude:' + resp.coords.latitude +'Logitidue:' + resp.coords.longitude + 'Accuracy: ' + resp.coords.accuracy);
     }).catch((error) => {
       console.log('Error getting location', error);
     });
  }

}
