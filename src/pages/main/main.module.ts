import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MainPage } from './main';
import { ComponentsModule } from '../../components/components.module';
import { IonicImageLoader } from 'ionic-image-loader';
import { Geolocation } from '@ionic-native/geolocation';

@NgModule({
  declarations: [
    MainPage
  ],
  imports: [
    ComponentsModule,
    IonicImageLoader,
    IonicPageModule.forChild(MainPage),
  ],
  providers: [
     Geolocation
  ]
})
export class MainPageModule {}
