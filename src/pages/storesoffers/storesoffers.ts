import { IonicPage, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import { Component, ViewChild } from '@angular/core';
import {  NavController , Slides , Content} from 'ionic-angular';
import { StoresearchPage } from '../../pages/storesearch/storesearch';

/**
 * Generated class for the StoresoffersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-storesoffers',
  templateUrl: 'storesoffers.html',
})
export class StoresoffersPage {
  data: any;
  products: any;
  go: any=0;

  loaded:any=0;
  public images: any;
  page :any='0';
  offername:any;
  offertype:any;
  offercode:any;
  storesearchpage=StoresearchPage;

  constructor(public navCtrl: NavController, public navParams: NavParams,public http: Http) {



  //  this.getproducts(id);
  }
  gostosearch(){

    this.navCtrl.push(this.storesearchpage, { });

  }
  
  ionViewDidEnter(){
    async function delay(ms: number) {
      await new Promise(resolve => setTimeout(()=>resolve(), 1000)).then(()=>console.log("fired"));
  }

  delay(500).then(any=>{
      this.offertype=this.navParams.get('offertype');
       this.offername=this.navParams.get('offername');

//alert('hh');
  this.offercode=this.navParams.get('offerid');
    this.getproducts(this.offercode);
});
  
  }

  getproducts(id) {
    var link = 'https://api.mmsilks.onstep.in/farmlifeappbackend/offerdisplay.php'; 
    
     var myData = JSON.stringify({offerid:  id});
     
    
      
     
     
    this.http.post(link, myData)
    //this.http.get(link)
    
     .subscribe(data => {

     this.data = data["_body"];
     //alert(this.data);
     console.log(this.data);

     this.products = JSON.parse(this.data);

 
this.loaded="1";
     }, error => {
    // alert(id);
     });
     }



  
   
  
}
