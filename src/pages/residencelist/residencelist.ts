import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http'; //https://stackoverflow.com/questions/43609853/angular-4-and-ionic-3-no-provider-for-http
import { Component ,ViewChild} from '@angular/core';
import { Content } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { App, ViewController } from 'ionic-angular';
import { ApptabPage } from '../../pages/apptab/apptab';

/**
 * Generated class for the ResidencelistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-residencelist', 
  templateUrl: 'residencelist.html',
})
export class ResidencelistPage {
  residencetype:any;
  data: any;
  loaded:any="0";
  home:any=ApptabPage;
  constructor(private storage: Storage ,public navCtrl: NavController, public navParams: NavParams,public http: Http, public app :App) {
  }

  ionViewDidLoad() {
    async function delay(ms: number) {
      await new Promise(resolve => setTimeout(()=>resolve(), 1000)).then(()=>console.log("fired"));
  }

    delay(500).then(any=>{
      //alert('hh');
     

            this.getlist('appartment');
        });

   // alert(this.residencetype);
  }

  getlist(type) {
    var link = 'https://api.mmsilks.onstep.in/farmlifeappbackend/residence.php'; 
    
     var myData = JSON.stringify({type: type});
     
    
      
     
     
    this.http.post(link, myData)
    //this.http.get(link)
    
     .subscribe(data => {

     this.data = data["_body"];
    console.log(this.data);

   this.data = JSON.parse(this.data);
      console.log(this.data);

           

  //  console.log(this.catg.subcategory);


this.loaded="1";
     }, error => {
     
     });
     }

     setresidence(id){
              
          
              var residenceid=id;
              this.storage.set('residenceid', residenceid);
              this.navCtrl.setRoot(this.home);
              this.navCtrl.popToRoot() ;

     }

}
