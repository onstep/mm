import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FoodthankyouPage } from './foodthankyou';

@NgModule({
  declarations: [
    FoodthankyouPage,
  ],
  imports: [
    IonicPageModule.forChild(FoodthankyouPage),
  ],
})
export class FoodthankyouPageModule {}
