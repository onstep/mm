import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CartPage } from './cart';
import { SuperTabsModule } from 'ionic2-super-tabs';

@NgModule({
  declarations: [
    CartPage,
  ],
  imports: [
    IonicPageModule.forChild(CartPage),
    SuperTabsModule,
  ],
})
export class CartPageModule {}
