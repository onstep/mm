import { Component, ChangeDetectorRef } from '@angular/core'; //import ChangeDetectorRef
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FoodlistPage } from '../../pages/foodlist/foodlist';
import { FoodcartPage } from '../../pages/foodcart/foodcart';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http'; //https://stackoverflow.com/questions/43609853/angular-4-and-ionic-3-no-provider-for-http
import { SharedProvider } from '../../providers/shared/shared';
import { ImageLoader } from 'ionic-image-loader';
import { ViewChild} from '@angular/core';
import { Content } from 'ionic-angular';
import { StorecartPage } from '../../pages/storecart/storecart';
import { StorecategoryPage } from '../../pages/storecategory/storecategory';
  import { App, ViewController } from 'ionic-angular';
  import { HoteldetailsPage } from '../../pages/hoteldetails/hoteldetails';
  import { FoodProvider } from '../../providers/food/food';
/**
 * Generated class for the FoodPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-food',
  templateUrl: 'food.html',
})
export class FoodPage {
  foodcart:any=FoodcartPage;
  foodlist:any=FoodlistPage;
  menuname:any;
  menuid:any;
  data: any;
  loaded: any="0";
  location: any="madurai";

  foodtypetoogle:any;
  foodtype:any;
    constructor(public navCtrl: NavController, public navParams: NavParams,private storage: Storage,private cdr: ChangeDetectorRef,public http: Http,public shared: SharedProvider,public food: FoodProvider, public imageLoader: ImageLoader,public app :App) {
     
      
  
    }
  
    ionViewDidLoad() {
      console.log('ionViewDidLoad FoodlistPage');
     // alert('jhj');

    }
    ionViewWillEnter	() {
      console.log('ionViewDidLoad FoodlistPage');
    //  alert('jhj');

    }
    ionViewDidEnter() {
  //alert('jhj');
         
      this.storage.get('foodtype').then((val) => {
        if(val=='veg'){
         this.foodtypetoogle=true;
          this.foodtype='veg';
          this.cdr.detectChanges();
  
        }
        else{
         this.foodtypetoogle=false;
    
          this.foodtype='all';
          this.cdr.detectChanges();
  
          }
          this.cdr.detectChanges();
  this.getmenu(this.location,this.foodtype);
      });
  
  
     }
     gofoodcart(){
       this.navCtrl.push(this.foodcart);
     }
     gofoodlist(id,name){
       this.app.getRootNav().push(this.foodlist,{menuid:id,menuname:name});
     }
     updatetoall(){
       
           
      this.storage.set('foodtype', 'all').then((data) => {
        this.loaded="0";

      this.ionViewDidEnter();
        console.log('alled');
      });
    
   // 
   //alert(veg);
    }
  
    updatetoveg(){
  
      this.storage.set('foodtype', 'veg').then((data) => {
        this.loaded="0";
       this.ionViewDidEnter();
        console.log('veged');
  
      });
  //alert
    }
  

    getmenu(loaction,type) {
      var link = 'http://18.220.1.217/food/menu.php'; 
      
       var myData = JSON.stringify({location:  location,type: type});
       
      
        
       
       
      this.http.post(link, myData)
      //this.http.get(link)
      
       .subscribe(data => {
  
       this.data = data["_body"];
       console.log(this.data);

     this.data = JSON.parse(this.data);
     console.log(this.data);

             

    //  console.log(this.catg.subcategory);
  
  
  this.loaded="1";
       }, error => {
       
       });
       }
  
  }
  