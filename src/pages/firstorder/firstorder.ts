import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController,Events } from 'ionic-angular';
import {Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
//import { OTPAutoVerification } from 'cordova-plugin-otp-auto-verification';
import { Storage } from '@ionic/storage';
declare var OTPAutoVerification: any;
//import { AndroidPermissions } from '@ionic-native/android-permissions';
import { Platform } from 'ionic-angular';
import { OneSignal } from '@ionic-native/onesignal';
import { ToastController } from 'ionic-angular';
declare var CashFree: any;
declare var RazorpayCheckout: any;
import {  FormGroup, FormControl } from '@angular/forms';
import { SharedProvider } from '../../providers/shared/shared';
import { LoginPage } from '../../pages/login/login';
import { MembersuccessPage } from '../../pages/membersuccess/membersuccess';
/**
 * Generated class for the FirstorderPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-firstorder',
  templateUrl: 'firstorder.html',
})
export class FirstorderPage {
formdata:any={};
paydata:any={};
config:any={};
data:any={};
loaded:any='0';
token:any;
amount:any;
cfInitialized:any;
langs;
langForm;
langForm1;
constructor(public shared: SharedProvider,public toastCtrl: ToastController,public navCtrl: NavController, public navParams: NavParams,public http: Http,public loadingCtrl: LoadingController,private storage: Storage, public events: Events,public plt: Platform,public oneSignal: OneSignal) {
  
  this.langForm = new FormGroup({
    "langs": new FormControl({value: 'boiled', disabled: false})
  });
  this.langForm1 = new FormGroup({
    "langs": new FormControl({value: 'synergy', disabled: false})
  });
  this.storage.get('register').then((val) => {
    if(val){
this.formdata=val;
this.storage.get('userdata').then((val2) => {
  if(val2){
    this.formdata.userdata=val2;
    console.log('next');
    console.log(this.formdata);

   this.verifydetails();

  }

   });

//this.register();

    }

     });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FirstorderPage');
  }
  pay2() {
    var data = {};
    this.paydata.orderId = "121";
    this.paydata.orderAmount = 6500;
    this.paydata.customerName = "Seth";
    this.paydata.customerPhone = "8667378458";
    this.paydata.customerEmail = "ravisankar300@gmail.com";
    this.paydata.returnUrl = "https://mysite.com/payment/response";
    this.paydata.notifyUrl = "https://mysite.com/payment/notify";
    this.paydata.appId = "5820ac3d0451532dc41c438c0285";
    this.paydata.paymentToken = this.token;
    data=this.paydata;
  console.log(data);
    var callback = function (event) {
        var eventName = event.name;
        switch(eventName) {
          case "PAYMENT_REQUEST":
             console.log(event.message);
             break;
          default:
             console.log(event.message);
         };
    }

    var config = {};
    this.config.layout = {view: "popup", width: "650"};
    this.config.mode = "TEST"; //use PROD when you go live
    var response = CashFree.init(this.config);
    if (response.status == "OK") {
      this.cfInitialized = true;
    } else {
      //handle error
       console.log(response.message);
    }
    // Make sure you put id of your payment button that triggers the payment flow in the below statement.
      if (this.cfInitialized) {
        CashFree.makePayment(data, callback);
      }
  
   ;
    
  }
    verifydetails(){

      var myData = JSON.stringify({mobile:  'mobile'});

      
      var link = 'https://api.mmsilks.onstep.in/farmlifeappbackend/user/token.php';
    
    
    
    
    
        this.http.post(link,myData)
        //this.http.get(link)
    
         .subscribe(data => {
          console.log(data);
    
         this.data = data["_body"];
      //alert(this.data);
      console.log(this.data);
    
         this.data = JSON.parse(this.data);
      console.log(this.data);
      if(this.data.status=='success'){
       // alert("sucecss");
      // this.pay(this.data.token);
       this.token=this.data.token;
       this.amount=this.data.amount;

       // this.engineerid=this.data.id;
        this.loaded='1';
        //this.ngOnInit();
        //this.openSuccessCancelSwal();
    
      }
    
         }, error => {
      this.data.status='failed';
         this.loaded='0';
    
         });
         } 
         gohome(){

          this.navCtrl.popToRoot();
          this.navCtrl.push(MembersuccessPage);

         }
    register(rzpid){
      this.formdata.rice_option=this.langForm.value.langs;
      this.formdata.combo=this.langForm1.value.langs;
      this.formdata.paymentid=rzpid;
      this.formdata.entrytype=this.formdata.userdata.entrytype;
      let loading = this.loadingCtrl.create({
        content: ' Please wait...'
      });
      
      loading.present();
      this.formdata.razorpayid='1';

      var myData = JSON.stringify(this.formdata);

      //alert(JSON.stringify(this.formdata));
      if(this.formdata.entrytype=='vsp'){
              var link = 'https://api.mmsilks.onstep.in/farmlifeappbackend/user/completevso.php';

      }else{
              var link = 'https://api.mmsilks.onstep.in/farmlifeappbackend/user/completeuser.php';

      }

    //  var link = 'https://api.mmsilks.onstep.in/farmlife/auth/onstepadd.php';
    
    
    
    
    
        this.http.post(link,myData)
        //this.http.get(link)
    
         .subscribe(data => {
          console.log(data);
    
         this.data = data["_body"];
      //alert(this.data);
      console.log(this.data);
    
         this.data = JSON.parse(this.data);
     // alert(this.data);
      console.log(this.data);
      if(this.data.status=='success'){
       // alert(this.data.customer_id);
       // this.storage.set('userid', this.data.customer_id);
        this.storage.set('userdata', this.data.userdata[0]);
        
     this.shared.userid=this.data.customer_id;
     this.shared.mobile=this.formdata.mobile;
     this.shared.status='active';
     this.events.publish('app:loaduserdata');
     this.shared.getstatus();

    // this.navCtrl.push(LoginPage);
this.gohome();

loading.dismiss();
// this.pay(this.data.token);
//this.toast('sucess');
this.toast(this.data.statusmessage);

       // this.engineerid=this.data.id;
        this.loaded='1';
        //this.ngOnInit();
        //this.openSuccessCancelSwal();
    
      }else{
        this.toast(this.data.statusmessage);
        this.loaded='1';
loading.dismiss();
      }
    
         }, error => {
          this.toast('server error');
          loading.dismiss();

      this.data.status='failed';
         this.loaded='1';
    
         });
         } 
         doSubmit(event) {
          console.log('Submitting form', this.langForm.value);
          event.preventDefault();
        }
         doSubmit1(event) {
          console.log('Submitting form', this.langForm1.value);
          event.preventDefault();
        }
       
        ricechange(){

        }
         pay() {
           console.log(this.langForm.value);
           this.formdata.rice_option=this.langForm.value.langs;
           this.formdata.combo=this.langForm1.value.langs;
                      console.log(this.formdata);

          // alert(this.langForm.value.langs);
          var options = {
            description: 'Amount payable.',
            image: 'http://18.220.1.217/images/logolow.png',
            currency: 'INR',
           // key: 'rzp_test_enFay33iw1itRL',
            key: this.token,
            amount: this.amount*100,
            name: 'FARM Life',
            prefill: {
            
           
            },
            external: {
              wallets: ['mobiwik']
            },
            theme: {
              color: '#ff7b1c'
            },
            modal: {
              ondismiss: function() {
              //  alert('dismissed')
              this.toast('dismissed');
              }
            }
          };
      
          var successCallback = (payment_id) => {
            let vm=this;
        // alert('payment_id: ' + payment_id);
          // vm.completeorder(payment_id);
         //  alert('sucess');
         vm.toast('sucess, payment id: '+payment_id);
    vm.register(payment_id);
           
      
          };
      
          var cancelCallback = function(error) {
           // alert(error.description + ' (Error ' + error.code + ')');
           // alert("failed");
           let vm=this;
           vm.toast('error.description');

          };
      
          RazorpayCheckout.open(options, successCallback, cancelCallback);
        }

  toast(text) {
    const toast = this.toastCtrl.create({
      message: text,
      duration: 5000,
      position:'bottom'
    });
    toast.present();
  }
        
}
