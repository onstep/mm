import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import { StoresearchresultsPage } from '../../pages/storesearchresults/storesearchresults';
import {Component, Input, ViewChild} from '@angular/core';

/**
 * Generated class for the StoresearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-storesearch',
  templateUrl: 'storesearch.html',
})
export class StoresearchPage {
  @ViewChild('input') myInput ;

data:any;
loaded:any='0';
str:any;
resultpage:any=StoresearchresultsPage;
  constructor(public navCtrl: NavController, public navParams: NavParams,public http: Http) {
    //this.myInput.setFocus();
    setTimeout(() => {
  // this.myInput.setFocus();
      },550);
  } 

  ionViewDidEnter() {
    setTimeout(() => {
     this.myInput.setFocus();
    },150);
    console.log('ionViewDidLoad StoresearchPage');
  }
  search(ev: any) {
    
    // set val to the value of the searchbar
    const val = ev.target.value;

    // if the value is an empty string don't call
    if (val && val.trim() != '') {
     // console.log(val);
   this.searchnext(val);
    }
  }
  gotoresults(sid,keywords){
 // StoresearchresultsPage
  this.navCtrl.push(this.resultpage,{sid: sid,keyword:keywords});  


}
  searchnext(str){
this.str=str;
 var link = 'https://api.mmsilks.onstep.in/farmlifeappbackend/search.php'; 
 
  
  var myData = JSON.stringify({search: str});

   
  
  
 this.http.post(link, myData)
 //this.http.get(link)
 
  .subscribe(data => {

  this.data = data["_body"];
  console.log(this.data);

 this.data = JSON.parse(this.data);
if(this.data.status=='sucess'){
console.log(this.data);
//alert("sucess");
this.loaded='1';
}

  }, error => {


  });
  
  

}

}
