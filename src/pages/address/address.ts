import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import { AddaddressPage } from '../../pages/addaddress/addaddress';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the AddressPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-address',
  templateUrl: 'address.html',
})
export class AddressPage {
  data:any;
  response:any;
  loaded:any="0";
  userid:any;
  addaddresspage:any=AddaddressPage;

  constructor(public navCtrl: NavController, public navParams: NavParams,public http: Http,private storage: Storage) {
   
      
      
      
        
  }

  ionViewDidLoad() {
     this.storage.get('userid').then((val) => {
      this.userid= val;
   // alert(this.userid);
    console.log('ionViewDidLoad AddressPage');
    this.addresslink();
  });
  }
  ionViewWillEnter(){
    console.log(' AddressPage');
//this.ionViewDidLoad();
  }
  goaddaddress(){
  var pop= this.navParams.get('pop');
 // alert(pop);
this.navCtrl.push(this.addaddresspage,{pop: pop});  
  }
addresslink() {
  //alert('ss')
    var link = 'https://api.mmsilks.onstep.in/farmlifeappbackend/addresslist.php'; 
    
     var myData = JSON.stringify({userid:  this.userid});
     
    
      
     
     
    this.http.post(link, myData)
    //this.http.get(link)
    
     .subscribe(data => {

     this.data = data["_body"];
//alert(this.data);
this.data = JSON.parse(this.data);

if(this.data.status=='noaddress'){
  //this.navCtrl.pop();
 this.goaddaddress();

}
else{


if(this.data.status=='sucess'){
 console.log(this.data);

}

}
 this.loaded="1";

     }, error => {
    // alert(id);
     });
     }

     deleteaddress(addressid) {
       //alert(addressid);
      var link = 'https://api.mmsilks.onstep.in/farmlifeappbackend/deleteaddress.php'; 
      
       var myData = JSON.stringify({userid:  this.userid, id:addressid});
       
      
        
       
       
      this.http.post(link, myData)
      //this.http.get(link)
      
       .subscribe(data => {
  
       this.data = data["_body"];
 //alert(this.data);
  this.response = JSON.parse(this.data);
  
  if(this.response.status=='sucess'){
    this.ionViewDidLoad();
  
  }
 
   this.loaded="1";
  
       }, error => {
      // alert(id);
       });
       }

     activeaddress(addressid) {
     //alert(addressid);
      var link = 'https://api.mmsilks.onstep.in/farmlifeappbackend/addressactive.php'; 
      
       var myData = JSON.stringify({userid:  this.userid, id:addressid});
       
      
        
       
       
      this.http.post(link, myData)
      //this.http.get(link)
      
       .subscribe(data => {
  
       this.data = data["_body"];
// alert(this.data);
  this.response = JSON.parse(this.data);
  
  if(this.response.status=='sucess'){
    var pop= this.navParams.get('pop');
if(pop=='1'){
    this.ionViewDidLoad();

}
if(pop=='2'){
    this.ionViewDidLoad();
    this.navCtrl.pop();

}

  
  }
 
   this.loaded="1";
  
       }, error => {
      // alert(id);
       });
       }
}
