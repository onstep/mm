import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import { App,ModalController, ViewController } from 'ionic-angular'; 
import { StoresubscriptionPage } from '../../pages/storesubscription/storesubscription';

/**
 * Generated class for the ForstoresubscribtionlistbydayPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-forstoresubscribtionlistbyday',
  templateUrl: 'forstoresubscribtionlistbyday.html',
})
export class ForstoresubscribtionlistbydayPage {
  loaded:any="0";
  data:any;
  userid:any;
  day:any;
  subspage:any=StoresubscriptionPage;

  constructor(public navCtrl: NavController, public navParams: NavParams,public http: Http,private storage: Storage,public modalCtrl: ModalController) {
  }


  ionViewDidLoad() {
    this.day=this.navParams.get('day');
//alert(this.day);
    this.storage.get('userid').then((val) => {
      this.userid= val;
        this.getdata(this.userid,this.day);

  });
  }
  subscription(id) {
    let subsmodal = this.modalCtrl.create(this.subspage, { productid: id },{showBackdrop: true});
    subsmodal.present();
  }
  getdata(userid,day) {
    var link = 'https://api.mmsilks.onstep.in/farmlifeappbackend/subscriptionday.php'; 
    
     var myData = JSON.stringify({userid:  userid,day: day});
     
    
      
     
     
    this.http.post(link, myData)
    //this.http.get(link)
    
     .subscribe(data => {

     this.data = data["_body"];
     console.log(this.data);


     this.data = JSON.parse(this.data);

console.log(this.data);
 
this.loaded="1";

     }, error => {
    // alert(id);
     });
     }
}
