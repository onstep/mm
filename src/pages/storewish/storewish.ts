import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SharedProvider } from '../../providers/shared/shared';
import { Http } from '@angular/http';
import {  ViewChild } from '@angular/core';
import { StorecheckoutPage } from '../../pages/storecheckout/storecheckout';
import { MobileregPage } from '../../pages/mobilereg/mobilereg';
import { Storage } from '@ionic/storage';
import { NewcheckoutPage } from '../../pages/newcheckout/newcheckout';
import { MemberintroPage } from '../../pages/memberintro/memberintro';

/**
 * Generated class for the StorewishPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-storewish',
  templateUrl: 'storewish.html',
})
export class StorewishPage {
  data: any;
  go: any;
  loaded: any="0";
  userid:any;

  products: any=[];
  cartfill: any;
  cartids:any=[];
  storecheckout=StorecheckoutPage;
  mobilepage:any=MobileregPage;

  constructor(public shared: SharedProvider,public navCtrl: NavController, public navParams: NavParams,public http: Http,private storage: Storage) {
  }

  ionViewDidLoad() {
//alert(this.shared.status);
    this.getcartdetails(); 
    this.checkstatus();

  }
  checkstatus(){
    this.storage.get('userdata').then((val) => {
     // alert(val.status);
      if(val){
        this.shared.status=val.status;

      }
     });
  }
  gostorecheckout(){
    this.storage.get('userid').then((val) => {
      this.userid= val;
      if(this.userid=='nouserid'){
      
      this.navCtrl.push(this.mobilepage);
      }else{
          this.navCtrl.push(this.storecheckout, { });

      }
          });

  }
  becomemember(){
    
          this.navCtrl.push(MemberintroPage, { });

  }
  getcartdetails() {
   
  console.log(this.shared.storecartdetails.all);
var cartarray=this.shared.storecartdetails.all;
for(var i=0;i<cartarray.length; i++){
 
  this.cartids.push( cartarray[i].productid );


   }
   if(this.cartids.length>0)
   {
     this.cartfill='yes';
    var link = 'https://api.mmsilks.onstep.in/farmlifeappbackend/addcart.php'; 
    //console.log(this.cartids);
     var myData = JSON.stringify({productid:  this.cartids});
     
    
      
     
     
    this.http.post(link, myData)
    //this.http.get(link)
    
     .subscribe(data => {

     this.data = data["_body"];
  console.log(this.data); 

    this.products = JSON.parse(this.data);
    //console.log(this.products.records);

    
    for(var i=0;i<this.products.records.length; i++){
// console.log(this.products.records[i].price);
    this.shared.updatecart(this.products.records[i].pid,this.products.records[i].price,this.products.records[i].mrp);
    
    
       }
 
    this.loaded="1";

     }, error => {
    // alert(id);
     });
     }
     else{
      this.cartfill='no';

     }

    }
    gohome(){
      this.navCtrl.popToRoot() 
  }
}



