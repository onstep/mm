import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StorewishPage } from './storewish';

@NgModule({
  declarations: [
    StorewishPage,
  ],
  imports: [
    IonicPageModule.forChild(StorewishPage),
  ],
})
export class StorewishPageModule {}
