import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http'; //https://stackoverflow.com/questions/43609853/angular-4-and-ionic-3-no-provider-for-http
import { SharedProvider } from '../../providers/shared/shared';
import { ImageLoader } from 'ionic-image-loader';
import { Component ,ViewChild} from '@angular/core';
import { Content } from 'ionic-angular';
import { StorecartPage } from '../../pages/storecart/storecart';
import { StoresoffersPage } from '../../pages/storesoffers/storesoffers';
import { StoresearchPage } from '../../pages/storesearch/storesearch';
import { StorecategoryPage } from '../../pages/storecategory/storecategory';
  import { App, ViewController } from 'ionic-angular'; 
  import { ForstoreproductlistPage } from '../../pages/forstoreproductlist/forstoreproductlist';
  import { ProductdetailsPage } from '../../pages/productdetails/productdetails';
  import { Storage } from '@ionic/storage';
  import { MemberintroPage } from '../../pages/memberintro/memberintro';
  import { MembervsoPage } from '../../pages/membervso/membervso';
  import { MmcategoryPage} from '../../pages/mmcategory/mmcategory';
  import {mmsubcategory} from '../../pages/mmsubcategory/mmsubcategory';
/**
 * Generated class for the ShopPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-shop',
  templateUrl: 'shop.html',
})
export class ShopPage {
  @ViewChild(Content) content: Content;
  title :any="ravu";
  categoryname :any="ravu";
  data: any;
  userdata: any;
  userdataload: any='0';
  catg: any;
  go: any=0;
  loaded:any="0";
  productlist=ForstoreproductlistPage;
  productdetails=ProductdetailsPage;
  storecartpage=StorecartPage;
  storesearchpage=StoresearchPage;
  storecategorypage=StorecategoryPage;
  storeoffers=StoresoffersPage;
  placeimg:any="assets/imgs/logo.png";
  category:any;
  products:any;
  banner:any;
  featured:any={};
  Mmcategory=MmcategoryPage;
  


    constructor(private storage: Storage,public navCtrl: NavController ,public http: Http,public shared: SharedProvider, public imageLoader: ImageLoader,public app :App) {
     // this.shared.store();
  //this.shared.addtostorecart('1','90','100');
  //this.shared.reduceinstorecart('10');

  this.storage.get('userdata').then((val) => {
    this.userdata= val;

    this.userdataload='1';
    
 


  

});

   this.shared.in='shop'; 
}
    gostorecart(){

      this.app.getRootNav().push(this.storecartpage, { });
  
    }
    gostorecategory(){

      this.app.getRootNav().push(this.storecategorypage, { });
  
    }
  
    gotooffers(id,type){

      this.app.getRootNav().push(this.storeoffers, {offerid: id,offertype: type });
  
    }
    becomeamember(skip)
    {
      this.app.getRootNav().push(MemberintroPage,{skip:skip});
    
     } 
    becomevso(skip)
    {
      this.app.getRootNav().push(MembervsoPage,{skip:skip});
    
     } 

    gobanner(type,id){
      
if(type=='offer'){
  this.app.getRootNav().push(this.storeoffers, {offerid: id,offertype: type });

}

if(type=='subscription'){
  this.app.getRootNav().push(this.storeoffers, {offerid: id,offertype: type });

}

if(type=='product'){
  this.app.getRootNav().push(this.productdetails, {productid: id});

}
if(type=='subcategory'){
  this.app.getRootNav().push(this.productlist, {subcategoryid: id});

}


    }
    // subcategories(){
    //   this.app.getRootNav().push(this.Mmcategory);
    //   }
  
    gostosearch(){

      this.app.getRootNav().push(this.storesearchpage, { });
  
    }
  
    ionViewDidEnter() {
     // console.log('ionViewDidLoad MainPage');
     this.initial();
      // this.getposts();
      this.storage.get('userdata').then((val) => {
        this.userdata= val;
    
        this.userdataload='1';
        
        if(this.userdata.status=='lead'){
          this.storage.get('joinpage').then((shown) => {
     
     if(shown=='yes'){
    
     }else{
       this.storage.set('joinpage','yes');


       if(this.userdata.entrytype!='vso'){
       this.becomeamember('yes');
       }else{
       this.becomevso('yes');

       }
     }
          });
    
        }
    
    
      
    
    });
    }
    
    subcategories(id) {
      var link = 'http://13.232.62.63/bigcart/mmsilksbackend/home/subcategorylisting.php'; 
    
      
       var myData = JSON.stringify({id:  id});
      this.http.post(link, myData)
      //this.http.get(link)
      
       .subscribe(data => {
  
       this.data = data["_body"];
    // console.log(this.data);
    

       this.catg = JSON.parse(this.data);
            // console.log(this.catg.status);

            if(this.catg.status=="success"){    

    //  console.log(this.catg.subcategory);
    this.navCtrl.push(this.Mmcategory,{subcategory:this.catg});
    }
    else{
      return;
    }

  
  this.loaded="1"
       }, error => {
       
       });
       }
      //  subcategories(id) {
      // console.log(this.catg.subcategory);
      // }

       
       initial() {
      var link = 'http://13.232.62.63/bigcart/mmsilksbackend/home/index.php'; 
      
       var myData = JSON.stringify({location:  'all'});
 
      this.http.post(link, myData)
      //this.http.get(link)
      
       .subscribe(data => {
  
       this.data = data["_body"];
      // console.log(this.data);

       this.catg = JSON.parse(this.data);
       this.products=this.catg.categories;
      //  console.log(this.products);
       this.banner=this.catg.banner;
      //  console.log(this.banner);
       this.featured=this.catg.featuredproduct;


              
    //  console.log(this.catg.subcategory);
  
  
  this.loaded="1"
       }, error => {
       
       });
       }
   
       ionViewDidLoad() {
        console.log('ionViewDidLoad MmhomePage');
    
       this.category=[
          { "price":"500", "nameone":"saree",  "nametwo":"saree", "image":"https://mmsilks.com/wp-content/uploads/2019/10/Vinay-Fashion-Sheesha-Harmony-19688-diva-exports-300x300.jpeg","imagetwo":"https://mmsilks.com/wp-content/uploads/2019/09/Suvesh-Lakhnavi-Nx-Chikan-Work-Kurti-Wholesale-Catalog-10-Pcs-4-300x300.jpg","imagethree":"https://mmsilks.com/wp-content/uploads/2019/11/181122233014_0011-300x300.jpg"},
          { "price":"600", "nameone":"Kurtis",  "nametwo":"saree", "image":"https://mmsilks.com/wp-content/uploads/2019/10/Vinay-Fashion-Sheesha-Harmony-19688-diva-exports-300x300.jpeg","imagetwo":"https://mmsilks.com/wp-content/uploads/2019/09/Suvesh-Lakhnavi-Nx-Chikan-Work-Kurti-Wholesale-Catalog-10-Pcs-4-300x300.jpg" ,"imagethree":"https://mmsilks.com/wp-content/uploads/2019/11/181122233014_0011-300x300.jpg"},
          { "price":"700", "nameone":"Salwar Suits",  "nametwo":"saree", "image":"https://mmsilks.com/wp-content/uploads/2019/10/Vinay-Fashion-Sheesha-Harmony-19688-diva-exports-300x300.jpeg","imagetwo":"https://mmsilks.com/wp-content/uploads/2019/09/Suvesh-Lakhnavi-Nx-Chikan-Work-Kurti-Wholesale-Catalog-10-Pcs-4-300x300.jpg" ,"imagethree":"https://mmsilks.com/wp-content/uploads/2019/11/181122233014_0011-300x300.jpg"},
          { "price":"800", "nameone":"Lehenga",  "nametwo":"saree", "image":"https://mmsilks.com/wp-content/uploads/2019/10/Vinay-Fashion-Sheesha-Harmony-19688-diva-exports-300x300.jpeg","imagetwo":"https://mmsilks.com/wp-content/uploads/2019/09/Suvesh-Lakhnavi-Nx-Chikan-Work-Kurti-Wholesale-Catalog-10-Pcs-4-300x300.jpg" ,"imagethree":"https://mmsilks.com/wp-content/uploads/2019/11/181122233014_0011-300x300.jpg"},
          { "price":"900", "nameone":"Party Wears",  "nametwo":"saree", "image":"https://mmsilks.com/wp-content/uploads/2019/10/Vinay-Fashion-Sheesha-Harmony-19688-diva-exports-300x300.jpeg","imagetwo":"https://mmsilks.com/wp-content/uploads/2019/09/Suvesh-Lakhnavi-Nx-Chikan-Work-Kurti-Wholesale-Catalog-10-Pcs-4-300x300.jpg" ,"imagethree":"https://mmsilks.com/wp-content/uploads/2019/11/181122233014_0011-300x300.jpg"},
          { "price":"800", "nameone":"Handloom Wears",  "nametwo":"saree", "image":"https://mmsilks.com/wp-content/uploads/2019/10/Vinay-Fashion-Sheesha-Harmony-19688-diva-exports-300x300.jpeg","imagetwo":"https://mmsilks.com/wp-content/uploads/2019/09/Suvesh-Lakhnavi-Nx-Chikan-Work-Kurti-Wholesale-Catalog-10-Pcs-4-300x300.jpg" ,"imagethree":"https://mmsilks.com/wp-content/uploads/2019/11/181122233014_0011-300x300.jpg"},
        ]
      }
      
  }
  