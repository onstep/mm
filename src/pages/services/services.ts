import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import { ServicecategoryPage } from '../../pages/servicecategory/servicecategory';
import { App, ViewController } from 'ionic-angular';
import { SharedProvider } from '../../providers/shared/shared';

/**
 * Generated class for the ServicesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-services',
  templateUrl: 'services.html',
})
export class ServicesPage {
  loaded:any="0";
  data: any;
categorypage:any=ServicecategoryPage;
  constructor(public shared: SharedProvider,public navCtrl: NavController, public navParams: NavParams,public http: Http,public app :App) {
    this.shared.in='services'; 

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ServicesPage');
        this.loadservicehome();

  }
  gocategory(id){
    this.app.getRootNav().push(this.categorypage, {categoryid: id });


  }

  loadservicehome() {
    var link = 'http://18.220.1.217/services/home.php'; 
    
     var myData = JSON.stringify({location:  'all'});
     
    
      
     
     
    this.http.post(link, myData)
    //this.http.get(link)
    
     .subscribe(data => {

     this.data = data["_body"];
       console.log(this.data);

     this.data = JSON.parse(this.data);



this.loaded="1";
     }, error => {
     
     });
     }
}
