import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { FoodloaderPage } from '../../pages/foodloader/foodloader';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  page1: any = FoodloaderPage;
  page2: any = FoodloaderPage;
  page3: any = FoodloaderPage;

  constructor(public navCtrl: NavController) {

  }

}
