import { Component } from '@angular/core';
import { IonicPage,LoadingController,NavController,ViewController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import { MobileregPage } from '../../pages/mobilereg/mobilereg';
import { AlertController } from 'ionic-angular';
 import { AddressPage } from '../../pages/address/address';
 import { AddaddressPage } from '../../pages/addaddress/addaddress';

/**
 * Generated class for the StoresubscriptionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
 
@Component({
  selector: 'page-storesubscription',
  templateUrl: 'storesubscription.html',
})
export class StoresubscriptionPage {

  item:any;
  data:any;
  sdata:any;
  pdata:any;
  loaded:any='0';
  productdeatailsloaded:any='0';

  userid:any;
  address:any;
  mobilepage:any=MobileregPage;

  productid: any;
   productname: any;
   productunit: any;
 productprice: number;
  productmrp: number;
 img: any;
 day: any;
 addresspage:any=AddaddressPage;

  //startdate: any='12/12/2001'
  constructor( public viewCtrl: ViewController,public navCtrl: NavController, public navParams: NavParams,public http: Http,public loadingCtrl: LoadingController,private storage: Storage,private alertCtrl: AlertController) {
    this.productid=this.navParams.get('productid');
 

  }

  ionViewDidEnter(){
    this.productid=this.navParams.get('productid');
    this.storage.get('userid').then((val) => {
      this.userid= val;
     
      
   
    
    var tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate()+1);

  this.day=formatDate(tomorrow);
function formatDate(date) {
  var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

  if (month.length < 2) month = '0' + month;
  if (day.length < 2) day = '0' + day;

  return [year, month, day].join('-');
}

  this.item ={userid: this.userid,productid: this.productid,sunday:1,monday:1,tuesday:1,wednesday:1,thursday:1,friday:1,saturday:1,status: 'active',startdate: this.day};

this.getdata();

  });

  }


  dismiss() {
    this.viewCtrl.dismiss();
  }

  ionViewDidLoad() {
    this.getproductdetails(1);
    console.log('ionViewDidLoad StoresubscriptionPage');
  }


  

  
  getproductdetails(id) {
    var link = 'https://api.mmsilks.onstep.in/farmlifeappbackend/product.php'; 
    
     var myData = JSON.stringify({pid:  this.productid});
     
    
      
     
     
    this.http.post(link, myData)
    //this.http.get(link)
    
     .subscribe(data => {

     this.data = data["_body"];
      
//alert(this.data);
console.log(this.data);

     this.pdata = JSON.parse(this.data);
 this.productid=this.pdata.product[0].pid;
 this.productprice= this.pdata.product[0].price;
 this.productname= this.pdata.product[0].name;
 this.productunit= this.pdata.product[0].finalunit;
 this.img= this.pdata.product[0].imagelow;
 this.productmrp=this.pdata.product[0].mrp;
this.productdeatailsloaded="1";

     }, error => {
    // alert(id);
     });
     }

  subscribe() {

    if(this.userid=='nouserid'){
      
      this.navCtrl.push(this.mobilepage);

    }  
    else{
console.log(this.address);
      if(this.address=='no'){
        //  this.navCtrl.push(this.addresspage,{pop :1});
        
          let alert = this.alertCtrl.create({
            title: 'ADD ADDRESS',
            message: 'Add your address',
            buttons: [
              {
                text: 'Cancel',
                role: 'cancel',
                handler: () => {
                  console.log('Cancel clicked');
                  this.navCtrl.pop();
                }
              },
              {
                text: 'Add',
                handler: () => {
                  this.navCtrl.push(this.addresspage,{pop :1});
        
                  console.log('Buy clicked');
                }
              }
            ]
          });
          alert.present();
        
        }
        else{

        

    let loading = this.loadingCtrl.create({
      spinner: 'ios',
      content: 'Updating Subscription..',
    });
    
    loading.present();
    //console.log(this.item['startdate']);
    var link = 'https://api.mmsilks.onstep.in/farmlifeappbackend/subscription.php'; 
    
     var myData = JSON.stringify(this.item);
     
     console.log(this.item);
    
      
     
     
    this.http.post(link, myData)
    //this.http.get(link)
    
     .subscribe(data => {

     this.data = data["_body"];
     //  alert(this.data);

   this.data = JSON.parse(this.data);
if(this.data.status=='sucess'){

  this.getdata();
  loading.dismiss();

  let sucess = this.alertCtrl.create({
    title: 'Subscribed !!',
    subTitle: 'Well deliverd the product as per the shedule before 7 A.M.',
    buttons: ['ok']
  });
  sucess.present();

}
 
//this.loaded="1";
     }, error => {
    // alert(id);
     });
     }
    }
  }
      unsubscribe() {
     // console.log(this.item['startdate']);
     let loading = this.loadingCtrl.create({
      spinner: 'ios',
      content: 'Unsubscribing..',
    });
    
    loading.present();
      var link = 'https://api.mmsilks.onstep.in/farmlifeappbackend/stopsubscription.php'; 
      
       var myData = JSON.stringify({userid: this.userid,productid: this.productid});
       
      
        
       
       
      this.http.post(link, myData)
      //this.http.get(link)
      
       .subscribe(data => {
  
        this.data = data["_body"];
        // alert(this.data);
     
        this.data = JSON.parse(this.data);
     if(this.data.status=='sucess'){
       
      this.item ={userid: this.userid,productid: this.productid,sunday:1,monday:1,tuesday:1,wednesday:1,thursday:1,friday:1,saturday:1,status: 'active',startdate: this.day};

       this.getdata();
       loading.dismiss();


     }
  //this.loaded="1";
       }, error => {
      // alert(id);
       });
       }
      

     getdata() {
     // console.log(this.item['startdate']);
      var link = 'https://api.mmsilks.onstep.in/farmlifeappbackend/viewsubscription.php'; 
      
       var myData = JSON.stringify({userid: this.userid, productid: this.productid});
       
      
        
       
       
      this.http.post(link, myData)
      //this.http.get(link)
      
       .subscribe(data => {
  
       this.sdata = data["_body"];
   // alert(this.data);
      console.log(this.sdata);

    this.sdata = JSON.parse(this.sdata);

   // alert(this.sdata.product.sunday);
   this.address=this.sdata.address;

   if(this.sdata.status=='sucess'){
        this.item ={userid: this.sdata.product.userid,productid: this.sdata.product.productid,sunday:this.sdata.product.sunday,monday:this.sdata.product.monday,tuesday:this.sdata.product.tuesday,wednesday:this.sdata.product.wednesday,thursday:this.sdata.product.thursday,friday:this.sdata.product.friday,saturday:this.sdata.product.saturday,startdate: this.sdata.product.startdate,status: this.sdata.product.status};

   }

  this.loaded="1";
       }, error => {
      // alert(id);
       });
       }
  
  add(day){
  //  console.log('hhh');

    this.item[day]++;
    console.log(this.item[day]);
  }

  sub(day){
    //console.log('hhh');
if(this.item[day]==0){

}else{
    this.item[day]--;
    console.log(this.item);
}
  

  }
  
  createRange(number){
    var items: number[] = [];
    for(var i = 1; i <= number; i++){
       items.push(i);
    }
    return items;
  }




 


}
