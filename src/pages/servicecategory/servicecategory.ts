import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ServicesubcategoryPage } from '../../pages/servicesubcategory/servicesubcategory';
import { Http } from '@angular/http';

/**
 * Generated class for the ServicecategoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-servicecategory',
  templateUrl: 'servicecategory.html',
})
export class ServicecategoryPage {
subcatgegory:any=ServicesubcategoryPage;
data:any;
loaded:any="0";
categoryid:any;
selected:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public http: Http) {
    this.categoryid=this.navParams.get('categoryid');
    this.selected=this.categoryid-1;
  }
 
  ionViewDidLoad() {
   // console.log('ionViewDidLoad ServicecategoryPage');
  }
  onTabSelect(ev: any) {
  //  console.log('Tab selected', 'Index: ' + ev.title, 'Unique ID: ' + ev.id);
  }
}
