import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import { AddressPage } from '../../pages/address/address'
import { Storage } from '@ionic/storage';

/**
 * Generated class for the AddaddressPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-addaddress',
  templateUrl: 'addaddress.html',
})
export class AddaddressPage {
  data:any;
  newdata:any;
  new:any=[];
  loaded:any="0"
  userid:any;
  addresspage:any=AddressPage;
  constructor(public navCtrl: NavController, public navParams: NavParams,public http: Http,private storage: Storage) {
  }

  ionViewDidLoad() {
    this.storage.get('userid').then((val) => {
      this.userid= val;
        this.getappartments();

  });
  }
  getappartments() { 
    var link = 'https://api.mmsilks.onstep.in/farmlifeappbackend/getappartment.php'; 
    
     var myData = JSON.stringify({city:  'madurai'});
     
    
      
     
     
    this.http.post(link, myData)
    //this.http.get(link)
    
     .subscribe(data => {

     this.data = data["_body"];
//alert(this.data);
this.data = JSON.parse(this.data);





 this.loaded="1";

     }, error => {
    // alert(id);
     });
     }

     

     

  postnewaddress() {
    var link = 'https://api.mmsilks.onstep.in/farmlifeappbackend/addaddress.php'; 
    
    var aa = JSON.stringify({userid:  this.userid,name:  this.new.name,line1:  this.new.line1,line2:  this.new.line2,line3:  this.new.appartment});
     
  // console.log(aa);
      
     
     
    this.http.post(link, aa)
    //this.http.get(link)
    
     .subscribe(data => {

     this.newdata = data["_body"];
console.log(this.newdata);
this.newdata = JSON.parse(this.newdata);


if(this.newdata.status=='sucess'){

  this.navCtrl.pop();


}



     }, error => {
    // alert(id);
     });
     }
}
