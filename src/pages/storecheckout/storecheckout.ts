import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import { AddressPage } from '../../pages/address/address';
import { AddaddressPage } from '../../pages/addaddress/addaddress';
import { StorepaymentPage } from '../../pages/storepayment/storepayment';
import { SharedProvider } from '../../providers/shared/shared';
import { Storage } from '@ionic/storage';
import { AlertController } from 'ionic-angular';
import { StorethankyouPage } from '../../pages/storethankyou/storethankyou';

/**
 * Generated class for the StorecheckoutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
 
@Component({
  selector: 'page-storecheckout',
  templateUrl: 'storecheckout.html',
})
export class StorecheckoutPage {
data:any;
loaded:any="0";
address:any;
slott:any;
slotid:any=0;
userid:any;
deliverytype:any="typeslot";
addaddresspage:any=AddaddressPage;
addresspage:any=AddressPage;
paymentpage:any=StorepaymentPage;
dates:any;
expressdeliverycharge;
expressdeliverychargediscount;
finalexpressdeliverycharge;
myColor: string = 'slotcolor';
razorpaysecure:any;
onlinepay:any;
wallet:any;
mobile:any;
id:any;
userdata:any;
paymenttype:any='online';
thankyou:any=StorethankyouPage;
amount:any=this.navParams.get('amount');
cart:any=this.navParams.get('cart');
 addressid:any=this.navParams.get('addressid');
 totalmrp:any=this.navParams.get('totalmrp');
  loadingCtrl: any;


constructor(public shared: SharedProvider,public navCtrl: NavController, public navParams: NavParams,public http: Http,private storage: Storage,private alertCtrl: AlertController) {
  this.day('1');
    }
  

  ionViewDidLoad() {
    


  }
  toggleColor() {
    this.myColor = 'slotcolor';
}
  ionViewDidEnter(){
   // this.ionViewDidLoad(); 
   this.day('1');

   this.storage.get('mobile').then((val) => {
  this.mobile=val;
   
        });

this.storage.get('userid').then((val) => {
      this.userid= val;
      if(this.userid=='nouserid'){
        
      }else{
      
      }
          });

this.storage.get('userdata').then((val) => {
      if(val){
        this.userdata=val;
        this.slot();

      }
          });
           console.log('ionViewDidLoad StorecheckoutPage');
    var cartarray=this.shared.storecartdetails.all;
console.log(this.shared.storecartdetails.all);

  }
  gotoaddresspage(){
    this.navCtrl.push(this.addaddresspage,{pop :1});
  }
  deliverytypeselect(type){
    this.deliverytype=type;
  }

   day(date) {
    var d = new Date(date);
    var weekday = new Array(7);
    weekday[0] = "Sunday";
    weekday[1] = "Monday";
    weekday[2] = "Tuesday";
    weekday[3] = "Wednesday";
    weekday[4] = "Thursday";
    weekday[5] = "Friday";
    weekday[6] = "Saturday";

    var n = weekday[d.getDay()];
    return n;
}

  slot() {
    var link = 'https://api.mmsilks.onstep.in/farmlifeappbackend/getaddress.php'; 
    
     var myData = JSON.stringify({id:  this.userdata.id});
     
   // alert(this.userid);
      
     
     
    this.http.post(link, myData)
    //this.http.get(link)
    
     .subscribe(data => {

     this.data = data["_body"];
 //alert(this.data);
 console.log(this.data);

     this.data = JSON.parse(this.data);
if(this.data.status=='success'){

  this.razorpaysecure=this.data.razorid;
  this.onlinepay=this.data.onlinepay;
  this.wallet=this.data.userdata.wallet;
  this.expressdeliverycharge=this.data.deliverycharge;
  this.expressdeliverychargediscount=this.data.deliverydiscount;
  this.finalexpressdeliverycharge=this.expressdeliverycharge-this.expressdeliverychargediscount;

//this.dates=this.data.data[0];
console.log(this.data);
 //alert(this.address);


 //this.slott='2018-06-16';
 //alert(this.slott);
 console.log(this.data);
}
if(this.data.status=='noaddress'){
//  this.navCtrl.push(this.addresspage,{pop :1});


  let alert = this.alertCtrl.create({
    title: 'ADD ADDRESS',
    message: 'Add your address',
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
          this.navCtrl.pop();
        }
      },
      {
        text: 'Add',
        handler: () => {
          this.navCtrl.push(this.addaddresspage,{pop :1});

          console.log('Buy clicked');
        }
      }
    ]
  });
  alert.present();

}
 this.loaded="1";

     }, error => {
    // alert(id);
     });
     }
     
 
  gopayment(){
    this.slotid='0';
this.navCtrl.push(this.paymentpage,{userid:this.userid,cart: this.shared.storecartdetails.all,deliverytype: this.deliverytype,amount:this.shared.storecartdetails.price.toFixed(0),totalmrp:this.shared.storecartdetails.mrp,expressdeliverycharge: this.expressdeliverycharge,expressdeliverychargediscount: this.expressdeliverychargediscount,razorpaysecure:this.razorpaysecure,onlinepay:this.onlinepay,wallet: this.wallet,mobile: this.mobile} );
//var myy = JSON.stringify({userid:this.userid,cart: this.shared.storecartdetails.all,addressid: this.address.id,deliverytype: this.deliverytype, slotid: this.slotid,amount:this.shared.storecartdetails.price,totalmrp:this.shared.storecartdetails.mrp});
//alert(this.shared.storecartdetails.mrp);
//console.log(myy);
  }


  placeorder(){
    if(this.paymenttype=='cod'){
      this.codconfirm();
    }
    if(this.paymenttype=='online'){
this.pay();
    }
  }

  codconfirm() {
    let alert = this.alertCtrl.create({
      title: 'Confirm Order',
      message: 'Do you want to place this order on COD?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Buy',
          handler: () => {
            console.log('Buy clicked');
            this.completeorder('0');
          }
        }
      ]
    });
    alert.present();
  }

  pay() {
    var options = {
      description: 'Amount payable.',
      image: 'http://18.220.1.217/images/logolow.png',
      currency: 'INR',
      key: this.razorpaysecure,
      amount: this.amount*100,
      name: 'Onstep',
      prefill: {
      
     
      },
      external: {
        wallets: ['paytm','mobiwik']
      },
      theme: {
        color: '#ff7b1c'
      },
      modal: {
        ondismiss: function() {
          alert('dismissed')
        }
      }
    };

    var successCallback = (payment_id) => {
      let vm=this;
    // alert('payment_id: ' + payment_id);
     vm.completeorder(payment_id);
   //  alert('sucess');

     

    };

    var cancelCallback = function(error) {
      alert(error.description + ' (Error ' + error.code + ')');
     // alert("failed");
    };

    RazorpayCheckout.open(options, successCallback, cancelCallback);
  }

  completeorder(razorid){
    console.log('ionViewDidLoad StorepaymentPage');
    let loading = this.loadingCtrl.create({
     content: 'Please wait...'
   });
   loading.present();

       setTimeout(() => {
       loading.dismiss();
     }, 20000);
 var link = 'https://api.mmsilks.onstep.in/farmlifeappbackend/order.php'; 
 
//  alert(razorid);


  var myData = JSON.stringify({userdata:this.userdata,mobile: this.mobile,userid:this.userid,cart: this.cart,addressid: this.addressid,deliverytype: this.deliverytype, slotid: this.slotid,amount:this.amount,totalmrp:this.totalmrp,paymenttype:this.paymenttype,expressdeliverychargediscount:this.expressdeliverychargediscount,expressdeliverycharge:this.expressdeliverycharge,razorpayid: razorid,firstorder:'no'});

   
  
  
 this.http.post(link, myData)
 //this.http.get(link)
 
  .subscribe(data => {

  this.data = data["_body"];
console.log(this.data);

  this.data = JSON.parse(this.data);
if(this.data.status=='sucess'){
//alert("sucess");
loading.dismiss();


this.storage.get('storecart').then((val) => {
//console.log(val);
this.shared.removestorecart();
this.navCtrl.popToRoot() ;
this.navCtrl.push(this.thankyou,{orderid: this.data.orderid});

 


});
}

  }, error => {


  });
  
  

}
}
