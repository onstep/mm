import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController,Events } from 'ionic-angular';
import {Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
//import { OTPAutoVerification } from 'cordova-plugin-otp-auto-verification';
import { Storage } from '@ionic/storage';
declare var OTPAutoVerification: any;
//import { AndroidPermissions } from '@ionic-native/android-permissions';
import { Platform } from 'ionic-angular';
import { OneSignal } from '@ionic-native/onesignal';
import { ToastController } from 'ionic-angular';
import { FirstorderPage} from '../../pages/firstorder/firstorder';
import { AlertController } from 'ionic-angular';

/**
 * Generated class for the UpdateaddressPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-updateaddress',
  templateUrl: 'updateaddress.html',
})
export class UpdateaddressPage {
  new:any={};
  error:any;
  error2:any;
  panerror:any;
  aadharerror:any;
  mobileerror:any;
  formdata:any={};
  userid:any;
  disablebutton:any;
  loaded:any;
  data:any;
  address:any;
  sponserdata:any;
  orderpage:any=FirstorderPage;
  constructor(private alertCtrl: AlertController,public toastCtrl: ToastController,public navCtrl: NavController, public navParams: NavParams,public http: Http,public loadingCtrl: LoadingController,private storage: Storage, public events: Events,public plt: Platform,public oneSignal: OneSignal) {
   this.address='no';
   this.storage.get('mobile').then((val) => {
    if(val){
      this.formdata.mobile=val;
      this.storage.get('userdata').then((val2) => {
        if(val2){
        console.log(val2);
          this.formdata.userdata=val2;
          console.log('next');
          console.log(this.formdata);

                this.checksponser();


        }
    
         });
    }

     });
    
   
  }
  
    ionViewDidLoad() {
      console.log('ionViewDidLoad RegistrationPage');
  
  var today = new Date();
  today = new Date('1 Jan 1995');
  this.formdata.dob = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    }
  
  
    toast(text) {
      const toast = this.toastCtrl.create({
        message: text,
        duration: 3000,
        position:'bottom'
      });
      toast.present();
    }
  
    update() {
      console.log('clicked');
  
      //return;
     
   
      if(!this.formdata.name || this.formdata.name==''){
       this.error='Invalid Name!';
       this.disablebutton=false;
       this.toast(this.error);
  
       return;
      }
      if(!this.formdata.line1 || this.formdata.line1==''){
       this.error='Invalid Flat/House/Office No.!';
       this.disablebutton=false;
       this.toast(this.error);
  
       return;
      }
      if(!this.formdata.line2 || this.formdata.line2==''){
       this.error='Invalid Street/Society/Office name!';
       this.disablebutton=false;
       this.toast(this.error);
  
       return;
      }
      if(!this.formdata.line3 || this.formdata.line3==''){
       this.error='Invalid Locality/Area!';
       this.disablebutton=false;
       this.toast(this.error);
  
       return;
      }
      if(!this.formdata.city || this.formdata.city==''){
       this.error='Invalid City!';
       this.disablebutton=false;
       this.toast(this.error);
  
       return;
      }
      if(!this.formdata.pincode || this.formdata.pincode==''){
       this.error='Invalid Pincode!';
       this.disablebutton=false;
       this.toast(this.error);
  
       return;
      }

  
   
  
     
  this.error=null;
  
  let loading = this.loadingCtrl.create({
    content: ' Please wait...'
  });
  
  loading.present();
  
         // this.router.navigate(['/workers/addworker/'+this.engineerid]);
  
         this.loaded='1';
         this.disablebutton=true;
      this.formdata.companyid=this.userid;
    
      var link = 'https://api.mmsilks.onstep.in/farmlifeappbackend/user/updateaddress.php';
      //  var link = 'https://api.mmsilks.onstep.in/farmlifeappbackend/user/checksponsor.php';

    
         var myData = JSON.stringify(
          {
            id: this.formdata.userdata.id,
            first_name: this.formdata.name,
            last_name: this.formdata.surname,
            customer_phone: this.formdata.mobile,
            address: this.formdata.line1,
            address2: this.formdata.line2,
            address3: this.formdata.line3,
            city: this.formdata.city ,
            state: this.formdata.state,
            country: 'india',
            pincode: this.formdata.pincode,
            longitude: '-',
            latitude: '-',
            sponsor_name:  this.formdata.refname,
            sponsor_id: this.formdata.refid,
            sponsor_mobile : this.formdata.refmobile,
            aadhar_no: this.formdata.aadhar,
            bank_name: this.formdata.bank,
            account_no: this.formdata.accountnumber,
            branch: this.formdata.branch,
            ifsc_code: this.formdata.ifsc,
            unit_key: 11204,
            rice_option: 'ravi',
            customer_type: 'customer',
            country_code: 91,
            customer_active_status: '1',
            combo_active_status: '1'
          }
         );
         console.log(myData);
    
    
    
  
    
        this.http.post(link,myData)
        //this.http.get(link)
    
         .subscribe(data => {
          console.log(data);
    
         this.data = data["_body"];
      //alert(this.data);
      console.log(this.data);
    
         this.data = JSON.parse(this.data);
      console.log(this.data);
      if(this.data.status=='success'){
        //this.toast('sucess');
       // this.data = JSON.parse(this.data);
       this.events.publish('app:loaduserdata');

        loading.dismiss();
  
    let alertcontrol = this.alertCtrl.create({
      title: 'Updated Successfully !',
      subTitle: 'Upcoming orders will delivered to new address.',
      buttons: ['Okay']
    });
    alertcontrol.present();
    this.navCtrl.pop();
  
          // this.navCtrl.push(this.orderpage);
      //  this.formdata={};
       // this.engineerid=this.data.id;
        this.loaded='0';
  
        //this.ngOnInit();
        //this.openSuccessCancelSwal();
    
      }
      if(this.data.status=='Failure'){
        loading.dismiss();
  
        this.toast(this.data.statusmessage);
      }
    
         }, error => {
          loading.dismiss();
          this.toast('Check your internet connection');
  
      this.data.status='failed';
         this.loaded='0';
    
         });
  
  
        
     
  
  
    
         } 
  
      
  
  
         checksponser(){
       
          var link = 'https://api.mmsilks.onstep.in/farmlifeappbackend/user/getuserdata.php';
       
          var myData = JSON.stringify({id:  this.formdata.userdata.id});
  
             console.log(myData);
        
        
        
        
        
            this.http.post(link,myData)
            //this.http.get(link)
        
             .subscribe(data => {
              console.log(data);
        
             this.sponserdata = data["_body"];
          //alert(this.data);
          console.log(this.sponserdata);
        
             this.sponserdata = JSON.parse(this.sponserdata);
          console.log(this.sponserdata);
          if(this.sponserdata.status=='success'){
           // alert("sucecss");
         
           this.formdata.name=this.sponserdata.records.name
           this.formdata.line1=this.sponserdata.records.line1
           this.formdata.line2=this.sponserdata.records.line2
           this.formdata.line3=this.sponserdata.records.line3
           this.formdata.city =this.sponserdata.records.city
           this.formdata.pincode=this.sponserdata.records.pincode

  
     
     
          
           // this.engineerid=this.data.id;
            this.loaded='1';
            //this.ngOnInit();
            //this.openSuccessCancelSwal();
        
          }
        
             }, error => {
          this.data.status='failed';
             this.loaded='0';
        
             });
             
            }
  
  
  }
  
