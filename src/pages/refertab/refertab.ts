import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { ReferafriendPage } from '../../pages/referafriend/referafriend';
import { ReferreportPage } from '../../pages/referreport/referreport';

/**
 * Generated class for the RefertabPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-refertab',
  templateUrl: 'refertab.html',
})
export class RefertabPage {
  refer:any=ReferafriendPage;
  report:any=ReferreportPage;

  selected:any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.selected=this.navParams.get('day');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad StoresubscribtionlistbydayPage');
  }

}
