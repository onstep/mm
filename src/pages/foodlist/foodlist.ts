import { Component, ChangeDetectorRef } from '@angular/core'; //import ChangeDetectorRef
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ForfoodlistPage } from '../../pages/forfoodlist/forfoodlist';
import { FoodcartPage } from '../../pages/foodcart/foodcart';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the FoodlistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({ 
  selector: 'page-foodlist',
  templateUrl: 'foodlist.html',
})
export class FoodlistPage {
forfoodlist:any=ForfoodlistPage;
foodcart:any=FoodcartPage;
menuname:any;
menuid:any;

foodtypetoogle:any;
foodtype:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,private storage: Storage,private cdr: ChangeDetectorRef) {
    this.menuname=this.navParams.get('menuname');
    this.menuid=this.navParams.get('menuid');

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FoodlistPage');
  }
  ngAfterViewInit() {

       
    this.storage.get('foodtype').then((val) => {
      if(val=='veg'){
       this.foodtypetoogle=true;
        this.foodtype='veg';
        this.cdr.detectChanges();

      }
      else{
       this.foodtypetoogle=false;
  
        this.foodtype='all';
        this.cdr.detectChanges();

        }
        this.cdr.detectChanges();

    });


   }
   gofoodcart(){
     this.navCtrl.push(this.foodcart);
   }
   updatetoall(){
     
         
    this.storage.set('foodtype', 'all').then((data) => {
    this.ngAfterViewInit();
      console.log('alled');
    });
  
 // 
 //alert(veg);
  }

  updatetoveg(){

    this.storage.set('foodtype', 'veg').then((data) => {
     this.ngAfterViewInit();
      console.log('veged');

    });
//alert
  


//alert('changed');

}

}
