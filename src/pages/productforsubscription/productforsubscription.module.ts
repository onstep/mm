import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProductforsubscriptionPage } from './productforsubscription';

@NgModule({
  declarations: [
    ProductforsubscriptionPage,
  ],
  imports: [
    IonicPageModule.forChild(ProductforsubscriptionPage),
  ],
})
export class ProductforsubscriptionPageModule {}
