import { IonicPage, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import { Component, ViewChild } from '@angular/core';
import {  NavController , Slides , Content} from 'ionic-angular';
/**
 * Generated class for the ProductlistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-storesearchresults',
  templateUrl: 'storesearchresults.html',
})
export class StoresearchresultsPage {
 
  data: any;
  products: any;
  go: any=0;

  loaded:any=0;
  public images: any;
  page :any='0';
  constructor(public navCtrl: NavController, public navParams: NavParams,public http: Http) {

var id=this.navParams.get('subcategoryid');
var id=this.navParams.get('subcategoryid');
  //  this.getproducts(id);
  }
  
  
  ionViewDidEnter(){
    async function delay(ms: number) {
      await new Promise(resolve => setTimeout(()=>resolve(), 1000)).then(()=>console.log("fired"));
  }

  
  delay(500).then(any=>{
//alert('hh');
  var id=this.navParams.get('sid');
  var keyword=this.navParams.get('keyword');
    this.getproducts(id,keyword);
});
  
  }
back(){
  this.navCtrl.pop();

  }
  getproducts(id,keyword) {
    var link = 'https://api.mmsilks.onstep.in/farmlifeappbackend/searchresults.php'; 
    
     var myData = JSON.stringify({subcategory:  id,keyword: keyword});
     
    
      
     
     
    this.http.post(link, myData)
    //this.http.get(link)
    
     .subscribe(data => {

     this.data = data["_body"];
     //alert(this.data);
     console.log(this.data);

     this.products = JSON.parse(this.data);

 
this.loaded="1";
     }, error => {
    // alert(id);
     });
     }



  
   
  
}
