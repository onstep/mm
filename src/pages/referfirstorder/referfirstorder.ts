import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController,Events } from 'ionic-angular';
import {Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
//import { OTPAutoVerification } from 'cordova-plugin-otp-auto-verification';
import { Storage } from '@ionic/storage';
declare var OTPAutoVerification: any;
//import { AndroidPermissions } from '@ionic-native/android-permissions';
import { Platform } from 'ionic-angular';
import { OneSignal } from '@ionic-native/onesignal';
import { ToastController } from 'ionic-angular';
declare var CashFree: any;
declare var RazorpayCheckout: any;
import {  FormGroup, FormControl } from '@angular/forms';
import { SharedProvider } from '../../providers/shared/shared';
import { ShopPage } from '../../pages/shop/shop';

/**
 * Generated class for the ReferfirstorderPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-referfirstorder',
  templateUrl: 'referfirstorder.html',
})
export class ReferfirstorderPage {
  formdata:any={};
  paydata:any={};
  config:any={};
  data:any={};
  loaded:any='0';
  token:any;
  cfInitialized:any;
  langs;
  langForm;
  constructor(public shared: SharedProvider,public toastCtrl: ToastController,public navCtrl: NavController, public navParams: NavParams,public http: Http,public loadingCtrl: LoadingController,private storage: Storage, public events: Events,public plt: Platform,public oneSignal: OneSignal) {

    this.langForm = new FormGroup({
      "langs": new FormControl({value: 'boiled', disabled: false})
    });
    this.storage.get('referregister').then((val) => {
      console.log(val);
      if(val){
  this.formdata=val;
  this.loaded='1';
  //this.register();
  
      }
  
       });
    }
  
    ionViewDidLoad() {
      console.log('ionViewDidLoad FirstorderPage');
    }
    pay2() {
      var data = {};
      this.paydata.orderId = "121";
      this.paydata.orderAmount = 6500;
      this.paydata.customerName = "Seth";
      this.paydata.customerPhone = "8667378458";
      this.paydata.customerEmail = "ravisankar300@gmail.com";
      this.paydata.returnUrl = "https://mysite.com/payment/response";
      this.paydata.notifyUrl = "https://mysite.com/payment/notify";
      this.paydata.appId = "5820ac3d0451532dc41c438c0285";
      this.paydata.paymentToken = this.token;
      data=this.paydata;
    console.log(data);
      var callback = function (event) {
          var eventName = event.name;
          switch(eventName) {
            case "PAYMENT_REQUEST":
               console.log(event.message);
               break;
            default:
               console.log(event.message);
           };
      }
  
      var config = {};
      this.config.layout = {view: "popup", width: "650"};
      this.config.mode = "TEST"; //use PROD when you go live
      var response = CashFree.init(this.config);
      if (response.status == "OK") {
        this.cfInitialized = true;
      } else {
        //handle error
         console.log(response.message);
      }
      // Make sure you put id of your payment button that triggers the payment flow in the below statement.
        if (this.cfInitialized) {
          CashFree.makePayment(data, callback);
        }
    
     ;
      
    }
      verifydetails(){
  
        var myData = JSON.stringify({mobile:  'mobile'});
  
        
        var link = 'https://api.mmsilks.onstep.in/farmlife/registration/checksum.php';
      
      
      
      
      
          this.http.post(link,myData)
          //this.http.get(link)
      
           .subscribe(data => {
            console.log(data);
      
           this.data = data["_body"];
        //alert(this.data);
        console.log(this.data);
      
           this.data = JSON.parse(this.data);
        console.log(this.data);
        if(this.data.status=='success'){
         // alert("sucecss");
        // this.pay(this.data.token);
         this.token=this.data.token;
  
         // this.engineerid=this.data.id;
          this.loaded='0';
          //this.ngOnInit();
          //this.openSuccessCancelSwal();
      
        }
      
           }, error => {
        this.data.status='failed';
           this.loaded='0';
      
           });
           } 
      register(){
        let loading = this.loadingCtrl.create({
          content: ' Please wait...'
        });
        
        loading.present();
  
        var myData = JSON.stringify(this.formdata);
  
        //alert(JSON.stringify(this.formdata));
        var link = 'https://api.mmsilks.onstep.in/farmlife/auth/onstepadd.php';
      
      
      
      
      
          this.http.post(link,myData)
          //this.http.get(link)
      
           .subscribe(data => {
            console.log(data);
      
           this.data = data["_body"];
        //alert(this.data);
        console.log(this.data);
      
           this.data = JSON.parse(this.data);
       // alert(this.data);
        console.log(this.data);
        if(this.data.status=='Success'){
          loading.dismiss();
          this.navCtrl.setRoot(ShopPage);
            this.navCtrl.popToRoot(); 

       // this.pay(this.data.token);
  this.toast('sucess');
  this.toast(this.data.statusmessage);


  
         // this.engineerid=this.data.id;
          this.loaded='1';
          //this.ngOnInit();
          //this.openSuccessCancelSwal();
      
        }else{
          this.toast(this.data.statusmessage);
          this.loaded='1';
  loading.dismiss();
        }
      
           }, error => {
            this.toast('server error');
            loading.dismiss();
  
        this.data.status='failed';
           this.loaded='1';
      
           });
           } 
           doSubmit(event) {
            console.log('Submitting form', this.langForm.value);
            event.preventDefault();
          }
         
          ricechange(){
  
          }
           pay() {
             console.log(this.langForm.value);
             this.formdata.rice_option=this.langForm.value.langs;
                        console.log(this.formdata);
  
            // alert(this.langForm.value.langs);
            var options = {
              description: 'Amount payable.',
              image: 'http://18.220.1.217/images/logolow.png',
              currency: 'INR',
              key: 'rzp_test_enFay33iw1itRL',
              amount: 6500*100,
              name: 'FARM Life',
              prefill: {
              
             
              },
              external: {
                wallets: ['mobiwik']
              },
              theme: {
                color: '#ff7b1c'
              },
              modal: {
                ondismiss: function() {
                //  alert('dismissed')
                this.toast('dismissed');
                }
              }
            };
        
            var successCallback = (payment_id) => {
              let vm=this;
          // alert('payment_id: ' + payment_id);
            // vm.completeorder(payment_id);
           //  alert('sucess');
           vm.toast('sucess, payment id: '+payment_id);
       vm.register();
             
        
            };
        
            var cancelCallback = function(error) {
             // alert(error.description + ' (Error ' + error.code + ')');
             // alert("failed");
             let vm=this;
             vm.toast('error.description');
  
            };
        
            RazorpayCheckout.open(options, successCallback, cancelCallback);
          }
  
    toast(text) {
      const toast = this.toastCtrl.create({
        message: text,
        duration: 5000,
        position:'bottom'
      });
      toast.present();
    }
          
  }
  