import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import { Component, ViewChild, OnInit, Renderer, Input } from '@angular/core';
import { App,ModalController, ViewController } from 'ionic-angular'; 
import { SharedProvider } from '../../providers/shared/shared';
import { Storage } from '@ionic/storage';
import { StoresubscriptionPage } from '../../pages/storesubscription/storesubscription';
import { ToastController } from 'ionic-angular';
import { Content, Select } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
/**
 * Generated class for the StoreorderdeatailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-storeorderdeatail',
  templateUrl: 'storeorderdeatail.html',
})

export class StoreorderdeatailPage {
  @ViewChild('mySelect') selectRef: Select;

  orderid: any;
loaded:any="0";
data:any;
canceldata:any;
x:any;

  constructor(public navCtrl: NavController, public navParams: NavParams,public http: Http,public app :App,public shared: SharedProvider,private storage: Storage,public modalCtrl: ModalController,private toastCtrl: ToastController,private alertCtrl: AlertController,public loadingCtrl: LoadingController) {

  }

 

  ionViewDidEnter() {
   
 this.orderid=this.navParams.get('orderid');
//this.orderid="66";
this.getproductdetails(this.orderid);
//alert(this.orderid);
  }

  


  getproductdetails(id) {
    var link = 'https://api.mmsilks.onstep.in/farmlifeappbackend/orderview.php'; 
    
    var myData = JSON.stringify({orderid:  this.orderid});
     
    
      
     
     
    this.http.post(link, myData)
    //this.http.get(link)
    
     .subscribe(data => {

     this.data = data["_body"];
      
console.log(this.data);

     this.data = JSON.parse(this.data);
this.x=this.data.order[0];
console.log(this.data.product);
//console.log(this.data);
 //this.productid=this.data.product[0].pid;

this.loaded="1";


     }, error => {
    // alert(id);
     });
     }

  cancel() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();

        setTimeout(() => {
        loading.dismiss();
      }, 20000);
    var link = 'https://api.mmsilks.onstep.in/farmlifeappbackend/cancelorder.php'; 
    
     var myData = JSON.stringify({id:  this.orderid});
     
    
      
     
     
    this.http.post(link, myData)
    //this.http.get(link)
    
     .subscribe(data => {

     this.canceldata = data["_body"];
      
console.log(this.canceldata);

     this.canceldata = JSON.parse(this.canceldata);
if(this.canceldata.status=='sucess'){
  
  this.ionViewDidEnter();
  loading.dismiss();

}
//console.log(this.data);
 //this.productid=this.data.product[0].pid;

this.loaded="1";


     }, error => {
    // alert(id);
     });
     }


     confirmcancel() {
      let alert = this.alertCtrl.create({
        title: 'Are you sure ! ',
        message: 'Do you want to cancel this order ?',
        buttons: [
          {
            text: 'No',
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
            }
          },
          {
            text: 'Yes',
            handler: () => {
              console.log('Buy clicked');
              this.cancel();
            }
          }
        ]
      });
      alert.present();
    }
  

    
    
    }
    