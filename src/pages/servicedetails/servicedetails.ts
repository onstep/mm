import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SharedProvider } from '../../providers/shared/shared';
import { Http } from '@angular/http';
import { StorecheckoutPage } from '../../pages/storecheckout/storecheckout';
import { MobileregPage } from '../../pages/mobilereg/mobilereg';
import { ServicebookingPage } from '../../pages/servicebooking/servicebooking';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the ServicedetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-servicedetails',
  templateUrl: 'servicedetails.html',
})
export class ServicedetailsPage {
  data: any;
  go: any;
  loaded: any="0";
  userid:any;
  mobilepage:any=MobileregPage;
  booking:any=ServicebookingPage;
 serviceid:any;
 servicename:any;
  constructor(public shared: SharedProvider,public navCtrl: NavController, public navParams: NavParams,public http: Http,private storage: Storage) {
    this.serviceid=this.navParams.get('serviceid');
    this.servicename=this.navParams.get('servicename');

  }

  ionViewDidEnter() {
    this.serviceid=this.navParams.get('serviceid');
    this.getservices(this.serviceid); 
    console.log('ionViewDidLoad ServicedetailsPage');
  }

    booknow(){
    this.storage.get('userid').then((val) => {
      this.userid= val;
      if(this.userid=='nouserid'){
      
     this.navCtrl.push(this.mobilepage);
      }else{
       this.navCtrl.push(this.booking, {serviceid: 1 , servicename: this.servicename});
      }
          });

  }

  getservices(id) {
    var link = 'http://18.220.1.217/services/servicedetails.php'; 
   // alert(id);
     var myData = JSON.stringify({serviceid:  id});
     
    
      
     
     
    this.http.post(link, myData)
    //this.http.get(link)
    
     .subscribe(data => {

     this.data = data["_body"];
         console.log(this.data);

     this.data = JSON.parse(this.data);
        


this.loaded="1";
     }, error => {
    // alert(id);
     });
     }
}

