import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { NewregistrationPage } from '../../pages/newregistration/newregistration';
import htmlToImage from 'html-to-image';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import { AlertController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
/**
 * Generated class for the MemberintroPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-membervso',
  templateUrl: 'membervso.html',
})
export class MembervsoPage {
  amount:any="";
  userid:any;
  razorpaysecure:any;
  data:any;
  loaded:any='0';
  loading:any;
  skip:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public http: Http,private storage: Storage,private alertCtrl: AlertController,public loadingCtrl: LoadingController) {
this.getdata();
this.skip=this.navParams.get('skip');

  }
  skipback(){
    this.navCtrl.pop();
  }
click(){
  var node = document.getElementById('my-node');
  htmlToImage.toPng(node)
  .then(function (dataUrl) {
    var img = new Image();
    img.src = dataUrl;
    document.body.appendChild(img);
    console.log(img.src);
 //   window.open(img.src);

  //  window.open(img.src,'_blank');

  })
  .catch(function (error) {
    console.error('oops, something went wrong!', error);
  });
    }
  ionViewDidLoad() {
    console.log('ionViewDidLoad MemberintroPage');
  }

  goregister(){
    this.navCtrl.push(NewregistrationPage);

  }
  
  getdata() {
    var link = 'https://api.mmsilks.onstep.in/farmlifeappbackend/joiningcombo.php'; 
    
     var myData = JSON.stringify({userid:  'sd'});
     
    
      
     
     
    this.http.post(link, myData)
    //this.http.get(link)
    
     .subscribe(data => {

     this.data = data["_body"];
     console.log(this.data);


     this.data = JSON.parse(this.data);
console.log(this.data);
 
this.loaded="1";

     }, error => {
    // alert(id);
     });
     }
}
