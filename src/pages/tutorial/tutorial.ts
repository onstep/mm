import { Component } from '@angular/core';
import { IonicPage, MenuController, NavController, Platform } from 'ionic-angular';

import { TranslateService } from '@ngx-translate/core';
import { MobileregPage } from '../../pages/mobilereg/mobilereg';
import { Storage } from '@ionic/storage';
import { ViewChild } from '@angular/core';
import { Slides } from 'ionic-angular';
export interface Slide {
  title: string;
  description: string;
  image: string;
}

@Component({
  selector: 'page-tutorial',
  templateUrl: 'tutorial.html'
})
export class TutorialPage {
  s: Slide[];
  showSkip = true;
  dir: string = 'ltr';
  @ViewChild('slides') slides: Slides;

  constructor(private storage: Storage,public navCtrl: NavController, public menu: MenuController, translate: TranslateService, public platform: Platform) {
    this.dir = platform.dir();
    translate.get(["TUTORIAL_SLIDE1_TITLE",
      "TUTORIAL_SLIDE1_DESCRIPTION",
      "TUTORIAL_SLIDE2_TITLE",
      "TUTORIAL_SLIDE2_DESCRIPTION",
      "TUTORIAL_SLIDE3_TITLE",
      "TUTORIAL_SLIDE3_DESCRIPTION",
    ]).subscribe(
      (values) => {
        console.log('Loaded values', values);
        this.s = [
          {
            title: "Welcome to the Ionic Super ",
            description:"The <b>Ionic Super Starter</b> is a fully-featured Ionic starter with many pre-built pages and best practices.",
            image: 'https://cdn.dribbble.com/users/1206528/screenshots/4792084/water_first-scene.gif',
          },
          {
            title: "Welcome to the Ionic Super Starter",
            description:"The <b>Ionic Super Starter</b> is a fully-featured Ionic starter with many pre-built pages and best practices.",
            image: 'https://cdn.dribbble.com/users/1206528/screenshots/4270242/01.gif',
          },
          {
            title: "Welcome to the Ionic Super Starter",
            description:"The <b>Ionic Super Starter</b> is a fully-featured Ionic starter with many pre-built pages and best practices.",
            image: 'https://cdn.dribbble.com/users/1206528/screenshots/4275689/02.gif',
          }
          
        
        ];
      });
  }

  startApp() {
    this.storage.set('introskipped', 'yes');
    this.navCtrl.setRoot(MobileregPage, {}, {
      animate: true,
      direction: 'forward'
    });
  }
next(){
  this.slides.slideNext();

}

  onSlideChangeStart(slider) {
    this.showSkip = !slider.isEnd();
  }

  ionViewDidEnter() {
    // the root left menu should be disabled on the tutorial page
    this.menu.enable(false);
  }

  ionViewWillLeave() {
    // enable the root left menu when leaving the tutorial page
    this.menu.enable(true);
  }

}
