import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ForstoresubscribtionlistbydayPage } from '../../pages/forstoresubscribtionlistbyday/forstoresubscribtionlistbyday';

/**
 * Generated class for the StoresubscribtionlistbydayPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-storesubscribtionlistbyday',
  templateUrl: 'storesubscribtionlistbyday.html',
})
export class StoresubscribtionlistbydayPage {
  forbyday:any=ForstoresubscribtionlistbydayPage;
  selected:any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.selected=this.navParams.get('day');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad StoresubscribtionlistbydayPage');
  }

}
