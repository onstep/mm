import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SharedProvider } from '../../providers/shared/shared';
import { Http } from '@angular/http';
import {  ViewChild } from '@angular/core';
import { StorecheckoutPage } from '../../pages/storecheckout/storecheckout';
import { MobileregPage } from '../../pages/mobilereg/mobilereg';
import { Storage } from '@ionic/storage';
import { FoodProvider } from '../../providers/food/food';

/**
 * Generated class for the FoodcartPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-foodcart',
  templateUrl: 'foodcart.html',
})
export class FoodcartPage {
  data: any;
  go: any;
  loaded: any="0";
  userid:any;

  products: any=[];
  cartfill: any;
  cartids:any=[];
  storecheckout=StorecheckoutPage;
  mobilepage:any=MobileregPage;
  constructor(public food: FoodProvider,public shared: SharedProvider,public navCtrl: NavController, public navParams: NavParams,public http: Http,private storage: Storage) {
  }

  ionViewDidEnter() {
    console.log('ionViewDidLoad FoodcartPage');
    this.getcartdetails();
  }
  getcartdetails() {
   
    console.log(this.food.foodcartdetails.all);
  var cartarray=this.food.foodcartdetails.all;
  for(var i=0;i<cartarray.length; i++){
   
    this.cartids.push( cartarray[i].productid );
  
  
     }
     if(this.cartids.length>0)
     {
       this.cartfill='yes';
      var link = 'http://18.220.1.217/food/addcart.php'; 
     console.log(this.cartids);
       var myData = JSON.stringify({productid:  this.cartids});
       
      
        
       
       
      this.http.post(link, myData)
      //this.http.get(link)
      
       .subscribe(data => {
  
       this.data = data["_body"];
    console.log(this.data); 
  
      this.products = JSON.parse(this.data);
      //console.log(this.products.records);
  
      
      for(var i=0;i<this.products.records.length; i++){
  // console.log(this.products.records[i].price);
      this.food.updatecart(this.products.records[i].id,this.products.records[i].price);
      
      
         }
   
      this.loaded="1";
  
       }, error => {
      // alert(id);
       });
       }
       else{
        this.cartfill='no';
  
       }
  
      }
}
