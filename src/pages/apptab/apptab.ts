import { IonicPage, NavController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ResidencelistPage } from '../residencelist/residencelist';
import { Component, ViewChild, OnInit, Renderer, Input } from '@angular/core';
import {Tabs} from 'ionic-angular';

import { SharedProvider } from '../../providers/shared/shared';

/**
 * Generated class for the ApptabPage tabs.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-apptab',
  templateUrl: 'apptab.html'
})
export class ApptabPage {
  @ViewChild("tabs") tabs: Tabs;

selected:any;
  mainRoot = 'MainPage'
  shopRoot = 'ShopPage'
  foodRoot = 'FoodPage'
  servicesRoot = 'ServicesPage'
  cartRoot = 'CartPage'
    seeTabs: boolean = true;

  Residencetype:any;
  loaded:any='0';

  constructor(public shared: SharedProvider,public navCtrl: NavController,private storage: Storage ) {

  
  }
  change(){
    this.selected=this.tabs.getSelected();
  this.shared.in=this.selected.root ;
  }
}