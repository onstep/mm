import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
// import htmlToImage from 'html-to-image';
import { SocialSharing } from '@ionic-native/social-sharing';

/**
 * Generated class for the MembersuccessPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-membersuccess',
  templateUrl: 'membersuccess.html',
})
export class MembersuccessPage {
loaded:any='1';
class:any='ribbon';
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    setTimeout(() => 
    {
      this.class='ribbon active';
    },
    1000);

  }
   changeclass(){
    this.class='ribbon active';
  }
  // click(){
  //   var node = document.getElementById('my-node');
  //   htmlToImage.toPng(node)
  //   .then( (dataUrl) =>  {
  //     var img = new Image();
  //     img.src = dataUrl;
  //     document.body.appendChild(img);
  //     console.log(img.src);
  //    // window.open(img.src);
  //    this.socialSharing.share('Here is my contribution to farmers through FARMLife app', 'FARMLIFE', img.src, 'https://farmliferetail.com')

  //   //  window.open(img.src,'_blank');
  
  //   })
  //   .catch(function (error) {
  //     console.error('oops, something went wrong!', error);
  //   });
  //     }
  ionViewDidLoad() {
    console.log('ionViewDidLoad MembersuccessPage');
  }
  gohome(){
    this.navCtrl.popToRoot() ;
}


}
