import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController,Events } from 'ionic-angular';
import {Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
//import { OTPAutoVerification } from 'cordova-plugin-otp-auto-verification';
import { Storage } from '@ionic/storage';
declare var OTPAutoVerification: any;
//import { AndroidPermissions } from '@ionic-native/android-permissions';
import { Platform } from 'ionic-angular';
import { OneSignal } from '@ionic-native/onesignal';
import { ToastController } from 'ionic-angular';
import { FirstorderPage} from '../../pages/firstorder/firstorder';
import { AlertController } from 'ionic-angular';
import { SharedProvider } from '../../providers/shared/shared';

/**
 * Generated class for the UpdateaddressPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-updatebank',
  templateUrl: 'updatebank.html',
})
export class UpdatebankPage {
  new:any={};
  error:any;
  error2:any;
  panerror:any;
  aadharerror:any;
  mobileerror:any;
  formdata:any={};
  userid:any;
  disablebutton:any;
  loaded:any;
  data:any;
  address:any;
  sponserdata:any;
  ifscdata:any;
  orderpage:any=FirstorderPage;
  constructor(public shared: SharedProvider,private alertCtrl: AlertController,public toastCtrl: ToastController,public navCtrl: NavController, public navParams: NavParams,public http: Http,public loadingCtrl: LoadingController,private storage: Storage, public events: Events,public plt: Platform,public oneSignal: OneSignal) {
   this.address='no';
   this.storage.get('mobile').then((val) => {
    if(val){
      this.formdata.mobile=val;
      this.storage.get('userdata').then((val2) => {
        if(val2){
        console.log(val2);
          this.formdata.userdata=val2;
          console.log('next');
          console.log(this.formdata);

                this.checksponser();


        }
    
         });
    }

     });
    
   
  }
  
    ionViewDidLoad() {
      console.log('ionViewDidLoad RegistrationPage');
  
  var today = new Date();
  today = new Date('1 Jan 1995');
  this.formdata.dob = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    }
  
  
    toast(text) {
      const toast = this.toastCtrl.create({
        message: text,
        duration: 3000,
        position:'bottom'
      });
      toast.present();
    }
  
    update() {
      console.log('clicked');
  
      //return;
     
   
      if(!this.formdata.pan || this.formdata.pan == ' ' || this.formdata.pan.toString().length!=10){
        // this.error='Invalid PAN Number !';
         //this.disablebutton=false;
    
        // return;
    
        }
        if(!this.formdata.aadhar || this.formdata.aadhar == ' '  || this.formdata.aadhar.toString().length!=12){
         this.error='Invalid Aadhar Number!';
         this.disablebutton=false;
         this.toast(this.error);
    
         return;
    
        }

  
        if(!this.formdata.nameinbank || this.formdata.namein == ' '){
          this.error='Invalid  Name!';
          this.disablebutton=false;
          this.toast(this.error);
     
          return;
     
         }
      
       
         if(!this.formdata.accountnumber || this.formdata.accountnumber == ' '){
          this.error='Invalid Account Number!';
          this.disablebutton=false;
          this.toast(this.error);
     
          return;
     
         }
         if(!this.formdata.ifsc || this.formdata.ifsc == ' '){
          this.error='Invalid IFSC !';
          this.disablebutton=false;
          this.toast(this.error);
     
          return;
     
         }
         if(!this.formdata.branch || this.formdata.branch == ' '){
          this.error='Invalid IFSC !';
          this.disablebutton=false;
          this.toast(this.error);
     
          return;
     
         }
        
        
         if(this.formdata.verifyaccountnumber!=this.formdata.accountnumber){
          this.error="Account number mismatch !";
          this.disablebutton=false;
          this.toast(this.error);
     
          return;
     
         }

       //  return;
  this.error=null;
  
  let loading = this.loadingCtrl.create({
    content: ' Please wait...'
  });
  
  loading.present();
  
         // this.router.navigate(['/workers/addworker/'+this.engineerid]);
  
         this.loaded='1';
         this.disablebutton=true;
      this.formdata.companyid=this.userid;
    
      var link = 'https://api.mmsilks.onstep.in/farmlifeappbackend/user/updatekyc.php';
      //  var link = 'https://api.mmsilks.onstep.in/farmlifeappbackend/user/checksponsor.php';

    
         var myData = JSON.stringify(
          {
            id: this.formdata.userdata.id,
            first_name: this.formdata.name,
            last_name: this.formdata.surname,
            customer_phone: this.formdata.mobile,
            address: this.formdata.line1,
            address2: this.formdata.line2,
            address3: this.formdata.line3,
            city: this.formdata.city ,
            state: this.formdata.state,
            country: 'india',
            pincode: this.formdata.pincode,
            longitude: '-',
            latitude: '-',
            sponsor_name:  this.formdata.refname,
            sponsor_id: this.formdata.refid,
            sponsor_mobile : this.formdata.refmobile,
            aadhar_no: this.formdata.aadhar,
            nameinbank: this.formdata.nameinbank,
            bank_name: this.formdata.bank,
            account_no: this.formdata.accountnumber,
            branch: this.formdata.branch,
            ifsc_code: this.formdata.ifsc,
            unit_key: 11204,
            rice_option: 'ravi',
            customer_type: 'customer',
            country_code: 91,
            customer_active_status: '1',
            combo_active_status: '1'
          }
         );
         console.log(myData);
    
    
    
  
    
        this.http.post(link,myData)
        //this.http.get(link)
    
         .subscribe(data => {
          console.log(data);
    
         this.data = data["_body"];
      //alert(this.data);
      console.log(this.data);
    
         this.data = JSON.parse(this.data);
      console.log(this.data);
      if(this.data.status=='success'){
        this.storage.set('userdata', this.data.userdata[0]);
        this.events.publish('app:loaduserdata');
      
        this.shared.getstatus();
        //this.toast('sucess');
       // this.data = JSON.parse(this.data);
  
        loading.dismiss();
  
    let alertcontrol = this.alertCtrl.create({
      title: 'Updated Successfully !',
      buttons: ['Okay']
    });
    alertcontrol.present();
    this.navCtrl.pop();
  
          // this.navCtrl.push(this.orderpage);
      //  this.formdata={};
       // this.engineerid=this.data.id;
        this.loaded='0';
  
        //this.ngOnInit();
        //this.openSuccessCancelSwal();
    
      }
      if(this.data.status=='Failure'){
        loading.dismiss();
  
        this.toast(this.data.statusmessage);
      }
    
         }, error => {
          loading.dismiss();
          this.toast('Check your internet connection');
  
      this.data.status='failed';
         this.loaded='0';
    
         });
  
  
        
     
  
  
    
         } 
  
      
  
  
         checksponser(){
       
          var link = 'https://api.mmsilks.onstep.in/farmlifeappbackend/user/getuserdata.php';
       
          var myData = JSON.stringify({id:  this.formdata.userdata.id});
  
             console.log(myData);
        
        
        
        
        
            this.http.post(link,myData)
            //this.http.get(link)
        
             .subscribe(data => {
              console.log(data);
        
             this.sponserdata = data["_body"];
          //alert(this.data);
          console.log(this.sponserdata);
        
             this.sponserdata = JSON.parse(this.sponserdata);
          console.log(this.sponserdata);
          if(this.sponserdata.status=='success'){
           // alert("sucecss");
         
           this.formdata.pan=this.sponserdata.records.pan;
           this.formdata.aadhar=this.sponserdata.records.aadar;
           this.formdata.nameinbank=this.sponserdata.records.nameinbank;
           this.formdata.ifsc=this.sponserdata.records.ifsc;
           this.formdata.accountnumber =this.sponserdata.records.accountnumber;
           this.formdata.verifyaccountnumber =this.sponserdata.records.accountnumber;

           this.checkifsc();
     
     
          
           // this.engineerid=this.data.id;
            this.loaded='1';
            //this.ngOnInit();
            //this.openSuccessCancelSwal();
        
          }
        
             }, error => {
          this.data.status='failed';
             this.loaded='0';
        
             });
             
            }
  
         checkifsc(){
          if(this.formdata.ifsc.toString().length!=11){
          return;
          }
       
          var link = 'https://ifsc.razorpay.com/'+this.formdata.ifsc;
       
          var myData = JSON.stringify({id:  this.formdata.userdata.id});
  
             console.log(myData);
        
        
        
        
        
            this.http.get(link,myData)
            //this.http.get(link)
        
             .subscribe(data => {
              console.log(data);
                 console.log(data);

           this.ifscdata = data["_body"];
         

              this.ifscdata = JSON.parse(this.ifscdata);

           this.formdata.branch=this.ifscdata.BRANCH;
           this.formdata.bankname=this.ifscdata.BANK;

          
           // alert("sucecss");
                  
  
     
     
          
           // this.engineerid=this.data.id;
            this.loaded='1';
            //this.ngOnInit();
            //this.openSuccessCancelSwal();
        
          
        
             }, error => {
                       this.toast('Invalid IFSC !');  
              this.loaded='0';
              this.formdata.branch='';
           this.formdata.bankname='';
        
             });
             
            }
  
  
  }
  
