import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController,Events } from 'ionic-angular';
import {Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
//import { OTPAutoVerification } from 'cordova-plugin-otp-auto-verification';
import { Storage } from '@ionic/storage';
import { StorecheckoutPage } from '../../pages/storecheckout/storecheckout';
declare var OTPAutoVerification: any;
//import { AndroidPermissions } from '@ionic-native/android-permissions';
import { Platform } from 'ionic-angular';
import { SharedProvider } from '../../providers/shared/shared';
import { OneSignal } from '@ionic-native/onesignal';
import { ToastController } from 'ionic-angular';
import { RegistrationPage } from '../../pages/registration/registration';
import { ShopPage } from '../../pages/shop/shop';
import { Contacts, Contact, ContactField, ContactName } from '@ionic-native/contacts';
import { AlertController } from 'ionic-angular';


declare var SMS:any;
declare var document:any;
declare var window: any;
@Component({
  selector: 'page-referafriend',
  templateUrl: 'referafriend.html',
})
export class ReferafriendPage {
  mobile:any="";
  otp:any="";
  data:any;
  disablebutton:any;
  error:any;
  formdata:any={};
  userdata:any={};
  storecheckoutpage:any=StorecheckoutPage;
  loaded:any;
  step:any='login';
  otpp:any='';
  mobilee:any='';
  smsArived:any;
  onesignalid:any;
  userid:any;
  password:any;
  friendmobile:any;
  friendname:any;
  name:any;

  public backgroundImage = 'https://api.mmsilks.onstep.in/farmlife/auth/download3.png';
  constructor(private alertCtrl: AlertController,public contacts: Contacts,public toastCtrl: ToastController,public shared: SharedProvider,public navCtrl: NavController, public navParams: NavParams,public http: Http,public loadingCtrl: LoadingController,private storage: Storage, public events: Events,public plt: Platform,public oneSignal: OneSignal) {
    
    this.storage.get('userid').then((userid) => {
      //  alert(val);
        this.userid=userid;
      this.storage.get('name').then((name) => {

        this.name=name;

      });
      this.storage.get('userdata').then((userdata) => {

        this.userdata=userdata;

      });
  
    
      });

    /*
    if (this.plt.is('android')) {
        this.checkPermission();
  
        // This will only print when on iOS
        console.log('I am an android device!');
      
  
      //this.autoverify();
      document.addEventListener('onSMSArrive', (e) =>  {
        var sms = e.data;
     // alert(sms.body);
       console.log("received sms "+JSON.stringify( sms ) );
       
       var res = sms.body.split(" ");
       //alert(res[2]);
     //  alert(res[4]);
  
       if(res[2]=='Onstep'){
  this.otp=res[5];
  //alert(this.otp);
  //this.goback();
        this.verifyotp();
       // alert('ddeded');
         this.stopSMS();
  
       }
       
       if(sms.address=='HP-611773') //look for your message address
       {
         this.otp=sms.body.substr(0,4);
              
   this.stopSMS();
        this.verifyotp();
       }
        
        
        
      });
  
  //this.checkPermission();
    }
    */
    }
    toast(text) {
      const toast = this.toastCtrl.create({
        message: text,
        duration: 3000,
        position:'bottom'
      });
      toast.present();
    }
    ionViewDidLoad() {
      console.log('ionViewDidLoad MobileregPage');
    }
    ionViewWillEnter() {
      //document.addEventListener('onSMSArrive', this.smsArived);
    }
    goback(){
      this.step='1';
    }
    register(){
      this.navCtrl.push(RegistrationPage);
    }
    pick(){
     
    this.contacts.pickContact().then((contact)=>{

    //alert("contacts:-->"+ JSON.stringify(contact));
    console.log(contact);
    console.log('1');
   console.log(contact['_objectInstance'].id);
   console.log(contact['_objectInstance']);
    //alert("contacts:-->"+ JSON.stringify(contact._objectInstance));
     this.name=contact['_objectInstance'].name.givenName;
     var fullnumber=contact['_objectInstance'].phoneNumbers[0].value;
     fullnumber = fullnumber.replace(/ +/g, "");
     this.mobile=fullnumber.substr(fullnumber.length - 10);
     
   //  this.formdata.mobile=
  });
  }
    referfriend() {


      if(!this.name || this.name == ' '){
        // PNotify.notice({ text: 'Notice 1.', stack: {'dir1':'down', 'firstpos1':25}   });
        this.error='Invalid name !';
        this.disablebutton=false;
        this.toast(this.error);
   
        return;
   
       }
  
       if(!this.mobile || this.mobile == ' ' || this.mobile.toString().length!=10 ){
        // PNotify.notice({ text: 'Notice 1.', stack: {'dir1':'down', 'firstpos1':25}   });
        this.error='Invalid Mobile Number !';
        this.disablebutton=false;
        this.toast(this.error);
   
        return;
   
       }
    
       
      
        let loading = this.loadingCtrl.create({
          content: 'Please wait...'
        });
      
        loading.present();
      
       
     
      
      //alert(this.mobile);
        var link = 'https://api.mmsilks.onstep.in/farmlifeappbackend/user/referafriend.php'; 
      
       var myData = JSON.stringify({userid:  this.userdata.id,mobile:this.mobile,name:this.name});
       
      
        
       
       
      this.http.post(link, myData)
      //this.http.get(link)
      
       .subscribe(data => {
  
       this.data = data["_body"];
   console.log(this.data);
  
       this.data = JSON.parse(this.data);
  if(this.data.status=='success'){
  
          loading.dismiss();
          let alertcontrol = this.alertCtrl.create({
            title: this.data.message,
            buttons: ['Okay']
          });
          alertcontrol.present();

  this.friendname='';
  this.friendmobile='';

   //alert(this.slotid);
   console.log(this.data);
  }
  else{
    loading.dismiss();
    this.toast('Try agian');  

  }
   this.loaded="2";
  
       }, error => {
        loading.dismiss();
        this.toast('Oops check your internet connection');  

      // alert(id);
       });
       }
  

  login() {
    let loading = this.loadingCtrl.create({
      content: ' Verifying...'
    });
  
    loading.present();
    //alert(this.otp);
  
  
    console.log(66);

  

     
    
  console.log(77);
        var link = 'https://api.mmsilks.onstep.in/farmlife/login/applogin.php'; 
        
         var myData = JSON.stringify({userid:  this.userid,password:this.password});
        
          
         
         
        this.http.post(link, myData)
        //this.http.get(link)
        
         .subscribe(data => {
    
         this.data = data["_body"];
    
         this.data = JSON.parse(this.data);
    if(this.data.status=='success'){
if(this.data.records!='wrong'){


      this.storage.set('userid',this.userid );
      this.storage.set('mobile', this.data.records[0].mobile);
      this.storage.set('name', this.data.records[0].name);
   this.shared.userid=this.userid;
   this.shared.mobile=this.data.records[0].mobile;
      loading.dismiss();
  
      /*
      if (this.plt.is('android')) {
        this.stopSMS();
      }
      */
      this.toast('login successfull');
      this.navCtrl.setRoot(ShopPage);
      this.navCtrl.popToRoot() 
  
    //this.navCtrl.push(StorecheckoutPage);
  
    // this.step='2';
     
     //alert(this.slotid);
     console.log(this.data);
    }else{
      loading.dismiss();

      this.toast('Incorret userid or password!');

    }
    }
    else{     
         loading.dismiss();

       this.toast('Incorret userid or password!');

      
    }
     this.loaded="2";
    
         }, error => {
        // alert(id);
        loading.dismiss();

        this.toast('Oops check your internet connection !');

         }); 
        
         }

         sendpassword() {
          let loading = this.loadingCtrl.create({
            content: ' Please wait...'
          });
        
          loading.present();
          //alert(this.otp);
        
  
         
       
        
        
              var link = 'https://api.mmsilks.onstep.in/farmlife/user/forgotpassword.php'; 
              
               var myData = JSON.stringify({mobile:  this.mobile});
              
                
               
               
              this.http.post(link, myData)
              //this.http.get(link)
              
               .subscribe(data => {
          
               this.data = data["_body"];
          
               this.data = JSON.parse(this.data);
          if(this.data.status=='success'){
      
      
     
            loading.dismiss();
            this.toast('Password sent  to your mobile !');
            this.mobile='';

          }
          else{     
        
             loading.dismiss();
             this.toast('Mobile number not registered !');
      
            
          }
           this.loaded="2";
          
               }, error => {
              // alert(id);
              loading.dismiss();

              this.toast('Oops check your internet connection !');
      
               }); 
               }


         checkPermission()
         {
           /*
           this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_SMS).then(
             success => {
               
               //if permission granted
               this.receiveSMS();
             },
           err =>{
             
             this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_SMS).
             then(success=>{
               this.receiveSMS();
             },
           err=>{
             console.log("cancelled")
           });
           });
           
           this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_SMS]);
           
          */}
         receiveSMS()
         {
           /*
           if(SMS) SMS.startWatch(function(){
             console.log('watching started');
           }, function(){
             console.log('failed to start watching');
           });
          */}
         stopSMS()
         {
           /*
          if (this.plt.is('android')) {
            
          
           if(SMS) SMS.stopWatch(function(){
             console.log('watching stopped');
           }, function(){
             console.log('failed to stop watching');
           });
          }
          */
         }
     
        
    
     
     }