import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import { ServicedetailsPage } from '../../pages/servicedetails/servicedetails';
import { App, ViewController } from 'ionic-angular';

/**
 * Generated class for the ServicesubcategoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-servicesubcategory',
  templateUrl: 'servicesubcategory.html',
})
export class ServicesubcategoryPage {
  data:any;
  loaded:any="0";
  categoryid:any;
  servicedetails:any=ServicedetailsPage;
    constructor(public navCtrl: NavController, public navParams: NavParams,public http: Http,public app :App) {
      this.categoryid=this.navParams.get('subcategoryid');
    this.getservices(this.categoryid);
    }
  
    ionViewDidLoad() {
     // console.log('ionViewDidLoad ServicecategoryPage');
    }
  goservice(id,name){

    this.app.getRootNav().push(this.servicedetails, {serviceid: id, servicename: name});


  }
   
    getservices(id) {
      var link = 'http://18.220.1.217/services/servicelist.php'; 
      
       var myData = JSON.stringify({category:  id});
       
      
        
       
       
      this.http.post(link, myData)
      //this.http.get(link)
      
       .subscribe(data => {
  
       this.data = data["_body"];
            //console.log(this.data);

       this.data = JSON.parse(this.data);

  
  this.loaded="1";
       }, error => {
      // alert(id);
       });
       }
  }
  