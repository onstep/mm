import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import { StoresearchPage } from '../../pages/storesearch/storesearch';

/**
 * Generated class for the StorecategoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-storecategory',
  templateUrl: 'storecategory.html',
})
export class StorecategoryPage {
  catg: any;
  go: any=0;
  loaded:any="0";
  data: any;
  storesearchpage=StoresearchPage;

  constructor(public navCtrl: NavController, public navParams: NavParams,public http: Http) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad StorecategoryPage');
    this.getposts();
  }

  gotosearch(){

    this.navCtrl.push(this.storesearchpage, { });

  }
  getposts() {
    var link = 'https://api.mmsilks.onstep.in/farmlifeappbackend/store.php'; 
    
     var myData = JSON.stringify({location:  'all'});
     
    
      
     
     
    this.http.post(link, myData)
    //this.http.get(link)
    
     .subscribe(data => {

     this.data = data["_body"];

     this.catg = JSON.parse(this.data);
            console.log(this.catg);

  //  console.log(this.catg.subcategory);


this.loaded="1"
     }, error => {
     
     });
     }

}
