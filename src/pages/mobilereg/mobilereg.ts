import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController,Events,MenuController } from 'ionic-angular';
import {Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
//import { OTPAutoVerification } from 'cordova-plugin-otp-auto-verification';
import { Storage } from '@ionic/storage';
import { StorecheckoutPage } from '../../pages/storecheckout/storecheckout';
declare var OTPAutoVerification: any;
//import { AndroidPermissions } from '@ionic-native/android-permissions';
import { Platform } from 'ionic-angular';
import { SharedProvider } from '../../providers/shared/shared';
import { OneSignal } from '@ionic-native/onesignal';
import { ShopPage } from '../shop/shop';
import { StatusBar } from '@ionic-native/status-bar';
import { AlertController } from 'ionic-angular';

declare var SMS:any;
declare var document:any;
declare var window: any;

//ionic cordova run android &&  rm platforms/android/settings.gradle && cp -i settings.gradle platforms/android/
/**
 * Generated class for the MobileregPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-mobilereg',
  templateUrl: 'mobilereg.html',
})
export class MobileregPage {
  
mobile:any="";
otp:any="";
data:any;
storecheckoutpage:any=StorecheckoutPage;
loaded:any;
step:any='1';
otpp:any='';
mobilee:any='';
smsArived:any;
onesignalid:any;
constructor(private alertCtrl: AlertController,public menu: MenuController,public statusBar: StatusBar,public shared: SharedProvider,public navCtrl: NavController, public navParams: NavParams,public http: Http,public loadingCtrl: LoadingController,private storage: Storage, public events: Events,public plt: Platform,public oneSignal: OneSignal) {
  

  /*
  if (this.plt.is('android')) {
      this.checkPermission();

      // This will only print when on iOS
      console.log('I am an android device!');
    

    //this.autoverify();
    document.addEventListener('onSMSArrive', (e) =>  {
      var sms = e.data;
   // alert(sms.body);
     console.log("received sms "+JSON.stringify( sms ) );
     
     var res = sms.body.split(" ");
     //alert(res[2]);
   //  alert(res[4]);

     if(res[2]=='Onstep'){
this.otp=res[5];
//alert(this.otp);
//this.goback();
      this.verifyotp();
     // alert('ddeded');
       this.stopSMS();

     }
     
     if(sms.address=='HP-611773') //look for your message address
     {
       this.otp=sms.body.substr(0,4);
            
 this.stopSMS();
      this.verifyotp();
     }
      
      
      
    });

//this.checkPermission();
  }
  */
  }
  ionViewDidEnter() {
    // the root left menu should be disabled on the tutorial page
    this.statusBar.backgroundColorByHexString('#dce35b');
    this.menu.enable(false);

  }

  ionViewWillLeave() {
    // enable the root left menu when leaving the tutorial page
    this.menu.enable(true);
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad MobileregPage');
  }
 

  goback(){
    this.step='1';
  }
  reqotp() {
    
      let loading = this.loadingCtrl.create({
        content: 'Please wait...'
      });
    
      loading.present();
    
     
   
    
    
    var link = 'https://api.mmsilks.onstep.in/farmlifeappbackend/otp.php'; 
    
     var myData = JSON.stringify({userid:  this.mobile});
     
    
      
     
     
    this.http.post(link, myData)
    //this.http.get(link)
    
     .subscribe(data => {

     this.data = data["_body"];
 console.log(this.data);

     this.data = JSON.parse(this.data);
if(this.data.status=='sucess'){

        loading.dismiss();

 this.step='2';

 //alert(this.slotid);
 console.log(this.data);
}
else{
  loading.dismiss();

}
 this.loaded="2";

     }, error => {
      loading.dismiss();

    // alert(id);
     });
     }

verifyotp() {
  let loading = this.loadingCtrl.create({
    content: ' Verifying...'
  });

  loading.present();
  //alert(this.otp);
/*
  this.oneSignal.startInit('f17a3322-543e-40ba-b58a-b4f4484cde1b', '520179124830');

  this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);
  
  this.oneSignal.handleNotificationReceived().subscribe((data) => {
   // do something when notification is received
  
  });
  
  this.oneSignal.handleNotificationOpened().subscribe((data) => {
    // do something when a notification is opened
   //alert(JSON.stringify(data));
  
  
  });
  var ids=this.oneSignal.getIds().then((data) => {
this.onesignalid=data.userId;

  
  this.oneSignal.endInit();
   /////not required for mobile
  });
  ////////
*/
  this.onesignalid="nodata";
      var link = 'https://api.mmsilks.onstep.in/farmlifeappbackend/otpverify.php'; 
      
       var myData = JSON.stringify({userid:  this.mobile,otp:this.otp,secretid: this.onesignalid});
      
        
       
       
      this.http.post(link, myData)
      //this.http.get(link)
      
       .subscribe(data => {
  
       this.data = data["_body"];
  
       this.data = JSON.parse(this.data);
  if(this.data.status=='sucess'){
 this.storage.set('userid', this.data.userid);
    this.storage.set('mobile', this.mobile);
    this.storage.set('userdata', this.data.data[0]);
 //this.shared.userid=this.data.userid;
 this.events.publish('app:loaduserdata');
 this.shared.mobile=this.mobile;
 this.shared.getstatus();
    loading.dismiss();

    /*
    if (this.plt.is('android')) {
      this.stopSMS();
    }
    */
    this.statusBar.backgroundColorByHexString('#ffffff');
    this.statusBar.backgroundColorByHexString('#f4f2e5');

    this.navCtrl.setRoot(ShopPage);
    this.navCtrl.popToRoot();

  //this.navCtrl.pop();

  //this.navCtrl.push(StorecheckoutPage);

  // this.step='2';
   
   //alert(this.slotid);
   console.log(this.data);
  }
  if(this.data.status=='mismatch'){
    let alertcontrol = this.alertCtrl.create({
      title: 'Invalid OTP, Try agian',
      buttons: ['Okay']
    });
    loading.dismiss();
    alertcontrol.present();


  }
  else{     

     loading.dismiss();

    
  }
   this.loaded="2";
  
       }, error => {
      // alert(id);

       }); 

       ////important for mobile
     //  });
       /////
       }
       checkPermission()
       {
         /*
         this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_SMS).then(
           success => {
             
             //if permission granted
             this.receiveSMS();
           },
         err =>{
           
           this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_SMS).
           then(success=>{
             this.receiveSMS();
           },
         err=>{
           console.log("cancelled")
         });
         });
         
         this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_SMS]);
         
        */}
       receiveSMS()
       {
         /*
         if(SMS) SMS.startWatch(function(){
           console.log('watching started');
         }, function(){
           console.log('failed to start watching');
         });
        */}
       stopSMS()
       {
         /*
        if (this.plt.is('android')) {
          
        
         if(SMS) SMS.stopWatch(function(){
           console.log('watching stopped');
         }, function(){
           console.log('failed to stop watching');
         });
        }
        */
       }
   
      
   
   
   }