import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http'; //https://stackoverflow.com/questions/43609853/angular-4-and-ionic-3-no-provider-for-http
import { SharedProvider } from '../../providers/shared/shared';
import { ImageLoader } from 'ionic-image-loader';
import { Component ,ViewChild} from '@angular/core';
import { Content } from 'ionic-angular';
import { StorecartPage } from '../../pages/storecart/storecart';
import { StorecategoryPage } from '../../pages/storecategory/storecategory';
  import { App, ViewController } from 'ionic-angular';
  import { HoteldetailsPage } from '../../pages/hoteldetails/hoteldetails';
  import { FoodProvider } from '../../providers/food/food';
  import { FoodcartPage } from '../../pages/foodcart/foodcart';

/**
 * Generated class for the ForfoodlistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-forfoodlist',
  templateUrl: 'forfoodlist.html',
})
export class ForfoodlistPage {

  @ViewChild(Content) content: Content;
  title :any="ravu";
  categoryname :any="ravu";
  data: any;
  catg: any;
  go: any=0;
  loaded:any="0";
  forfoodlist:any=ForfoodlistPage;
menuid:any;
type:any;
foodcart:any=FoodcartPage;

    constructor(public navCtrl: NavController , public navParams: NavParams,public http: Http,public shared: SharedProvider,public food: FoodProvider, public imageLoader: ImageLoader,public app :App) {
     // this.shared.store();
  //this.shared.addtostorecart('1','90','100');
  //this.shared.reduceinstorecart('10');
    }
  
   
  
    ionViewDidLoad() {
     // console.log('ionViewDidLoad MainPage');'
    // this.menuname=this.navParams.get('menuname');
     this.menuid=this.navParams.get('menuid');
     this.type=this.navParams.get('type');
     async function delay(ms: number) {
      await new Promise(resolve => setTimeout(()=>resolve(), 1000)).then(()=>console.log("fired"));
  }

  delay(500).then(any=>{
//alert('hh');
  var id=this.navParams.get('subcategoryid');
 this.getposts(this.menuid,this.type);

  });
  
     
      console.log(this.food.foodcartdetails.price);
    }
    

    gofoodcart(){
      this.app.getRootNav().push(this.foodcart);
    }
    getposts(id,type) {
      var link = 'http://18.220.1.217/food/food.php'; 
      
       var myData = JSON.stringify({menu:  id,type: type});
       
      
        
       
       
      this.http.post(link, myData)
      //this.http.get(link)
      
       .subscribe(data => {
  
       this.data = data["_body"];
      // console.log(this.data);

     this.data = JSON.parse(this.data);
       //       console.log(this.catg);

             

    //  console.log(this.catg.subcategory);
  
  
  this.loaded="1";
       }, error => {
       
       });
       }
  
  }
  