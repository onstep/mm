import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import { AddressPage } from '../../pages/address/address';
import { AddaddressPage } from '../../pages/addaddress/addaddress';
import { StorepaymentPage } from '../../pages/storepayment/storepayment';
import { SharedProvider } from '../../providers/shared/shared';
import { Storage } from '@ionic/storage';
import { AlertController } from 'ionic-angular';
declare var RazorpayCheckout: any;
import { ToastController } from 'ionic-angular';

/**
 * Generated class for the NewcheckoutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-newcheckout',
  templateUrl: 'newcheckout.html',
})
export class NewcheckoutPage {
  data:any;
  ydata:any;
  formdata:any={};
  loaded:any="0";
  loaded2:any="0";
  address:any;
  slott:any;
  slotid:any=0;
  userid:any;
  deliverytype:any="typeslot";
  addaddresspage:any=AddaddressPage;
  addresspage:any=AddressPage;
  paymentpage:any=StorepaymentPage;
  dates:any;
  expressdeliverycharge;
  expressdeliverychargediscount;
  finalexpressdeliverycharge;
  myColor: string = 'slotcolor';
  razorpaysecure:any;
  onlinepay:any;
  wallet:any;
  mobile:any;
  count:any;
  products: any=[];
  cartfill: any;
  cartids:any=[];
  
  constructor(public toastCtrl: ToastController,public shared: SharedProvider,public navCtrl: NavController, public navParams: NavParams,public http: Http,private storage: Storage,private alertCtrl: AlertController) {
    this.day('1');
      }
    
  
    ionViewDidLoad() {
      
  
  
    }
    toggleColor() {
      this.myColor = 'slotcolor';
  }
    ionViewDidEnter(){
     // this.ionViewDidLoad(); 
     this.day('1');
  
     this.storage.get('mobile').then((val) => {
    this.mobile=val;
     
          });
  
  this.storage.get('userid').then((val) => {
        this.userid= val;
        if(this.userid=='nouserid'){
          
        }else{
          
              this.getcartdetails();

              this.slot();
        
        }
            });
             console.log('ionViewDidLoad StorecheckoutPage');
    
  
    }
    gotoaddresspage(){
      this.navCtrl.push(this.addaddresspage,{pop :1});
    }
    deliverytypeselect(type){
      this.deliverytype=type;
    }
  
     day(date) {
      var d = new Date(date);
      var weekday = new Array(7);
      weekday[0] = "Sunday";
      weekday[1] = "Monday";
      weekday[2] = "Tuesday";
      weekday[3] = "Wednesday";
      weekday[4] = "Thursday";
      weekday[5] = "Friday";
      weekday[6] = "Saturday";
  
      var n = weekday[d.getDay()];
      return n;
  }
  
   
    slot() {
      var link = 'https://api.mmsilks.onstep.in/farmlife/user/profile.php'; 
      
       var myData = JSON.stringify({userid:  this.userid});
       
     // alert(this.userid);
        
       
       
      this.http.post(link, myData)
      //this.http.get(link)
      
       .subscribe(data => {
  
       this.ydata = data["_body"];
   //alert(this.data);
   console.log(this.ydata);
  
       this.ydata = JSON.parse(this.ydata);
  if(this.ydata.status=='success'){
    this.razorpaysecure='rzp_test_enFay33iw1itRL';
   
  this.formdata.cust_name=this.ydata.user[0].name;
  this.formdata.ord_address=this.ydata.user[0].line1;
  this.formdata.ord_address2=this.ydata.user[0].line2;
  this.formdata.ord_address3=this.ydata.user[0].line3;
   this.loaded="1";

  }
  this.loaded="1";

 
  
       }, error => {
        this.loaded="1";

      // alert(id);
       });
       }
       
       getcartdetails() {
   
        console.log(this.shared.storecartdetails.all);
      var cartarray=this.shared.storecartdetails.all;
      for(var i=0;i<cartarray.length; i++){
       
        this.cartids.push( cartarray[i].productid );
      
      
         }
         if(this.cartids.length>0)
         {
           this.cartfill='yes';
          var link = 'https://api.mmsilks.onstep.in/farmlifeappbackend/addcart.php'; 
          //console.log(this.cartids);
           var myData = JSON.stringify({productid:  this.cartids});
           
          
            
           
           
          this.http.post(link, myData)
          //this.http.get(link)
          
           .subscribe(data => {
      
           this.data = data["_body"];
        console.log(this.data); 
      
          this.products = JSON.parse(this.data);
          //console.log(this.products.records);
      
          
          for(var i=0;i<this.products.records.length; i++){
      // console.log(this.products.records[i].price);
          this.shared.updatecart(this.products.records[i].pid,this.products.records[i].price,this.products.records[i].mrp);
          
          
             }
       
          this.loaded2="1";
      
           }, error => {
          // alert(id);
           });
           }
           else{
            this.cartfill='no';
      
           }
      
          }

          pay() {
           
                       console.log(this.formdata);
 
           // alert(this.langForm.value.langs);
           var options = {
             description: 'Amount payable.',
             image: 'http://18.220.1.217/images/logolow.png',
             currency: 'INR',
             key: 'rzp_test_enFay33iw1itRL',
             amount: this.shared.storecartdetails.price.toFixed(0)*100,
             name: 'FARM Life',
             prefill: {
             
            
             },
             external: {
               wallets: ['mobiwik']
             },
             theme: {
               color: '#ff7b1c'
             },
             modal: {
               ondismiss: function() {
               //  alert('dismissed')
              // this.toast('dismissed');
               }
             }
           };
       
           var successCallback = (payment_id) => {
             let vm=this;
         // alert('payment_id: ' + payment_id);
           // vm.completeorder(payment_id);
          //  alert('sucess');
         // vm.toast('sucess, payment id: '+payment_id);
          vm.toast('Order placed successfully');
          this.shared.removestorecart();
          this.navCtrl.popToRoot();
     // vm.register();
            
       
           };
       
           var cancelCallback = function(error) {
            // alert(error.description + ' (Error ' + error.code + ')');
            // alert("failed");
            let vm=this;
            vm.toast('error.description');
 
           };
       
           RazorpayCheckout.open(options, successCallback, cancelCallback);
         }



         toast(text) {
          const toast = this.toastCtrl.create({
            message: text,
            duration: 5000,
            position:'bottom'
          });
          toast.present();
        }

    gopayment(){
  
  this.navCtrl.push(this.paymentpage,{userid:this.userid,cart: this.shared.storecartdetails.all,addressid: this.address.id,deliverytype: this.deliverytype, slotid: this.slotid,amount:this.shared.storecartdetails.price.toFixed(0),totalmrp:this.shared.storecartdetails.mrp,expressdeliverycharge: this.expressdeliverycharge,expressdeliverychargediscount: this.expressdeliverychargediscount,razorpaysecure:this.razorpaysecure,onlinepay:this.onlinepay,wallet: this.wallet,mobile: this.mobile} );
  var myy = JSON.stringify({userid:this.userid,cart: this.shared.storecartdetails.all,addressid: this.address.id,deliverytype: this.deliverytype, slotid: this.slotid,amount:this.shared.storecartdetails.price,totalmrp:this.shared.storecartdetails.mrp});
  //alert(this.shared.storecartdetails.mrp);
  //console.log(myy);
    }
  
  
    getcount(productid){
      this.storage.get('storecart').then((val) => {
        //console.log(val);
        var count;
        var  index = val.findIndex((obj => obj.productid == productid));
        
         if(index == -1){
         count=0;
        
        }
        else{
       count=val[index].count;
        return count;
        }
        });
    }
  }
  
