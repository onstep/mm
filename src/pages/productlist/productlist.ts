import { IonicPage, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import { Component, ViewChild } from '@angular/core';
import {ProductdetailsPage } from '../../pages/productdetails/productdetails';

import {  NavController , Slides , Content} from 'ionic-angular';
/**
 * Generated class for the ProductlistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-productlist',
  templateUrl: 'productlist.html',
})
export class ProductlistPage {
  @ViewChild('SwipedTabsSlider') SwipedTabsSlider: Slides ;
  @ViewChild('scroll') scroll: Content;
  @ViewChild(Content) content: Content;

  SwipedTabsIndicator :any= null;
  tabElementWidth_px :any= 140;
  tabs:any=[];
  catg:any;
  data: any;
  productlisting:any;
  products: any;
  go: any=0;

  loaded:any=0;
  productdetails=ProductdetailsPage;
  public images: any;
  @ViewChild('slider') slider: Slides;
  page :any='0';
  constructor(public navCtrl: NavController, public navParams: NavParams,public http: Http) {

var id=this.navParams.get('subcategoryid');
  //  this.getproducts(id);
  }
  
  
  ionViewDidEnter(){
    async function delay(ms: number) {
      await new Promise(resolve => setTimeout(()=>resolve(), 1000)).then(()=>console.log("fired"));
  }

  delay(500).then(any=>{
//alert('hh');
  var id=this.navParams.get('data');
    this.getproducts(34);
    
});
  
  }
  product(id){
    this.navCtrl.push(this.productdetails,{data:id});
  }

  getproducts(id) {
    var link = 'http://13.232.62.63/bigcart/mmsilksbackend/home/subcatproduct.php'; 
    
     var myData = JSON.stringify({id:  id});
     
    
      
     
     
    this.http.post(link, myData)
    //this.http.get(link)
    
     .subscribe(data => {

     this.data = data["_body"];
     //alert(this.data);
    //  console.log(this.data);

     this.catg = JSON.parse(this.data);
    //  console.log(this.catg);
     this.productlisting=this.catg.data;
     console.log(this.productlisting);
     if(this.catg.status=="success"){    

      //  console.log(this.catg.subcategory);
      // this.navCtrl.push(this.Mmproduct);
      // this.navCtrl.push(this.productdetails);
  
      }
      else{
        return;
      }
      
this.loaded="1";
     }, error => {
    // alert(id);
     });
     }



     selectTab(index) {
      this.SwipedTabsIndicator.style.webkitTransform = 'translate3d('+(100*index)+'%,0,0)';
       this.scroll.scrollTo(index*this.tabElementWidth_px,0,500);
      this.SwipedTabsSlider.slideTo(index, 500);
    }
  
    updateIndicatorPosition() {
      this.scroll.scrollTo(this.SwipedTabsSlider.getActiveIndex()*this.tabElementWidth_px,0,200);
  
        // this condition is to avoid passing to incorrect index
      if( this.SwipedTabsSlider.length()> this.SwipedTabsSlider.getActiveIndex())
      {
        this.SwipedTabsIndicator.style.webkitTransform = 'translate3d('+(this.SwipedTabsSlider.getActiveIndex() * 100)+'%,0,0)';
      }
  
      }
  
  
    animateIndicator($event) {
      if(this.SwipedTabsIndicator)
           this.SwipedTabsIndicator.style.webkitTransform = 'translate3d(' + (($event.progress* (this.SwipedTabsSlider.length()-1))*100) + '%,0,0)';
  
    }
  
}
