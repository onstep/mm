import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MmproductlistPage } from './mmproductlist';

@NgModule({
  declarations: [
    MmproductlistPage,
  ],
  imports: [
    IonicPageModule.forChild(MmproductlistPage),
  ],
})
export class MmproductlistPageModule {}
