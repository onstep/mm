import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import { StoreorderdeatailPage } from '../../pages/storeorderdeatail/storeorderdeatail';
import { App, ViewController } from 'ionic-angular';
 
/**
 * Generated class for the StoreordersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-storeorders',
  templateUrl: 'storeorders.html',
})
export class StoreordersPage {
userid:any;
data:any;
loaded:any="0";
detailpage:any=StoreorderdeatailPage;
header:any=this.navParams.get('header');

constructor(public navCtrl: NavController, public navParams: NavParams,public http: Http,private storage: Storage, public app :App) {
  this.header=this.navParams.get('header');

}

  ionViewDidLoad() {
    console.log('ionViewDidLoad StoreordersPage');
  }
  ionViewDidEnter(){
    // this.ionViewDidLoad(); 
           //   this.orders();
 
 this.storage.get('userid').then((val) => {
       this.userid= val;
       if(this.userid=='nouserid'){
      //   alert(this.userid);
       }else{
      //  alert(this.userid);

        this.orders();

       }
           });
          
 
   }

   goorderdetails(id){
    this.app.getRootNav().push(this.detailpage, {orderid: id});

   }

   orders() {
    var link = 'https://api.mmsilks.onstep.in/farmlifeappbackend/myorder.php'; 
    
     var myData = JSON.stringify({userid:  this.userid});
     
    
      
     
     
    this.http.post(link, myData)
    //this.http.get(link)
    
     .subscribe(data => {

     this.data = data["_body"];
//alert(this.data);
console.log(this.data);

     this.data = JSON.parse(this.data);
this.loaded="1";
console.log(this.data);

     }, error => {
    // alert(id);
     });
     }
}
