
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import { Platform } from 'ionic-angular';
import { StoreordersPage } from '../../pages/storeorders/storeorders';
import { ServiceorderlistPage } from '../../pages/serviceorderlist/serviceorderlist';



/**
 * Generated class for the ServicethankyouPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-servicethankyou',
  templateUrl: 'servicethankyou.html',
})
export class ServicethankyouPage {
  data:any;
  response:any;
  loaded:any="0";
  orderid:any;
  order:any;
  myorderspage:any = ServiceorderlistPage;
  public unregisterBackButtonAction: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,public http: Http,public platform: Platform) {
 
  }
  ionViewDidLoad() {
        this.orderid= this.navParams.get('orderid');
    //alert(this.orderid);
this.orderdeatails();
   // this.initializeBackButtonCustomHandler();
    
}

ionViewWillLeave() {
    // Unregister the custom back button action for this page
   // this.unregisterBackButtonAction && this.unregisterBackButtonAction();
}
myorders(){
   this.navCtrl.push(this.myorderspage); 
}
gohome(){
    this.navCtrl.popToRoot() ;
}

initializeBackButtonCustomHandler(): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(function(event){
        console.log('Prevent Back Button Page Change');
        this.navCtrl.popToRoot() 
    }, 101); // Priority 101 will override back button handling (we set in app.component.ts) as it is bigger then priority 100 configured in app.component.ts file */
}      


 
 orderdeatails() {
  //alert('ss')
    var link = 'http://18.220.1.217/services/orderdetails.php'; 
    
     var myData = JSON.stringify({orderid:  this.orderid});
     
    
      
     
     
    this.http.post(link, myData)
    //this.http.get(link)
    
     .subscribe(data => {
      console.log(data);

     this.data = data["_body"];
//alert(this.data);
this.data = JSON.parse(this.data);

this.order=this.data.result[0];
 this.loaded="1";

     }, error => {
  console.log(error);
     });
     }
}
