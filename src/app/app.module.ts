import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpModule } from '@angular/http';
import { IonicStorageModule } from '@ionic/storage';
import { ForstoreproductlistPage } from '../pages/forstoreproductlist/forstoreproductlist'
import { IonicImageViewerModule } from 'ionic-img-viewer';
import { Contacts} from '@ionic-native/contacts';
import {domtoimage} from 'dom-to-image-more';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ApptabPage } from '../pages/apptab/apptab';
import { ExpandModule } from '../components/expand/expand.module';
import { ComponentsModule } from '../components/components.module';
import { IonicImageLoader } from 'ionic-image-loader';
import { TooltipsModule } from 'ionic-tooltips';
import { TranslateModule } from '@ngx-translate/core';


import { ProductlistPage } from '../pages/productlist/productlist';
import { ResidencetypePage } from '../pages/residencetype/residencetype';
import { ResidencelistPage } from '../pages/residencelist/residencelist';
import { ProductdetailsPage } from '../pages/productdetails/productdetails';
import { SharedProvider } from '../providers/shared/shared';
import { StorecheckoutPage } from '../pages/storecheckout/storecheckout';
import { StorecartPage } from '../pages/storecart/storecart';
import { FoodcartPage } from '../pages/foodcart/foodcart';
import { FoodlistPage } from '../pages/foodlist/foodlist';
import { ForfoodlistPage } from '../pages/forfoodlist/forfoodlist';
import { StoreordersPage } from '../pages/storeorders/storeorders';
import { StoreorderdeatailPage } from '../pages/storeorderdeatail/storeorderdeatail';
import { StoreloaderPage } from '../pages/storeloader/storeloader';
import { StoresoffersPage } from '../pages/storesoffers/storesoffers';
import { StoresubscriptionPage } from '../pages/storesubscription/storesubscription';
import { StoresubscribtionlistPage } from '../pages/storesubscribtionlist/storesubscribtionlist';
import { StoresubscribtionlistbydayPage } from '../pages/storesubscribtionlistbyday/storesubscribtionlistbyday';
import { ForstoresubscribtionlistbydayPage } from '../pages/forstoresubscribtionlistbyday/forstoresubscribtionlistbyday';
import { StoresearchresultsPage } from '../pages/storesearchresults/storesearchresults';
import { CallNumber } from '@ionic-native/call-number';

import { StorecategoryPage } from '../pages/storecategory/storecategory';
import { ServicecategoryPage } from '../pages/servicecategory/servicecategory';
import { ServicesubcategoryPage } from '../pages/servicesubcategory/servicesubcategory';
import { ServicedetailsPage } from '../pages/servicedetails/servicedetails';
import { ServicebookingPage } from '../pages/servicebooking/servicebooking';
import { ServicethankyouPage } from '../pages/servicethankyou/servicethankyou';
import { ServiceorderlistPage } from '../pages/serviceorderlist/serviceorderlist';
import { HoteldetailsPage } from '../pages/hoteldetails/hoteldetails';
import { HotelmenulistPage } from '../pages/hotelmenulist/hotelmenulist';
import { HotelmenubycategoryPage } from '../pages/hotelmenubycategory/hotelmenubycategory';
import { WalletPage } from '../pages/wallet/wallet';
import { MembervsoPage } from '../pages/membervso/membervso';
import { VsoorderPage } from '../pages/vsoorder/vsoorder';
import { StorethankyouPage } from '../pages/storethankyou/storethankyou';
import { StorepaymentPage } from '../pages/storepayment/storepayment';
import { StoresearchPage } from '../pages/storesearch/storesearch';
import { AddressPage } from '../pages/address/address';
import { AddaddressPage } from '../pages/addaddress/addaddress';
import { MobileregPage } from '../pages/mobilereg/mobilereg';
import { FoodloaderPage } from '../pages/foodloader/foodloader';
import { SuperTabsModule } from 'ionic2-super-tabs';
//import { AndroidPermissions } from '@ionic-native/android-permissions';
import {ScrollingHeaderModule} from '../directive/ionic-scrolling-header';
import { InAppBrowser } from '@ionic-native/in-app-browser';

import { AndroidFullScreen } from '@ionic-native/android-full-screen';

import { ImgFallbackModule } from 'ngx-img-fallback';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { Network } from '@ionic-native/network';
import { OneSignal } from '@ionic-native/onesignal';
import { Geolocation } from '@ionic-native/geolocation';
import { FoodProvider } from '../providers/food/food';
import { AppRate } from '@ionic-native/app-rate';
import { ShopPage } from '../pages/shop/shop';
import { FoodPage } from '../pages/food/food';
import { RegistrationPage } from '../pages/registration/registration';
import { NewregistrationPage } from '../pages/newregistration/newregistration';
import { NewRegistrationPage } from '../pages/new-registration/new-registration';
import { FirstorderPage } from '../pages/firstorder/firstorder';
import { LoginPage } from '../pages/login/login';
import { ReferanotherPage } from '../pages/referanother/referanother';
import { ReferafriendPage } from '../pages/referafriend/referafriend';
import { ReferfirstorderPage } from '../pages/referfirstorder/referfirstorder';
import { NewcheckoutPage } from '../pages/newcheckout/newcheckout';
import { MemberintroPage } from '../pages/memberintro/memberintro';
import { TutorialPage } from '../pages/tutorial/tutorial';
import { WelcomePage } from '../pages/welcome/welcome';
import { UpdateaddressPage } from '../pages/updateaddress/updateaddress';
import { MmhomePage } from '../pages/mmhome/mmhome';
import { MmcategoryPage} from '../pages/mmcategory/mmcategory';
import { UpdatebankPage } from '../pages/updatebank/updatebank';
import { RefertabPage } from '../pages/refertab/refertab';
import { ReferreportPage } from '../pages/referreport/referreport';
import { MembersuccessPage } from '../pages/membersuccess/membersuccess';
import { AssociatePage } from '../pages/associate/associate';
import { TreeordersPage } from '../pages/treeorders/treeorders';
import { VsowalletPage } from '../pages/vsowallet/vsowallet';
import { SocialSharing } from '@ionic-native/social-sharing';
import { MmproductlistPage } from '../pages/mmproductlist/mmproductlist';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CartPage } from '../pages/cart/cart';
import { StorewishPage } from '../pages/storewish/storewish';


@NgModule({
  declarations: [
    StoresearchPage,
    StoreordersPage,
    StoreloaderPage,
    MyApp,
    HomePage,
    ApptabPage,
    ProductlistPage,
    ProductdetailsPage,
    StorecheckoutPage,
    StorecartPage,
    StorepaymentPage,
    AddressPage,
    AddaddressPage,
    MobileregPage,
    FoodloaderPage,
    StorethankyouPage,
    ForstoreproductlistPage,
    StoresubscriptionPage,
    StorecategoryPage,
    ServicecategoryPage,
    ServicesubcategoryPage,
    ServicedetailsPage,
    ServicebookingPage,
    ServicethankyouPage,
    ServiceorderlistPage,
    HoteldetailsPage,
    HotelmenulistPage,
    HotelmenubycategoryPage,
    FoodcartPage,
    StoresearchresultsPage,
    StoresoffersPage,
    StoreorderdeatailPage,
    StoresubscribtionlistPage,
    StoresubscribtionlistbydayPage,
    ForstoresubscribtionlistbydayPage,
    FoodlistPage,
    ForfoodlistPage,
    ResidencetypePage,
    ResidencelistPage,
    WalletPage,
    ShopPage,
    FoodPage,
    RegistrationPage,
    FirstorderPage,
    LoginPage,
    ReferanotherPage,
    ReferafriendPage,
    ReferfirstorderPage,
    NewcheckoutPage,
    MemberintroPage,
    NewregistrationPage,
    TutorialPage,
    WelcomePage,
    UpdateaddressPage,
    MmhomePage,
    MmcategoryPage,
    UpdatebankPage,
    RefertabPage,
    ReferreportPage,
    MembersuccessPage,
    NewRegistrationPage,
    AssociatePage,
    MembervsoPage,
    VsoorderPage,
    TreeordersPage,
    VsowalletPage,
    MmproductlistPage,
    CartPage,
    StorewishPage
  ],
  imports: [
    // FivethreeCoreModule,
    // FivExpandableModule,
    // FivCenterModule,
    // BrowserAnimationsModule,
    ScrollingHeaderModule,
    TooltipsModule,
    IonicImageViewerModule,
    ImgFallbackModule,
    HttpModule,
    LazyLoadImageModule,
    BrowserModule,
    ExpandModule,
    ComponentsModule,
    TranslateModule.forRoot(),
    SuperTabsModule.forRoot(),
    IonicStorageModule.forRoot(),
    IonicImageLoader.forRoot(),
    IonicModule.forRoot(MyApp,{
      mode: 'ios'
    }) 
   ],
  bootstrap: [IonicApp],
  entryComponents: [
    StoreordersPage,
    StoresubscriptionPage,
    StoreloaderPage,
    MyApp,
    StorethankyouPage,
    HomePage,
    ApptabPage,
    ProductlistPage,
    ProductdetailsPage,
    StorecheckoutPage,
    StorecartPage,
    StorepaymentPage,
    AddressPage,
    AddaddressPage,
    MobileregPage,
    FoodloaderPage,
    StoresearchPage,
    ForstoreproductlistPage,
    StorecategoryPage,
    ServicecategoryPage,
    ServicesubcategoryPage,
    ServicedetailsPage,
    ServicebookingPage,
    ServicethankyouPage,
    ServiceorderlistPage,
    HoteldetailsPage,
    HotelmenulistPage,
    HotelmenubycategoryPage,
    FoodcartPage,
    StoresearchresultsPage,
    StoresoffersPage,
    StoreorderdeatailPage,
    StoresubscribtionlistPage,
    StoresubscribtionlistbydayPage,
    ForstoresubscribtionlistbydayPage,
    FoodlistPage,
    ForfoodlistPage,
    ResidencetypePage,
    ResidencelistPage,
    WalletPage,
    ShopPage,
    FoodPage,
    RegistrationPage,
    FirstorderPage,
    LoginPage,
    ReferanotherPage,
    ReferafriendPage,
    ReferfirstorderPage,
    NewcheckoutPage,
    MemberintroPage,
    NewregistrationPage,
    TutorialPage,
    WelcomePage,
    UpdateaddressPage,
    MmhomePage,
    MmcategoryPage,
    UpdatebankPage,
    RefertabPage,
    ReferreportPage,
    MembersuccessPage,
    NewRegistrationPage,
    AssociatePage,
    MembervsoPage,
    VsoorderPage,
    TreeordersPage,
    VsowalletPage,
    MmproductlistPage,
     CartPage ,
     StorewishPage

  ],
  providers: [
   // AndroidPermissions,
    AndroidFullScreen,
    StatusBar,
    Network,
    Geolocation,
    OneSignal,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    SharedProvider,
    FoodProvider,
    CallNumber,
    AppRate,
    InAppBrowser,
    SocialSharing
  ]
})
export class AppModule {}