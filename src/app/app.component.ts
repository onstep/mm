import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from '@ionic/storage';
import { ToastController } from 'ionic-angular';
import { App, ViewController } from 'ionic-angular';
import { ServiceorderlistPage } from '../pages/serviceorderlist/serviceorderlist';
import { CallNumber } from '@ionic-native/call-number';

import { HomePage } from '../pages/home/home';
import { ServicedetailsPage } from '../pages/servicedetails/servicedetails';
import { StorecheckoutPage } from '../pages/storecheckout/storecheckout';
import { StorethankyouPage } from '../pages/storethankyou/storethankyou';
import { StoreordersPage } from '../pages/storeorders/storeorders';
import { StorepaymentPage } from '../pages/storepayment/storepayment';
import { StoreloaderPage } from '../pages/storeloader/storeloader';
import { MobileregPage } from '../pages/mobilereg/mobilereg';
import { StoresearchPage } from '../pages/storesearch/storesearch';
import { ProductdetailsPage } from '../pages/productdetails/productdetails';
import { ApptabPage } from '../pages/apptab/apptab';
import { FoodloaderPage } from '../pages/foodloader/foodloader';
import { ImageLoaderConfig } from 'ionic-image-loader';
import { AndroidFullScreen } from '@ionic-native/android-full-screen';
import { StoresubscriptionPage } from '../pages/storesubscription/storesubscription';
import { Network } from '@ionic-native/network';
import { OneSignal } from '@ionic-native/onesignal';
import { AppRate } from '@ionic-native/app-rate';
import { MemberintroPage } from '../pages/memberintro/memberintro';
import { TutorialPage } from '../pages/tutorial/tutorial';
import { RefertabPage } from '../pages/refertab/refertab';
import { MembersuccessPage } from '../pages/membersuccess/membersuccess';
import { TreeordersPage } from '../pages/treeorders/treeorders';


import { AddaddressPage } from '../pages/addaddress/addaddress';

import { ServicecategoryPage } from '../pages/servicecategory/servicecategory';
import { StoreorderdeatailPage } from '../pages/storeorderdeatail/storeorderdeatail';
import { StoresubscribtionlistPage } from '../pages/storesubscribtionlist/storesubscribtionlist';

import { ResidencetypePage } from '../pages/residencetype/residencetype';
import { ShopPage } from '../pages/shop/shop';
import { ResidencelistPage } from '../pages/residencelist/residencelist';
import { WalletPage } from '../pages/wallet/wallet';
import { SharedProvider } from '../providers/shared/shared';
import { RegistrationPage } from '../pages/registration/registration';
import { LoginPage } from '../pages/login/login';
import { ReferanotherPage } from '../pages/referanother/referanother';
import { ReferafriendPage } from '../pages/referafriend/referafriend';
import { NewcheckoutPage } from '../pages/newcheckout/newcheckout';
import { WelcomePage } from '../pages/welcome/welcome';
import { UpdateaddressPage } from '../pages/updateaddress/updateaddress';
import { MmhomePage } from '../pages/mmhome/mmhome';
import { UpdatebankPage } from '../pages/updatebank/updatebank';
import { Events } from 'ionic-angular';
import { InAppBrowser,InAppBrowserOptions } from '@ionic-native/in-app-browser';
import { AssociatePage } from '../pages/associate/associate';
import { MembervsoPage } from '../pages/membervso/membervso';
import { VsowalletPage } from '../pages/vsowallet/vsowallet';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
 // rootPage:any = ApptabPage;
 rootPage:any ;
 mobile:any ;
 userid:any ;
 userdata:any ;
 loaded:any ;
  iab: any;

//  options : InAppBrowserOptions = {
//   location : 'yes',//Or 'no' 
//   hideurlbar:'yes',
//   toolbarcolor:'black',
//   navigationbuttoncolor:'white',
//   closebuttoncolor:'white',
//   hidden : 'no', //Or  'yes'
//   clearcache : 'yes',
//   clearsessioncache : 'yes',
//   zoom : 'yes',//Android only ,shows browser zoom controls 
//   hardwareback : 'yes',
//   mediaPlaybackRequiresUserAction : 'no',
//   shouldPauseOnSuspend : 'no', //Android only 
//   closebuttoncaption : 'Close', //iOS only
//   disallowoverscroll : 'no', //iOS only 
//   toolbar : 'yes', //iOS only 
//   enableViewportScale : 'no', //iOS only 
//   allowInlineMediaPlayback : 'no',//iOS only 
//   presentationstyle : 'pagesheet',//iOS only 
//   fullscreen : 'yes',//Windows only    
// };

 constructor(public events: Events,public shared: SharedProvider,private appRate: AppRate,private callNumber: CallNumber,public oneSignal: OneSignal,private network: Network,platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen,private storage: Storage ,   private imageLoaderConfig: ImageLoaderConfig,private androidFullScreen: AndroidFullScreen,private toastCtrl: ToastController, public app :App) {
 
  events.subscribe('app:loaduserdata', () => {
    this.loaduserdata();
  });

  platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.overlaysWebView(false);
      statusBar.styleDefault();
        this.listenConnection();
        this.storage.get('mobile').then((val) => {
          console.log(val);
          console.log(val);
          if(!val){
            this.storage.get('introskipped').then((introskipped) => {
             if(introskipped=='yes'){
              this.rootPage = MobileregPage;

             }else{
            //  this.rootPage = TutorialPage;
              this.rootPage = MobileregPage;

             }
            });    



          }else{
      //  this.rootPage = MmhomePage;
        
          this.rootPage = ShopPage;
     //this.rootPage = MembersuccessPage;

    //  this.rootPage = MemberintroPage;
          }
    
           });    
          this.imageLoaderConfig.enableDebugMode();
      this.imageLoaderConfig.enableFallbackAsPlaceholder(true);
      this.imageLoaderConfig.setFallbackUrl('assets/imgs/logo.png');
      this.imageLoaderConfig.setMaximumCacheAge(24 * 60 * 60 * 1000);
 
   // statusBar.backgroundColorByHexString('#ff7c1c');
   //  statusBar.backgroundColorByName('white');

      splashScreen.hide(); 
      this.androidFullScreen.showSystemUI()
        .then(() => console.log('Immersive mode supported'))
  .catch(err => console.log(err));
        this.declarestorecart();
      //  this.checkresidence();
        //statusBar.show();
        statusBar.styleDefault();

        statusBar.backgroundColorByHexString('#ffffff');
       // statusBar.backgroundColorByHexString('#f4f2e5');


//    this.oneSignal.startInit('f17a3322-543e-40ba-b58a-b4f4484cde1b', '520179124830');

// this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);

// this.oneSignal.handleNotificationReceived().subscribe((data) => {
//  // do something when notification is received

// });

// this.oneSignal.handleNotificationOpened().subscribe((data) => {
//   // do something when a notification is opened
//  //alert(JSON.stringify(data));


// });
// var ids=this.oneSignal.getIds().then((data) => {
// // alert(data.userId);
// //alert(data.pushToken);
// });

// this.oneSignal.endInit();
   



    });
  }

  loaduserdata(){
    this.storage.get('userdata').then((val2) => {
      if(val2){
        this.userdata=val2;
        console.log(this.userdata);
    this.loaded='1';
    
    
      }
    
       });
  }
  ionViewDidLoad() {
    this.storage.get('userdata').then((val2) => {
      if(val2){
        this.userdata=val2;
        console.log(this.userdata);
    this.loaded='1';
    
    
      }
    
       });
      }
      menuClosed() {
        this.events.publish('menu:closed', '');

    }
    
    menuOpened() {
        this.events.publish('menu:opened', '');

       this.storage.get('userdata').then((val2) => {
        if(val2){
          this.userdata=val2;
          console.log(this.userdata);
     this.loaded='1';
      
      
        }
      
         });
    }


  ngOnInit(){
    this.storage.get('userdata').then((val2) => {
      if(val2){
        this.userdata=val2;
        console.log(this.userdata);
   this.loaded='1';
    
    
      }
    
       });
    this.storage.get('mobile').then((mobile) => {
       if(!mobile){
         //this.rootPage=MobileregPage;
      }
        console.log(mobile);
               this.shared.mobile=mobile;
  
         });
    this.storage.get('userid').then((val) => {
    //  if(!val){
     //   this.rootPage=MobileregPage;
    //  }
      console.log(val);
             this.shared.userid=val;

       });

      

  }
  declarestorecart(){
  
    //this.storage.remove('storecart');
      this.storage.get('storecart').then((val) => {
      //  alert(val);
        if(val==null){
          var storecartdata=[];
            this.storage.set('storecart', storecartdata);
          var wishlist=[];
            this.storage.set('wishlist', wishlist);
            var userid="nouserid";
            this.storage.set('userid', userid);
      this.shared.userid="nouserid";

          //  var residencetype='notselected';
          //  this.storage.set('residencetype', residencetype);
//var residenceid='notselected';
          //  this.storage.set('residenceid', residenceid);
          //  alert("created");
        }
        else{
            this.storage.get('mobile').then((val) => {
this.shared.mobile=val;
//alert(this.mobile);
    });

  
        }

          
      
  
    
      });
  
    }
  checkresidence(){

    //this.storage.remove('storecart');
      this.storage.get('residenceid').then((val) => {
      //  alert(val);
        if(val==null){
         console.log(val);
        //  this.app.getRootNav().push(ResidencetypePage);
          this.rootPage = ResidencelistPage;

        }else{
          this.rootPage = WalletPage;

        }

          
      
  
    
      });
  
    }


    gotoproductlist()
    {
      this.app.getRootNav().push(StoresubscribtionlistPage);
    
     } 
     refer()
    {
      this.app.getRootNav().push(ReferanotherPage);
    
     } 
     becomeamember()
    {
      this.app.getRootNav().push(MemberintroPage);
    
     } 
     becomevso()
    {
      this.app.getRootNav().push(MembervsoPage);
    
     } 
     vsowallet()
    {
      this.app.getRootNav().push(VsowalletPage);
    
     } 
     treeorders()
    {
      this.app.getRootNav().push(TreeordersPage);
    
     } 
     referafriend()
    {
      this.app.getRootNav().push(RefertabPage);
    
     } 

    gomobilereg()
    {
      this.app.getRootNav().push(MobileregPage);
    
     } 
    updatekyc()
    {
      this.app.getRootNav().push(UpdatebankPage);
    
     } 
    becomeassosicate()
    {
      this.app.getRootNav().push(AssociatePage);
    
     } 
    panel()
   // panel(url : string)
    {

      var url ="http://adpay.farmliferetail.com";
        let target = "_blank";
        this.iab.create(url,target,this.options);
    

     } 
  options(url: string, target: string, options: any) {
    throw new Error("Method not implemented.");
  }
    address()
    {
      this.app.getRootNav().push(UpdateaddressPage);
    
     } 
    goserviceorders()
    {
      
      this.app.getRootNav().push(ServiceorderlistPage);
    
     } 
    gostoreorders()
    {
      
      this.app.getRootNav().push(StoreordersPage);
    
     } 
    gowallet()
    {
      this.app.getRootNav().push(WalletPage);
    
     } 
     logout(){
      var userid="nouserid";
      this.storage.set('userid', userid);
      this.shared.userid="nouserid";
      this.storage.remove('name');
      this.storage.remove('userid');
      this.storage.remove('userdata');
      this.storage.remove('mobile');
      this.storage.remove('joinpage');

      this.app.getRootNav().setRoot(MobileregPage);
      this.app.getRootNav().popToRoot();

  
      this.storage.get('userid').then((val) => {
   console.log(val);
    });
  }
    
    public listenConnection(): void {


      this.network.onDisconnect()
        .subscribe(() => {
          this.showAlert();
        });

       // alert(this.network.type);
       
    }
  
    private showAlert(): void {
this.showToastWithCloseButton();

  
    }
    
    call(){
      this.callNumber.callNumber(this.userdata.care, true)
  .then(res => console.log('Launched dialer!', res))
  .catch(err => console.log('Error launching dialer', err));
    }

    rate(){
      this.appRate.preferences.storeAppURL = {
      //  ios: '<app_id>',
        android: 'market://details?id=com.onstep.user',
       // windows: 'ms-windows-store://review/?ProductId=<store_id>'
      };
      
      this.appRate.promptForRating(true);
      
    }
    showToastWithCloseButton() {
      const toast = this.toastCtrl.create({
        message: 'Your internet connection appears to be offline. Check your Internet connection.',
        showCloseButton: true,
        closeButtonText: 'Ok'
        
      });
      toast.onDidDismiss(this.dismissHandler);
      toast.present();
    }
  
    private dismissHandler() {
  
      console.info('Toast onDidDismiss()');
    }
  
}

