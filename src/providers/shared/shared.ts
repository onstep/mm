import { Http } from '@angular/http'; //https://stackoverflow.com/questions/43609853/angular-4-and-ionic-3-no-provider-for-http
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { App, ViewController } from 'ionic-angular';
import {  ViewChild } from '@angular/core';
import { Content } from 'ionic-angular';

/*
  Generated class for the SharedProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class SharedProvider {
  @ViewChild(Content) content: Content;

  data: any;
  userid: any;
  mobile: any;
  status: any;
  in: any;
  productinacrtcount: any;
  storecartdetails: any=[];
  allproductcount:any=[];
 // storecartdata: any=[];
  constructor(public http: Http,private storage: Storage, public app :App) {
      //this.storage.remove('storecart');
      this.storage.get('mobile').then((val) => {
this.mobile=val;
      });
      this.storage.get('userdata').then((val) => {
       // alert(val.status);
       if(val){
         this.status=val.status;

       }
      });
      this.storage.get('storecart').then((val) => {
        //  alert(val);
          if(val==null){
            var storecartdata=[];
              this.storage.set('storecart', storecartdata);
              var userid="nouserid";
              this.storage.set('userid', userid);
            //  alert("created");
  
          }
  
  
            
            this.storecart();

    
      
        });
    
  }


 getstatus(){
     this.storage.get('userdata').then((val) => {
return val.status;
      });
 }


 removestorecart(){

  var storecartdata=[];

   this.storage.set('storecart', storecartdata)
   .then(
    () => 
  this.storage.get('storecart').then((val) => {
    
    this.storecartdetails=val;
    this.storecart();
  })

  );
 
 }
 
  storecart(){

    this.storage.get('storecart').then((val) => {
      console.log(val);
     // alert(val.length);
  var total:number=0;
  var totalmrp:number=0;
  var totalcount:number=0;

 for(var i=0;i<val.length; i++){
        total=total + (val[i].price * val[i].count);
        totalcount=totalcount+val[i].count;
        console.log(totalcount);
         }
 for(var i=0;i<val.length; i++){
        totalmrp=totalmrp + (val[i].mrp * val[i].count);
        totalcount=totalcount+val[i].count;
        console.log(totalcount);
         }
                 this.storecartdetails.all=val;
                 this.storecartdetails.price=total;
                 this.storecartdetails.mrp=totalmrp;
                 this.storecartdetails.totalcount=totalcount;
                 this.storecartdetails.count=val.length;

     // alert("price"+this.storecartdetails.price);
      //alert("price"+this.storecartdetails.count);
      //alert("price"+this.storecartdetails.totalcount);
    });
   
       
        
      
    
      }
    


  productcount(productid){

    this.storage.get('storecart').then((val) => {
  
  var  index = val.findIndex((obj => obj.productid == productid));
  
   if(index == -1){
    this.productinacrtcount[productid]=0;
  
  }
  else{
    this.productinacrtcount[productid]=val[index].count;
  
  }
  });
   }
  
addtostorecart(productid,price,mrp){

    this.storage.get('storecart').then((val) => {

    var  index = val.findIndex((obj => obj.productid == productid));
    //alert(index);
    if(index== -1){
   let item ={productid: productid, price: price,mrp:mrp,count:1};
  

  val.push(item),
this.storage.set('storecart',val).then((val) => {
this.storecart();

});
  
  this.storecart();

  var  index = val.findIndex((obj => obj.productid == productid));
  //val[index].count++;
  //this.storage.set('storecart',val);
// alert(val[index].count);



    }else{
      val[index].count++;
      this.storage.set('storecart',val).then((val) => {
        this.storecart();
        
        });
    }
    });
    this.storecart();




  }

 reduceinstorecart(productid){
 var count; 
    this.storage.get('storecart').then((val) => {

    var  index = val.findIndex((obj => obj.productid == productid));
    //alert(index);
   
      val[index].count--;
      if(val[index].count==0){
        val.splice(index,1);
      }
      this.storage.set('storecart',val).then((val) => {





        this.storage.get('storecart').then((val) => {
          console.log(val);
          var  index = val.findIndex((obj => obj.productid == productid));
        
           if(index == -1){
           count=0;
          
          }
          else{
         count=val[index].count;

          }
         // alert(count+'jjj');
       this.allproductcount[productid]= count;

          });






        this.storecart();
        




        });
    });
    this.storecart();
       
  }
  
  




   
     

      updatecart(productid,price,mrp){

        this.storage.get('storecart').then((val) => {

          var  index = val.findIndex((obj => obj.productid == productid));
          //alert(index);
          if(index== -1){
         let item ={productid: productid, price: price,mrp:mrp,count:1};
        
      
        val.push(item),
      this.storage.set('storecart',val).then((val) => {
      this.storecart();
      
      });
        
        this.storecart();
      
        var  index = val.findIndex((obj => obj.productid == productid));
        //val[index].count++;
        //this.storage.set('storecart',val);
      // alert(val[index].count);
      
      
      
          }else{
            val[index].price=price;
            val[index].mrp=mrp;
            this.storage.set('storecart',val).then((val) => {
              this.storecart();
              
              });
          }
          });
          this.storecart();
      
      
      
      
        }
}
