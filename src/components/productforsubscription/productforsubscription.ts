import { Component, ViewChild, OnInit, Renderer, Input } from '@angular/core';
import { ProductdetailsPage } from '../../pages/productdetails/productdetails';
import { App,ModalController, ViewController } from 'ionic-angular';
import { SharedProvider } from '../../providers/shared/shared';
import { Storage } from '@ionic/storage';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Content, Select } from 'ionic-angular';
import { Http } from '@angular/http';
import { VariableAst } from '@angular/compiler';
import { StoresubscriptionPage } from '../../pages/storesubscription/storesubscription';

/**
 * Generated class for the ProductforsubscriptionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'productforsubscription',
  templateUrl: 'productforsubscription.html'
})
export class ProductforsubscriptionComponent {
  @ViewChild('mySelect') selectRef: Select;

  @Input('productid') productid: any;
  @Input('productname') productname: any;
  @Input('productunit') productunit: any;
  @Input('productprice') productprice: number;
  @Input('productmrp') productmrp: number;
  @Input('variants') variants: any;
  @Input('img') img: any;
  @Input('container') container: any;
  count: any;
  productoffer: any;
  data: any;
  product: any;
  loaded: any='0';
  singleproduct: any;
  optionsloaded: any='0';
  nextloaded: any='0';
  a: any=this.variants;
subspage:any=StoresubscriptionPage;

  constructor(public navCtrl: NavController, public navParams: NavParams,public app :App,public shared: SharedProvider,private storage: Storage,public http: Http,public modalCtrl: ModalController) {

  //this.shared.addtostorecart(event,'1','90','100');

//setInterval(this.productcount, 5000);

this.productcount();
//console.log(this.productname);


  }
  ngAfterViewInit() {
    if(this.variants=='no'){

  
  } else{
        this.getvariants(this.variants);

  }
 }
 subscription() {
  let subsmodal = this.modalCtrl.create(this.subspage, { productid: this.productid },{showBackdrop: true});
  subsmodal.present();
}

  openSelect()
  {
    event.stopPropagation(); //THIS DOES THE MAGIC
    this.selectRef.open();

  }

  closeSelect()
  {
      this.selectRef.close();
  }


  changevariant(id){
    this.getproduct(id);
        this.selectRef.close();

  }
  gotoproductdetails(id)
{
 
  this.app.getRootNav().push(ProductdetailsPage, {productid: id });

 }

 

 productcount(){

  this.storage.get('storecart').then((val) => {
//console.log(val);
var  index = val.findIndex((obj => obj.productid == this.productid));

 if(index == -1){
  this.count=0;

}
else{
this.count=val[index].count;

}
});
 }
 
 
 add(event) {
  event.stopPropagation(); //THIS DOES THE MAGIC
this.count++;
this.addtostorecart(this.productid,this.productprice,this.productmrp);
   
     
//setTimeout(this.productcount, 3000)


}
 nothing(event) {
  event.stopPropagation(); //THIS DOES THE MAGIC
 // this.selectRef.open();

}
 sub(event) {
  event.stopPropagation(); //THIS DOES THE MAGIC
     //  this.count=this.count-1;
     this.count--;

     this.reduceinstorecart(this.productid);
    // this.count=this.shared.allproductcount[this.productid];
       

     }

     addtostorecart(productid,price,mrp){

      this.storage.get('storecart').then((val) => {
  
      var  index = val.findIndex((obj => obj.productid == productid));
      //alert(index);
      if(index== -1){
     let item ={productid: productid, price: price,mrp:mrp,count:1};
    
  
    val.push(item),
  this.storage.set('storecart',val).then((val) => {
    var count;
    this.storage.get('storecart').then((vall) => {
     // console.log(vall);
      var  index = vall.findIndex((obj => obj.productid == productid));
    
       if(index == -1){
       count=0;
      
      }
      else{
     count=vall[index].count;

      }
    
   this.count= count;

      });
  this.shared.storecart();
  
  });
    
  this.shared.storecart();
  
    var  index = val.findIndex((obj => obj.productid == productid));
    //val[index].count++;
    //this.storage.set('storecart',val);
  // alert(val[index].count);
  
  
  
      }else{
        val[index].count++;
        this.storage.set('storecart',val).then((val) => {
          var count;
          this.storage.get('storecart').then((vall) => {
           // console.log(vall);
            var  index = vall.findIndex((obj => obj.productid == productid));
          
             if(index == -1){
             count=0;
            
            }
            else{
           count=vall[index].count;
      
            }
          
         this.count= count;
      
            });
          this.shared.storecart();
          
          });
      }
      });
     this.shared.storecart();
     
  
  
  
  
    }

     reduceinstorecart(productid){
      var count; 
         this.storage.get('storecart').then((val) => {
     
         var  index = val.findIndex((obj => obj.productid == productid));
         //alert(index);
        
           val[index].count--;
           if(val[index].count==0){
             val.splice(index,1);
           }
           this.storage.set('storecart',val).then((val) => {
     
     
     
     
     
             this.storage.get('storecart').then((val) => {
            //   console.log(val);
               var  index = val.findIndex((obj => obj.productid == productid));
             
                if(index == -1){
                count=0;
                this.shared.storecart();

               }
               else{
              count=val[index].count;
              this.shared.storecart();

               }
             
            this.count= count;
            this.shared.storecart();

               });
     
     
     
     
     
     
             
     
     
     
     
             });
         });
         this.shared.storecart();
   
       }


       getvariants(id) {
        var link = 'https://api.mmsilks.onstep.in/farmlifeappbackend/variants.php'; 
        
         var myData = JSON.stringify({variantid:  this.variants});
         
        
          
         
         
        this.http.post(link, myData)
        //this.http.get(link)
        
         .subscribe(data => {
    
         this.data = data["_body"];
        // alert(this.data);
    
         this.product = JSON.parse(this.data);
    
     
   this.optionsloaded="1";

         }, error => {
        // alert(id);
         });
         }


         getproduct(id) {
          var link = 'https://api.mmsilks.onstep.in/farmlifeappbackend/product.php'; 
          
           var myData = JSON.stringify({pid:  id});
           
          
            
           
           
          this.http.post(link, myData)
          //this.http.get(link)
          
           .subscribe(data => {
      
           this.singleproduct = data["_body"];
          //alert(this.singleproduct);
      
           this.singleproduct = JSON.parse(this.singleproduct);
      
           this.productid=this.singleproduct.product[0].pid;
           this.productname=this.singleproduct.product[0].name;
           this.productunit=this.singleproduct.product[0].finalunit;
           this.productprice=this.singleproduct.product[0].price;
           this.productmrp=this.singleproduct.product[0].mrp;
           this.img=this.singleproduct.product[0].image;

           this.productcount();
     this.nextloaded="1";
  
           }, error => {
          // alert(id);
           });
           }
    



}
