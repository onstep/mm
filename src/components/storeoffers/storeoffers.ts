import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Http } from '@angular/http';
import { StoresubscriptionPage } from '../../pages/storesubscription/storesubscription';

import { App,ModalController, ViewController } from 'ionic-angular';
import { StoresoffersPage } from '../../pages/storesoffers/storesoffers';

/**
 * Generated class for the StoreoffersComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'storeoffers',
  templateUrl: 'storeoffers.html',
  inputs: ['name'] 


})
export class StoreoffersComponent {
@Input('image') img: string;
@Input('offername') offername: string;
@Input('offertype') offertype: any;
@Input('stock') stock: any;
  @Input('offercode')   offercode: any;
  @Input('color')   color: string;

  storeoffers=StoresoffersPage;

  subspage:any=StoresubscriptionPage;
  data: any;
  loaded: any="0";
  target:any = document.getElementById('target');

  constructor(public http: Http,public modalCtrl: ModalController,public app :App) {
  //  console.log('Hello StoreoffersComponent Component');
  }
  

  

  ngAfterViewInit() {
     console.log(this.offercode);
   this.getproductdetails(this.offercode);
   
  }
 
  gotooffers(id){


    this.app.getRootNav().push(this.storeoffers, {offerid: this.offercode,offertype: this.offertype ,offername:this.offername});

  }

  getproductdetails(offercode) {
    var link = 'https://api.mmsilks.onstep.in/farmlifeappbackend/offer.php'; 
    
     var myData = JSON.stringify({code:  offercode});
     
    
      
     
     
    this.http.post(link, myData)
    //this.http.get(link)
    
     .subscribe(data => {

     this.data = data["_body"];
      

     this.data = JSON.parse(this.data);
     this.loaded="1";

console.log(this.data);

     }, error => {
    // alert(id);
     });
     }

}



