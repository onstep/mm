
import { Component, ViewChild, OnInit, Renderer, Input } from '@angular/core';

/**
 * Generated class for the AccordionComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'expand',
  templateUrl: 'expand.html'
})
export class ExpandComponent implements OnInit {

  accordionExapanded = false;
  @ViewChild("cc") cardContent: any;
  @ViewChild("ccc") divContent: any;
  @Input('title') title: string;

  icon: string = "arrow-forward";
  loaded :boolean;
  constructor(public renderer: Renderer) {
  }

  ngOnInit() {
    console.log(this.cardContent.nativeElement);
    this.renderer.setElementStyle(this.cardContent.nativeElement, "webkitTransition", "max-height 500ms, padding 500ms");
    this.renderer.setElementStyle(this.divContent.nativeElement, "webkitTransition", "max-height 500ms, padding 500ms");
    this.renderer.setElementStyle(this.divContent.nativeElement, "webkitTransition", "display 500ms");
    this.renderer.setElementStyle(this.divContent.nativeElement,"max-height", "0px");

  }
 


  toggleAccordion() {
    if (this.accordionExapanded) {
      this.renderer.setElementStyle(this.cardContent.nativeElement, "max-height", "0px");
      this.renderer.setElementStyle(this.cardContent.nativeElement, "padding", "0px 0px 0px 0px");
      this.renderer.setElementStyle(this.divContent.nativeElement,"max-height", "0px");


    } else {
      this.renderer.setElementStyle(this.cardContent.nativeElement, "max-height", "5000px");
      
      
      this.renderer.setElementStyle(this.divContent.nativeElement,"max-height", "5000px");

      this.renderer.setElementStyle(this.cardContent.nativeElement, "padding", "0px 0px 0px 0px");

    }

    this.accordionExapanded = !this.accordionExapanded;
    this.icon = this.icon == "arrow-forward" ? "arrow-down" : "arrow-forward";

  }

}