

import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';

import { CommonModule } from '@angular/common';
import { ExpandComponent  } from './expand';
@NgModule({
  declarations: [ ExpandComponent ],
  imports: [
    CommonModule,
    IonicModule
  ],
  exports: [ExpandComponent]
})
export class ExpandModule { }
