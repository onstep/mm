import { Component, ViewChild, OnInit, Renderer, Input } from '@angular/core';
import { ProductdetailsPage } from '../../pages/productdetails/productdetails';
import { App,ModalController, ViewController } from 'ionic-angular';
import { SharedProvider } from '../../providers/shared/shared';
import { Storage } from '@ionic/storage';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { StoresubscriptionPage } from '../../pages/storesubscription/storesubscription';
import { ToastController } from 'ionic-angular';

/**
 * Generated class for the ProductforstorehomeComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'productforstorehome',
  templateUrl: 'productforstorehome.html'
})
export class ProductforstorehomeComponent {
  @Input('img') img: string;
  @Input('id')  productid: any;
  @Input('name') productname: string;
  @Input('mrp') productmrp: string;
  @Input('price') productprice: string;
  @Input('productunit') productunit: any;
  @Input('offertype') offertype: any;
  @Input('target') target: any;
  @Input('itemlimit') itemlimit: any;
  @Input('stock') stock: any;
  count: any;
  productoffer: any;
  subspage:any=StoresubscriptionPage;
  placeimg:any="assets/imgs/logo.png";

  constructor(public navCtrl: NavController, public navParams: NavParams,public app :App,public shared: SharedProvider,private storage: Storage,public modalCtrl: ModalController,private toastCtrl: ToastController) {
//this.shared.addtostorecart(event,'1','90','100');
//setInterval(this.productcount, 5000);
this.productcount();
//alert(this.productunit);
  }

  gotoproductdetails(id) 
{
  this.app.getRootNav().push(ProductdetailsPage, {productid: id });

 }
 subscription() {
  let subsmodal = this.modalCtrl.create(this.subspage, { productid: this.productid },{showBackdrop: true});
  subsmodal.present();
}

 productcount(){

  this.storage.get('storecart').then((val) => {
//console.log(val);
var  index = val.findIndex((obj => obj.productid == this.productid));

 if(index == -1){
  this.count=0;

}
else{
this.count=val[index].count;

}
});
 }
 
 add(event) {


  event.stopPropagation(); //THIS DOES THE MAGIC
 // alert(this.itemlimit);
 //alert(this.stock);
  if(this.stock=='0'){
      let toast = this.toastCtrl.create({
        message: 'Out of stock',
        duration: 2000,
        position: 'bottom'
      });
      toast.present();

      return;

    }

  if(this.count>this.itemlimit-1){


let toast = this.toastCtrl.create({
  message: 'You can buy only ' +this.itemlimit+ ' items.',
  duration: 2000,
  position: 'bottom'
});



toast.present();
  }else{


    if(this.count>this.stock-1){

      let toast = this.toastCtrl.create({
        message: 'You can buy only ' +this.stock+ ' items.',
        duration: 2000,
        position: 'bottom'
      });
      
      
      
      toast.present();
    
    }else{
      
    this.count++;
this.addtostorecart(this.productid,this.productprice,this.productmrp);

        }

  }

     
//setTimeout(this.productcount, 3000)


}
 nothing(event) {
  event.stopPropagation(); //THIS DOES THE MAGIC

}
 sub(event) {
  event.stopPropagation(); //THIS DOES THE MAGIC
     //  this.count=this.count-1;
     this.count--;

     this.reduceinstorecart(this.productid);
    // this.count=this.shared.allproductcount[this.productid];
       

     }

     addtostorecart(productid,price,mrp){

      this.storage.get('storecart').then((val) => {
  
      var  index = val.findIndex((obj => obj.productid == productid));
      //alert(index);
      if(index== -1){
     let item ={productid: productid, price: price,mrp:mrp,count:1};
    
  
    val.push(item),
  this.storage.set('storecart',val).then((val) => {
    var count;
    this.storage.get('storecart').then((vall) => {
     // console.log(vall);
      var  index = vall.findIndex((obj => obj.productid == productid));
    
       if(index == -1){
       count=0;
      
      }
      else{
     count=vall[index].count;

      }
    
   this.count= count;

      });
  this.shared.storecart();
  
  });
    
  this.shared.storecart();
  
    var  index = val.findIndex((obj => obj.productid == productid));
    //val[index].count++;
    //this.storage.set('storecart',val);
  // alert(val[index].count);
  
  
  
      }else{
        val[index].count++;
        this.storage.set('storecart',val).then((val) => {
          var count;
          this.storage.get('storecart').then((vall) => {
           // console.log(vall);
            var  index = vall.findIndex((obj => obj.productid == productid));
          
             if(index == -1){
             count=0;
            
            }
            else{
           count=vall[index].count;
      
            }
          
         this.count= count;
      
            });
          this.shared.storecart();
          
          });
      }
      });
     this.shared.storecart();
     
  
  
  
  
    }

     reduceinstorecart(productid){
      var count; 
         this.storage.get('storecart').then((val) => {
     
         var  index = val.findIndex((obj => obj.productid == productid));
         //alert(index);
        
           val[index].count--;
           if(val[index].count==0){
             val.splice(index,1);
           }
           this.storage.set('storecart',val).then((val) => {
     
     
     
     
     
             this.storage.get('storecart').then((val) => {
            //   console.log(val);
               var  index = val.findIndex((obj => obj.productid == productid));
             
                if(index == -1){
                count=0;
                this.shared.storecart();

               }
               else{
              count=val[index].count;
              this.shared.storecart();

               }
             
            this.count= count;
            this.shared.storecart();

               });
     
     
     
     
     
     
             
     
     
     
     
             });
         });
         this.shared.storecart();
   
       }



}
