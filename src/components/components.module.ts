import { NgModule } from '@angular/core';
import { CategoryComponent } from './category/category';
import { IonicModule } from "ionic-angular";
import { SubcategoryComponent } from './subcategory/subcategory';
import { IonicImageLoader } from 'ionic-image-loader';
import { ProductforlistingsComponent } from './productforlistings/productforlistings';
import { ProductmainComponent } from './productmain/productmain';
import { StorefooterComponent } from './storefooter/storefooter';
import { ProductinstorecartComponent } from './productinstorecart/productinstorecart';
import { BasketloaderComponent } from './basketloader/basketloader';
import { XscrollproductsComponent } from './xscrollproducts/xscrollproducts';
import { StoreoffersComponent } from './storeoffers/storeoffers';
import { ProductforstorehomeComponent } from './productforstorehome/productforstorehome';
import { FaIconComponent } from './fa-icon/fa-icon';
import { JetmanloaderComponent } from './jetmanloader/jetmanloader';
import { PanloaderComponent } from './panloader/panloader';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { HotelsComponent } from './hotels/hotels';
import { FoodmenuComponent } from './foodmenu/foodmenu';
import { FoodfooterComponent } from './foodfooter/foodfooter';
import { ProductforsubscriptionComponent } from './productforsubscription/productforsubscription';
import { SearchiconComponent } from './searchicon/searchicon';
import { FooditemComponent } from './fooditem/fooditem';
import { MmsubcategoryComponent } from './mmsubcategory/mmsubcategory';
import { MmproductlistComponent } from './mmproductlist/mmproductlist';
import { TapsComponent } from './taps/taps';
import { ProductinswishComponent } from './productinswish/productinswish';

@NgModule({
	declarations: [CategoryComponent,
    SubcategoryComponent,
    ProductforlistingsComponent,
    ProductmainComponent,
    StorefooterComponent,
    ProductinstorecartComponent,
    BasketloaderComponent,
    XscrollproductsComponent,
    StoreoffersComponent,
    ProductforstorehomeComponent,
    FaIconComponent,
    JetmanloaderComponent,
    PanloaderComponent,
    HotelsComponent,
    FoodmenuComponent,
    FoodfooterComponent,
    ProductforsubscriptionComponent,
    SearchiconComponent,
    FooditemComponent,
    MmsubcategoryComponent,
    MmproductlistComponent,
    TapsComponent,
    ProductinswishComponent,
    ],
	imports: [
        IonicModule,
        LazyLoadImageModule,
		IonicImageLoader
],
	exports: [CategoryComponent,
    SubcategoryComponent,
    ProductforlistingsComponent,
    ProductmainComponent,
    StorefooterComponent,
    ProductinstorecartComponent,
    BasketloaderComponent,
    XscrollproductsComponent,
    StoreoffersComponent,
    ProductforstorehomeComponent,
    FaIconComponent,
    JetmanloaderComponent,
    PanloaderComponent,
    HotelsComponent,
    FoodmenuComponent,
    FoodfooterComponent,
    ProductforsubscriptionComponent,
    SearchiconComponent,
    FooditemComponent,
    MmsubcategoryComponent,
    MmproductlistComponent,
    TapsComponent,
    ProductinswishComponent]
})
export class ComponentsModule {}
