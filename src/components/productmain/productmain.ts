import { Component } from '@angular/core';

/**
 * Generated class for the ProductmainComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'productmain',
  templateUrl: 'productmain.html'
})
export class ProductmainComponent {

  text: string;

  constructor() {
    console.log('Hello ProductmainComponent Component');
    this.text = 'Hello World';
  }

}
