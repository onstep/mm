
import { Component, ViewChild, OnInit, Renderer, Input } from '@angular/core';
import { SharedProvider } from '../../providers/shared/shared';
import { ForstoreproductlistPage } from '../../pages/forstoreproductlist/forstoreproductlist';
import { App, ViewController } from 'ionic-angular';

/**
 * Generated class for the AccordionComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'category',
  templateUrl: 'category.html'
})
export class CategoryComponent implements OnInit {

  accordionExapanded = false;
  @ViewChild("cc") cardContent: any;
  @ViewChild("cardmain") cardmain: any;
  @ViewChild("ccc") divContent: any;
  @ViewChild("categorycard") categorycard: any;
  @Input('categoryname') categoryname: any;
  @Input('img') img: any;
  @Input('id') id: any;
  @Input('categoryarray') categoryarray: any; 
  @Input('categoryoffers') categoryoffers: any; 
  @Input('container') container: any; 
 

  icon: string = "arrow-forward";
  loaded :boolean;
  subcategory :any=[];
  sname :any=[];
placeimg:any="assets/imgs/logo.png";
  constructor(public renderer: Renderer,public shared: SharedProvider, public app :App) {

  }
  

  ngOnInit() {
    this.renderer.setElementStyle(this.cardContent.nativeElement, "max-height", "5000px");
    this.renderer.setElementStyle(this.divContent.nativeElement, "background-color", "rgba(255, 255, 255)");
  this.renderer.setElementStyle(this.categorycard.nativeElement, "background-color", "rgba(255, 255, 255)");
    
    
    this.renderer.setElementStyle(this.divContent.nativeElement,"max-height", "5000px");

    this.renderer.setElementStyle(this.cardContent.nativeElement, "padding", "0px 0px 0px 0px");
    this.renderer.setElementStyle(this.cardmain.nativeElement,"margin-bottom", "5px");

    console.log(this.categoryoffers);
    for (var i = 0; i < this.categoryarray.length ; i++) {
      if (this.categoryarray[i].category === this.id) {
        this.subcategory.push(this.categoryarray[i]);
      }
      
          //  console.log(this.sname);

     // this.sname=this.subcategory.;
    //  alert(this.sname[0].sname);
  }
  for (var i = 0; i < this.subcategory.length ; i++) {
          this.sname.push(this.subcategory[i].sname);
      }
    //  console.log(this.sname);

  // alert(this.categoryarray[0].id);
   // console.log(this.cardContent.nativeElement);
    this.renderer.setElementStyle(this.cardContent.nativeElement, "webkitTransition", "max-height 500ms, padding 500ms");
    this.renderer.setElementStyle(this.divContent.nativeElement, "webkitTransition", "max-height 500ms, padding 500ms");
    this.renderer.setElementStyle(this.divContent.nativeElement, "webkitTransition", "display 500ms");
    this.renderer.setElementStyle(this.cardmain.nativeElement, "webkitTransition", "margin-bottom 500ms");
    this.renderer.setElementStyle(this.categorycard.nativeElement, "webkitTransition", "background-color 500ms");
    this.renderer.setElementStyle(this.divContent.nativeElement, "webkitTransition", "background-color 500ms");
    this.renderer.setElementStyle(this.divContent.nativeElement,"max-height", "0px");
  }



  toggleAccordion() {
   // this.shared.content.scrollTo(0, 150, 4000)


    if (this.accordionExapanded) {
      this.renderer.setElementStyle(this.cardContent.nativeElement, "padding", "0px 0px 0px 0px");
      this.renderer.setElementStyle(this.divContent.nativeElement,"max-height", "0px");
      this.renderer.setElementStyle(this.cardmain.nativeElement,"margin-bottom", "-15px");
   this.renderer.setElementStyle(this.categorycard.nativeElement, "background-color", "rgb(252, 239, 239)");
      this.renderer.setElementStyle(this.cardContent.nativeElement, "max-height", "0px");
      this.renderer.setElementStyle(this.divContent.nativeElement, "background-color", "rgb(252, 239, 239)");


    } else {
      this.renderer.setElementStyle(this.cardContent.nativeElement, "max-height", "5000px");
      this.renderer.setElementStyle(this.divContent.nativeElement, "background-color", "rgba(42, 184, 228, 0.26)");
    this.renderer.setElementStyle(this.categorycard.nativeElement, "background-color", "rgba(42, 184, 228, 0.26)");
      
      
      this.renderer.setElementStyle(this.divContent.nativeElement,"max-height", "5000px");

      this.renderer.setElementStyle(this.cardContent.nativeElement, "padding", "0px 0px 0px 0px");
      this.renderer.setElementStyle(this.cardmain.nativeElement,"margin-bottom", "5px");

    }

    this.accordionExapanded = !this.accordionExapanded;
    this.icon = this.icon == "arrow-forward" ? "arrow-down" : "arrow-forward";

  }
  gotoproductlist(id,name)
{
  this.app.getRootNav().push(ForstoreproductlistPage, {subcategoryid: id ,subcategoryname: name});

 } 

}