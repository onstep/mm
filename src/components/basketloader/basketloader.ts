import { Component } from '@angular/core';

/**
 * Generated class for the BasketloaderComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'basketloader',
  templateUrl: 'basketloader.html'
})
export class BasketloaderComponent {

  text: string;

  constructor() {
    console.log('Hello BasketloaderComponent Component');
    this.text = 'Hello World';
  }

}
