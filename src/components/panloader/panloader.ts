import { Component } from '@angular/core';

/**
 * Generated class for the PanloaderComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'panloader',
  templateUrl: 'panloader.html'
})
export class PanloaderComponent {

  text: string;

  constructor() {
    console.log('Hello PanloaderComponent Component');
    this.text = 'Hello World';
  }

}
