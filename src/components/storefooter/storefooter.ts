import { Component, ViewChild, OnInit, Renderer, Input } from '@angular/core';
import { SharedProvider } from '../../providers/shared/shared';
import { StorecartPage } from '../../pages/storecart/storecart';
import { App, ViewController } from 'ionic-angular';

/** 
 * Generated class for the StorefooterComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'storefooter',
  templateUrl: 'storefooter.html'
})
export class StorefooterComponent {
  @ViewChild("fot") footer: any;

  storecartpage=StorecartPage;

    constructor(public renderer: Renderer,public shared: SharedProvider,public app :App) {

    // alert(this.categoryarray[0].id);
  // console.log(this.footer.nativeElement);
    }

  gostorecart(){

    this.app.getRootNav().push(this.storecartpage, { });

  }
}
