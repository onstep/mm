import { FoodProvider } from '../../providers/food/food';
import { Component, ViewChild, OnInit, Renderer, Input } from '@angular/core';
import { ProductdetailsPage } from '../../pages/productdetails/productdetails';
import { App, ViewController } from 'ionic-angular';
import { SharedProvider } from '../../providers/shared/shared';
import { Storage } from '@ionic/storage';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Content, Select } from 'ionic-angular';
import { Http } from '@angular/http';
import { VariableAst } from '@angular/compiler';
/**
 * Generated class for the FooditemComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'fooditem',
  templateUrl: 'fooditem.html'
})
export class FooditemComponent {
  @ViewChild('mySelect') selectRef: Select;

  @Input('productid') productid: any;
  @Input('productname') productname: any;
  @Input('productunit') productunit: any;
  @Input('productprice') productprice: number;
  @Input('productmrp') productmrp: number;
  @Input('producttype') producttype: number;
  @Input('itemlimit') itemlimit: any;
  @Input('img') img: any;
  @Input('container') container: any;

  count: any;
  productoffer: any;
  data: any;
  product: any;
  loaded: any='0';
  singleproduct: any;
  optionsloaded: any='0';
  nextloaded: any='0';
 

  constructor(public navCtrl: NavController, public navParams: NavParams,public app :App,public shared: SharedProvider,public food: FoodProvider,private storage: Storage,public http: Http) {

    //this.shared.addtostorecart(event,'1','90','100');
  
  //setInterval(this.productcount, 5000);
  
  this.productcount();
  //console.log(this.productname);
  
  
    }
   
  
 
    gotoproductdetails(id)
  {
   
    this.app.getRootNav().push(ProductdetailsPage, {productid: id });
  
   }
  
  
  
   productcount(){
  
    this.storage.get('foodcart').then((val) => {
  //console.log(val);
  var  index = val.findIndex((obj => obj.productid == this.productid));
  
   if(index == -1){
    this.count=0;
  
  }
  else{
  this.count=val[index].count;
  
  }
  });
   }
   
   
   add(event) {
    event.stopPropagation(); //THIS DOES THE MAGIC
  this.count++;
  this.addtostorecart(this.productid,this.productprice);
     
       
  //setTimeout(this.productcount, 3000)
  
  
  }
   nothing(event) {
    event.stopPropagation(); //THIS DOES THE MAGIC
   // this.selectRef.open();
  
  }
   sub(event) {
    event.stopPropagation(); //THIS DOES THE MAGIC
       //  this.count=this.count-1;
       this.count--;
  
       this.reduceinstorecart(this.productid);
      // this.count=this.shared.allproductcount[this.productid];
         
  
       }
  
       addtostorecart(productid,price){
  
        this.storage.get('foodcart').then((val) => {
    
        var  index = val.findIndex((obj => obj.productid == productid));
        //alert(index);
        if(index== -1){
       let item ={productid: productid, price: price,count:1};
      
    
      val.push(item),
    this.storage.set('foodcart',val).then((val) => {
      var count;
      this.storage.get('foodcart').then((vall) => {
       // console.log(vall);
        var  index = vall.findIndex((obj => obj.productid == productid));
      
         if(index == -1){
         count=0;
        
        }
        else{
       count=vall[index].count;
  
        }
      
     this.count= count;
  
        });
    this.food.foodcart();
    
    });
      
    this.food.foodcart();
    
      var  index = val.findIndex((obj => obj.productid == productid));
      //val[index].count++;
      //this.storage.set('storecart',val);
    // alert(val[index].count);
    
    
    
        }else{
          val[index].count++;
          this.storage.set('foodcart',val).then((val) => {
            var count;
            this.storage.get('foodcart').then((vall) => {
             // console.log(vall);
              var  index = vall.findIndex((obj => obj.productid == productid));
            
               if(index == -1){
               count=0;
              
              }
              else{
             count=vall[index].count;
        
              }
            
           this.count= count;
        
              });
              this.food.foodcart();
            
            });
        }
        });
        this.food.foodcart();
       
    
    
    
    
      }
  
       reduceinstorecart(productid){
        var count; 
           this.storage.get('foodcart').then((val) => {
       
           var  index = val.findIndex((obj => obj.productid == productid));
           //alert(index);
          
             val[index].count--;
             if(val[index].count==0){
               val.splice(index,1);
             }
             this.storage.set('foodcart',val).then((val) => {
       
       
       
       
       
               this.storage.get('foodcart').then((val) => {
              //   console.log(val);
                 var  index = val.findIndex((obj => obj.productid == productid));
               
                  if(index == -1){
                  count=0;
                  this.food.foodcart();
  
                 }
                 else{
                count=val[index].count;
                this.food.foodcart();
  
                 }
               
              this.count= count;
              this.food.foodcart();
  
                 });
       
       
       
       
       
       
               
       
       
       
       
               });
           });
           this.food.foodcart();
     
         }
  
  
     
  
       
     
      
  
  
  
  }
  