import { Component } from '@angular/core';

/**
 * Generated class for the JetmanloaderComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'jetmanloader',
  templateUrl: 'jetmanloader.html'
})
export class JetmanloaderComponent {

  text: string;

  constructor() {
    console.log('Hello JetmanloaderComponent Component');
    this.text = 'Hello World';
  }

}
