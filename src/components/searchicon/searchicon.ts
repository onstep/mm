import { Component } from '@angular/core';
import { StoresearchPage } from '../../pages/storesearch/storesearch';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the SearchiconComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'searchicon',
  templateUrl: 'searchicon.html'
})
export class SearchiconComponent {

  storesearchpage=StoresearchPage;

  constructor(public navCtrl: NavController,) {
  }
  gotosearch(){

    this.navCtrl.push(this.storesearchpage, { });

  }
}
