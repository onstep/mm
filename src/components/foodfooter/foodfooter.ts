import { Component, ViewChild, OnInit, Renderer, Input } from '@angular/core';
import { FoodProvider } from '../../providers/food/food';
import { StorecartPage } from '../../pages/storecart/storecart';
import { App, ViewController } from 'ionic-angular';
/**
 * Generated class for the FoodfooterComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'foodfooter',
  templateUrl: 'foodfooter.html'
})
export class FoodfooterComponent {
  @ViewChild("fot") footer: any;

  storecartpage=StorecartPage;

    constructor(public renderer: Renderer,public food: FoodProvider,public app :App) {

    // alert(this.categoryarray[0].id);
  // console.log(this.footer.nativeElement);
    }

  gostorecart(){

    this.app.getRootNav().push(this.storecartpage, { });

  }
}
