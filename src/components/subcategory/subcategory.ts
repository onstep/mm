import { Component, ViewChild, OnInit, Renderer, Input } from '@angular/core';
import { ImageLoader } from 'ionic-image-loader';
import { ForstoreproductlistPage } from '../../pages/forstoreproductlist/forstoreproductlist';
import { NavController } from 'ionic-angular';
import { App, ViewController } from 'ionic-angular';

/**
 * Generated class for the SubcategoryComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'subcategory',
  templateUrl: 'subcategory.html'
})
export class SubcategoryComponent {
@Input('id') id: any;
@Input('name') name: any;
@Input('img') img: any;

  text: string;
  placeimg:any="assets/imgs/logo.png";

  constructor(imageLoader: ImageLoader,public navCtrl: NavController, public app :App) {
  }

gotoproductlist(id,name)
{
  this.app.getRootNav().push(ForstoreproductlistPage, {subcategoryid: id ,subcategoryname: name});

 } 
}
