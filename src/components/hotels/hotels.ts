import { Component, ViewChild, OnInit, Renderer, Input } from '@angular/core';
import { ProductdetailsPage } from '../../pages/productdetails/productdetails';
import { App, ViewController } from 'ionic-angular';
import { SharedProvider } from '../../providers/shared/shared';
import { Storage } from '@ionic/storage';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Content, Select } from 'ionic-angular';
import { Http } from '@angular/http';
import { VariableAst } from '@angular/compiler';
/**
 * Generated class for the HotelsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'hotels',
  templateUrl: 'hotels.html'
})
export class HotelsComponent {
  @ViewChild('mySelect') selectRef: Select;

  @Input('hotelid') hotelid: any;
  @Input('hotelname') hotelname: any;
  @Input('status') status: any;
  @Input('image') image: any;
  @Input('type') type: number;
  @Input('location') location: any;
  @Input('distance') distance: any;
  @Input('duration') duration: any;
  @Input('commision') commision: any;
  

  constructor(public navCtrl: NavController, public navParams: NavParams,public app :App,public shared: SharedProvider,private storage: Storage,public http: Http) {

  //this.shared.addtostorecart(event,'1','90','100');

//setInterval(this.productcount, 5000);

//console.log(this.productname);


  }
 


}
