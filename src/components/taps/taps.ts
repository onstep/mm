import { Component } from '@angular/core';
import { StorecartPage } from '../../pages/storecart/storecart';
import { App, ViewController } from 'ionic-angular';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { StorewishPage } from '../../pages/storewish/storewish';

/**
 * Generated class for the TapsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'taps',
  templateUrl: 'taps.html'
})
export class TapsComponent {

  text: string;

  constructor(public navCtrl: NavController, public navParams: NavParams,public app :App,) {
    console.log('Hello TapsComponent Component');
    this.text = 'Hello World';
  }
  cart(){
    this.app.getRootNav().push(StorecartPage);
   }
    heart(){
    this.app.getRootNav().push(StorewishPage);
   }
}
